package local;

import org.json.JSONObject;
import pipe.EndPoint;

import java.io.IOException;
import java.net.*;

public class Local {
    private static String timeStamp() {
        long now = System.currentTimeMillis();
        return String.format( "%tT: ", now );
    }
    private static Boolean isStart(int endpointPort) throws IOException {
        DatagramSocket serverSocket = new DatagramSocket(endpointPort);
        System.out.println("MAS server application has been started to listen port: " + endpointPort);
        byte[] receiveData = new byte[1024];
        byte[] sendData;
        boolean start = false;

        DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
        serverSocket.receive(receivePacket);
        String sentence = new String( receivePacket.getData());
        InetAddress IPAddress = receivePacket.getAddress();
        int port = receivePacket.getPort();
        String response = "starting process...";
        sendData = response.getBytes();
        DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, port);
        serverSocket.send(sendPacket);
        if(sentence.contains("{start:")) {
            JSONObject jsonObject = new JSONObject(sentence);
            start = jsonObject.getBoolean("start");
        }
        return true;
    }
    
    public static void main( String[] args ) throws IOException {
        System.out.println("In command-line passed next arguments:");
        System.out.println("1. Type of agent (possible: lc - local): " + args[0]);
        System.out.println("2. Current agent pair IP and port (possible arg format: <192.168.3.1 55532>)): " + args[1] + ":" + args[2]);
        System.out.println("3. Remote agents 3 pairs IP and Port for each rm agent (possible arg format: <192.168.3.3 55734>) the list of agent:" );
        System.out.println("3.1 rm agent 1 ip:port: " + args[3] + " " + args[4]);
        System.out.println("3.2 rm agent 2 ip:port: " + args[5] + " " + args[6]);
        System.out.println("3.3 rm agent 3 ip:port:" + args[7] + " " + args[8]);
        System.out.println("4 Nxt devices ports (in range 9876 - 9881):");
        System.out.println("4.1 Device 0 - WPCoordinator: " + args[9]);
        System.out.println("4.1 Device 1 - HCyl1: " + args[10]);
        String agentType = args[0];
        String hostLcAgent = args[1];
        int portLcAgent = Integer.parseInt(args[2]);
        String hostRmAgent1 = args[3];
        int portRmAgent1 = Integer.parseInt(args[4]);
        String hostRmAgent2 = args[5];
        int portRmAgent2 = Integer.parseInt(args[6]);
        String hostRmAgent3 = args[7];
        int portRmAgent3 = Integer.parseInt(args[8]);
        int portDevice0 = Integer.parseInt(args[9]);
        int portDevice1 = Integer.parseInt(args[10]);


        if (agentType.equals("lc")){
            while ( true ) {
                if (isStart(portLcAgent)) {

                    RemoteManager rm = new RemoteManager(hostRmAgent1, portRmAgent1);
                    if (!rm.connect()) {
                        System.err.println("unable to connect to remoteHCyl2 process");
                        System.exit(1);
                    }

                    RemoteManager rm2 = new RemoteManager(hostRmAgent2, portRmAgent2);
                    if (!rm2.connect()) {
                        System.err.println("unable to connect to remoteVCyl1 process");
                        System.exit(1);
                    }

                    RemoteManager rm3 = new RemoteManager(hostRmAgent3, portRmAgent3);
                    if (!rm3.connect()) {
                        System.err.println("unable to connect to remoteVCyl2 process");
                        System.exit(1);
                    }

                    LocalAgent a = new LocalAgent("K9", rm, rm2,rm3, hostLcAgent, portDevice0, hostLcAgent, portDevice1 );
                    a.make();
                    rm.disconnect();
                }
            }
        } else {
            System.out.println("The type of agent is not available, the program will be terminated in 5 sec" );
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.exit(0);
        }

    }
}
