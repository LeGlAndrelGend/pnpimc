package local;

import com.intendico.gorite.*;
import com.intendico.gorite.addon.TimeTrigger;
import com.intendico.gorite.addon.RemotePerforming;
import org.json.JSONObject;
import pipe.EndPoint;
import pipe.UDPClient;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LocalAgent extends Performer {
    
    RemotePerforming rp;
    RemotePerforming rp2;
    RemotePerforming rp3;

    // DSL definitions   
    Plan plan( String n, Goal[] g ) {
        return new Plan( n, g );
    }
    
    BDIGoal remote( String n ) {
        return new BDIGoal( n );
    }
    
    Goal[] subgoals( Goal... g ) {
        return g;
    }
     
    // goal definitions
    Goal dataCollection(String ipDevice0, int portDevice0, String ipDevice1, int portDevice1) {
        return plan( "collection data",
            subgoals(
                getWPCoordinates(ipDevice0, portDevice0),
                getLengthHCyl1(ipDevice1, portDevice1),
                remote( "getLengthHCyl2" ),
                remote("getLengthVCyl1"),
                remote("getLengthVCyl2"),
                scheduleStates(),
                sendStateHCyl1(ipDevice1, portDevice1),
                testContext(),
                remote("sendStateHCyl2"),
                remote("sendStateVCyl1"),
                remote("sendStateVCyl2"),
                remote("sendStateGripper1")
                )
        );
    }

    Goal testContext(){
        return new Goal("testContext") {
            @Override
            public Goal.States execute(Data d) {
     /*           String p = (String) d.getValue( "remoteVCyl1 process" );
                String m = (String) d.getValue( "remoteVCyl1 machine" );
                // Gson converts the time from int to double, as in JavaScript,
                // all numbers are floating point and values of data elements are
                // typed as Object. So fair enough.
                double t = (Double) d.getValue( "remoteVCyl1 time" )*1.0;
                System.err.println( p+" process at step2 performed by "+m+" in "+t+" time units" );
                System.out.println(d.getValue("local HCYLPARAMS"));
                System.out.println(d.getValue("local VCYLPARAMS"));
                System.out.println(d.getValue("XTARGET"));
                System.out.println(d.getValue("YTARGET"));
                System.out.println("HCyl2" + d.getValue("remoteHCyl2 machine"));
                System.out.println("HCyl2" + d.getValue("remoteHCyl2 time"));*/

                Map<String, Map<String, Object>> HCYLPARAMS = (Map<String, Map<String, Object>>) d.getValue("local HCYLPARAMS");
                System.out.println("testContext HCYLPARAMS" + HCYLPARAMS);
//                d.setValue("HCyl1 state", HCYLPARAMS.get("HCyl1").get("state"));
//                d.setValue("HCyl2 state", HCYLPARAMS.get("HCyl2").get("state"));

                System.out.println("testContext HCyl1 state" + d.getValue("HCyl1 state"));
                System.out.println("testContext HCyl2 state" + d.getValue("HCyl2 state"));
                System.out.println("testContext remoteHCyl2 machine" + d.getValue("remoteHCyl2 machine"));
                System.out.println("testContext XTARGET" + d.getValue("XTARGET"));

                return States.PASSED;
            }
        };
    }
    Goal sendStateHCyl1(String ipDevice, int portDevice){
        return new Goal("sendStateHCyl1") {
            @Override
            public Goal.States execute(Data d) {

                if ( TimeTrigger.isPending( d, "deadline", 1*1000 ) )
                    return Goal.States.BLOCKED;
                Map<String, Map<String, Object>> HCYLPARAMS = (Map<String, Map<String, Object>>) d.getValue("local HCYLPARAMS");
                String agentStatus = HCYLPARAMS.get("HCyl1").get("state").toString();
                UDPClient client = new UDPClient(ipDevice, portDevice, agentStatus);
                System.out.println("Has been sent control signal('" + agentStatus + "') to device:" + ipDevice + portDevice);

                d.setValue("HCyl1 state", true);
                d.setValue("XTARGET", 999);
                return States.PASSED;
            }
        };
    }
    Goal getWPCoordinates(String ipDevice, int portDevice) {
        return new Goal( "getWPCoordinates" ) {
            @Override
            public Goal.States execute(Data d) {

                System.out.println("Goal: " + getGoalName() + " has been lunched...");
                                // step 1 takes 1 seconds.
                if ( TimeTrigger.isPending( d, "deadline", 1*1000 ) )
                    return Goal.States.BLOCKED;

                /**GET WP coordinates from device in JSON format*/
                UDPClient client = new UDPClient(ipDevice, portDevice, "getWPCoordinates()");
                String responce = null;
                try{
                    responce = client.sendMessage();
                } catch (IOException e){System.err.println(e);}

                JSONObject coordinatesJSON = new JSONObject(responce);
                System.out.println("WP coordinates is " + coordinatesJSON.toString());
                int xTarget = coordinatesJSON.getInt("x");
                int yTarget = coordinatesJSON.getInt("y");
                d.setValue("XTARGET", xTarget);
                d.setValue("YTARGET", yTarget);

                System.err.println( "getWPCoordinates completed" );
                return Goal.States.PASSED;
            }
        };
    }
    
    Goal getLengthHCyl1(String ipDevice, int portDevice) {
        return new Goal( "getLengthHCyl1" ) {
            @Override
            public Goal.States execute(Data d) {    
                // step 2 takes 1 seconds.
                if ( TimeTrigger.isPending( d, "deadline", 1*1000 ) )
                    return Goal.States.BLOCKED;
                /**GET length HCyl1*/
                UDPClient client = new UDPClient(ipDevice, portDevice, "getLengthHCyl1()");
                String responce = null;
                try{
                    responce = client.sendMessage();
                } catch (IOException e){System.err.println(e);}
                JSONObject jsonObject = new JSONObject(responce);
                int length = jsonObject.getInt("length");
                Map<String, Map<String, Object>> HCYLPARAMS = new HashMap<>();
                HCYLPARAMS.put("HCyl1", new HashMap<>());
                HCYLPARAMS.get("HCyl1").put("length", length);

                d.setValue("local HCYLPARAMS", HCYLPARAMS);
                System.err.println( "getLengthHCyl1 completed" );

                return Goal.States.PASSED;
            }
        };
    }

    Goal scheduleStates() {
        return new Goal( "scheduleStates" ) {
            @Override
            public Goal.States execute(Data d) {
                // step 2 takes 1 seconds.
                if ( TimeTrigger.isPending( d, "deadline", 1*1000 ) )
                    return Goal.States.BLOCKED;

                System.out.println("Goal: " + getGoalName() + ". Scheduling process has been lunched...");
                int xTarget = (Integer) d.getValue("XTARGET");
                List<Integer> HCylLengths = new ArrayList<>();
                Map<String, Map<String, Object>> HCYLPARAMS = (Map<String, Map<String, Object>>) d.getValue("local HCYLPARAMS");

                for (Map.Entry agent : HCYLPARAMS.entrySet()) {
                    Map<String, Object> params = (Map<String, Object>)agent.getValue();
                    Double length = Double.parseDouble(params.get("length").toString());
                    HCylLengths.add(length.intValue());
                }

                SchedulerCyls schedulerCylsX = new SchedulerCyls(HCylLengths, xTarget);
                List<Integer> validCombX = schedulerCylsX.getValidComb();
                if (validCombX == null){
                    System.err.println("Error goal: There is no valid HORIZONTAL combination, goal is not reached");
                    return States.FAILED;
                } else {
                    System.out.println("Set enable state for HCylinders: ");
                    for (Map.Entry agent : HCYLPARAMS.entrySet()) {
                        Map<String, Object> params = (Map<String, Object>)agent.getValue();
                        params.put("state", false);
                    }

                    for (int curComb = 0; curComb < validCombX.size(); curComb++) {
                        int length = validCombX.get(curComb);
                        for (Map.Entry agent : HCYLPARAMS.entrySet()) {
                            Map<String, Object> params = (Map<String, Object>)agent.getValue();
                            Double lDouble = Double.parseDouble(params.get("length").toString());
                            if(lDouble.intValue() == length && !(Boolean)params.get("state")) {
                                params.put("state", true);
                                System.out.println("Agent: " + agent.getKey() + ", enabled for action");
                                break;
                            }
                        }
                    }
                }
                d.setValue("local HCYLPARAMS", HCYLPARAMS);

                int yTarget = (Integer) d.getValue("YTARGET");
                List<Integer> VCylLengths = new ArrayList<>();
                Map<String, Map<String, Object>> VCYLPARAMS = (Map<String, Map<String, Object>>) d.getValue("local VCYLPARAMS");
                for (Map.Entry agent : VCYLPARAMS.entrySet()) {
                    Map<String, Object> params = (Map<String, Object>)agent.getValue();
                    Double length = Double.parseDouble(params.get("length").toString());
                    VCylLengths.add(length.intValue());
                }

                SchedulerCyls schedulerCylsY = new SchedulerCyls(VCylLengths, yTarget);
                List<Integer> validCombY = schedulerCylsY.getValidComb();
                if (validCombY == null){
                    System.err.println("Error goal: There is no valid VERTICAL combination, goal is not reached");
                    return States.FAILED;
                } else {
                    System.out.println("Set enable state for VCylinders: ");
                    for (Map.Entry agent : VCYLPARAMS.entrySet()) {
                        Map<String, Object> params = (Map<String, Object>)agent.getValue();
                        params.put("state", false);
                    }

                    for (int curComb = 0; curComb < validCombY.size(); curComb++) {
                        int length = validCombY.get(curComb);
                        for (Map.Entry agent : VCYLPARAMS.entrySet()) {
                            Map<String, Object> params = (Map<String, Object>)agent.getValue();
                            Double lDouble = Double.parseDouble(params.get("length").toString());
                            if(lDouble.intValue() == length && !(Boolean)params.get("state")) {
                                params.put("state", true);
                                System.out.println("Agent: " + agent.getKey() + ", enabled for action");
                                break;
                            }
                        }
                    }
                }
                d.setValue("local VCYLPARAMS", VCYLPARAMS);

                return Goal.States.PASSED;
            }
        };
    }
    
    public void make() {
        Data d = new Data();
        d.setValue( "remoteHCyl2 process", "joining" );
        d.setValue( "remoteHCyl2 machine", "" );
        d.setValue( "remoteHCyl2 time", new Integer( 0 ) );

        d.setValue( "remoteVCyl1 process", "joining" );
        d.setValue( "remoteVCyl1 machine", "" );
        d.setValue( "remoteVCyl1 time", new Integer( 0 ) );

        d.setValue( "remoteVCyl2 process", "joining" );
        d.setValue( "remoteVCyl2 machine", "" );
        d.setValue( "remoteVCyl2 time", new Integer( 0 ) );

        d.setValue("local HCYLPARAMS", new HashMap<>());
        d.setValue("local VCYLPARAMS", new HashMap<>());
        d.setValue("XTARGET", new Integer(0));
        d.setValue("YTARGET", new Integer(0));
        d.setValue("HCyl1 state", new Boolean(false));
        d.setValue("HCyl2 state", new Boolean(false));
        rp.addGoal( rp.create(
            "getLengthHCyl2",
            new String[] { "remoteHCyl2 process", "local HCYLPARAMS", "HCyl1 state", "HCyl2 state", "XTARGET", "YTARGET" },
            new String[] { "remoteHCyl2 machine", "remoteHCyl2 time"}
        ));
//        rp.addGoal( rp.create(
//                "sendStateHCyl2",
//                new String[] { "remoteHCyl2 process", "local HCYLPARAMS" },
//                new String[] { }
//        ));
        rp2.addGoal( rp2.create(
                "getLengthVCyl1",
                new String[] {"remoteVCyl1 process", "local VCYLPARAMS"},
                new String[] {"remoteVCyl1 machine", "remoteVCyl1 time"}
        ));
//        rp2.addGoal( rp2.create(
//                "sendStateVCyl1",
//                new String[] { "remoteVCyl1 process", "local VCYLPARAMS" },
//                new String[] { }
//        ));
        rp3.addGoal( rp3.create(
                "getLengthVCyl2",
                new String[] {"remoteVCyl2 process", "local VCYLPARAMS"},
                new String[] {"remoteVCyl2 machine", "remoteVCyl2 time"}
        ));
//        rp3.addGoal( rp3.create(
//                "sendStateVCyl2",
//                new String[] { "remoteVCyl2 process", "local VCYLPARAMS" },
//                new String[] { }
//        ));
        performGoal( new BDIGoal( "collection data" ), "makeDataCollection", d );
    }
    
    public LocalAgent( String n, RemoteManager rm, RemoteManager rm2, RemoteManager rm3, String ipDev0, int portDev0, String ipDev1, int portDev1 ) {
        super( n );
        addGoal( dataCollection(ipDev0, portDev0, ipDev1, portDev1) );
        rp = new RemotePerforming( rm );
        addCapability( rp );
        rp2 = new RemotePerforming(rm2);
        addCapability(rp2);
        rp3 = new RemotePerforming(rm3);
        addCapability(rp3);
    }
    
}
