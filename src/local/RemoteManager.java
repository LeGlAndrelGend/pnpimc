package local;

import pipe.*;

import static pipe.Request.*;
import static pipe.Status.*;

import java.net.*;
import java.util.Vector;

import com.intendico.data.Ref;
import com.intendico.gorite.Goal;
import com.intendico.gorite.addon.RemotePerforming;
import com.intendico.gorite.addon.RemotePerforming.*;

public class RemoteManager implements Connector {

    Pipe pipe;

    private String timeStamp() {
        long now = System.currentTimeMillis();
        return String.format( "%tT: ", now );
    } 
    
    public RemoteManager(String ip, int port) {
        try {
            Socket s = new Socket( ip, port);
            pipe = new TextPipe( s );
        }
        catch( Exception e ) {
            e.printStackTrace();
            System.exit( 1 );
        } 
    }
    
    boolean connect() {
        return pipe.open();
    }
    
    public boolean disconnect() {
        pipe.write( ( new Request( CLOSE ) ).toJson() );
        return pipe.close();
    }
        
    @Override
    public Connection perform( String goal, String head, Vector<Ref> ins, Vector<Ref> outs ) {
        pipe.write( ( new Request( PERFORM, goal, head, ins, outs ) ).toJson() );
        // pass in r so that outs can be updated (in check()) and therefore accessible
        // from the data context
        return new RemoteConnector();   
    }
    
    public class RemoteConnector implements RemotePerforming.Connection {
        
        Vector<Ref> outs;
        
        public RemoteConnector( ) {
        }
        
        /**
	 * This method is used repeatedly in order to discover the
	 * status of the remoteHCyl2 goal execution. And also its outputs.
	 */
        @Override
	public Goal.States check() throws Throwable {
          try {
            pipe.write( ( new Request( STATUS ) ).toJson() );
            Status status = Status.fromJson( (String) pipe.read() );
            switch( status.state ) {
                case IDLE:
                    return Goal.States.STOPPED;
                case STOPPED:
                    return Goal.States.STOPPED;
                case PASSED:
                    outs = status.outs;
                    return Goal.States.PASSED;
            }
            return Goal.States.FAILED;
          }
          catch( Exception e ) {
                e.printStackTrace();
            }
          return Goal.States.FAILED;
        }

	/**
	 * This method will be called if the goal execution is to be
	 * canceled from the triggering side.
	 */
        @Override
	public void cancel(){
            // to do
        }
        
        @Override
        public Vector<Ref> results() {
            return outs;
        }
    }
}
