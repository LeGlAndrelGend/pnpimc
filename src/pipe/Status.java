package pipe;

import com.google.gson.Gson;
import com.intendico.data.Ref;
import java.util.Vector;
import java.io.Serializable;

public class Status implements Serializable {
    
    public static final String IDLE = "idle";
    public static final String STOPPED = "stopped";
    public static final String PASSED = "passed";
    
    private static Gson gson;
    
    static {
        gson = new Gson();
    }
    
    public static Status fromJson( String jstring ) {
        return (Status) gson.fromJson( jstring, Status.class );
    }

    public String goal;
    public Vector<Ref> outs;
    public String state;
    
    public Status( String g, Vector<Ref> o, String s ) {
        goal = g;
        outs = o;
        state = s;   
    }
        
    public String toJson() {
        return gson.toJson( this );
    }    
    
    @Override
    public String toString() {
        return toJson();
    }
}
