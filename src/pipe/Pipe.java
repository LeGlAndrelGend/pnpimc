package pipe;

public interface Pipe {    
    public boolean open();
    public boolean close();
    public boolean write( Object o );
    public Object read();  
}
