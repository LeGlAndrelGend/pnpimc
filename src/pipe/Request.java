package pipe;

import com.google.gson.Gson;
import com.intendico.data.Ref;
import java.util.Vector;
import java.io.Serializable;


public class Request implements Serializable {
    
    public static final String PERFORM = "perform";
    public static final String STATUS = "status";
    public static final String CANCEL = "cancel";
    public static final String CLOSE = "close";
    
    private static Gson gson;
    
    static {
        gson = new Gson();
    }
    
    public static Request fromJson( String jstring ) {
        return (Request) gson.fromJson( jstring, Request.class );
    }
    
    public String type;
    public String goal;
    public String head;
    public Vector<Ref> ins;
    public Vector<Ref> outs;
    public String state;
    
    public Request( String t, String g, String h, Vector<Ref> i, Vector<Ref> o ) {
        type = t;
        goal = g;
        head = h;
        ins = i;
        outs = o;
    } 
    
    public Request(String t ) {
        type = t;
        goal = null;
        head = null;
        ins = null;
        outs = null;
    }
    
    public String toJson() {
        return gson.toJson( this );
    }
    
    @Override
    public String toString() {
        return toJson();
    }
}
