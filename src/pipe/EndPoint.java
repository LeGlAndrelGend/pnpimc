package pipe;

public class EndPoint {
//    public static final String hostAgent3 = "192.168.3.121";
//    public static final int portAgent2 = 56789;


    public static final String hostAgent1 = "localhost";
    public static final int portAgent1 = 55875;
    public static final int portDevice0 = 9876;
    public static final int portDevice1 = 9877;

    public static final String hostAgent2 = "localhost";
    public static final int portAgent2 = 56789;
    public static final int portDevice2 = 9878;

    public static final String hostAgent3 = "localhost";
    public static final int portAgent3 = 56790;
    public static final int portDevice3 = 9879;

    public static final String hostAgent4 = "localhost";
    public static final int portAgent4 = 56791;
    public static final int portDevice4 = 9880;
    public static final int portDevice5 = 9881;
}
