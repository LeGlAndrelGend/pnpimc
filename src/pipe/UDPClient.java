package pipe;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

import static com.intendico.gorite.Goal.isTracing;

public class UDPClient {

    String ip;
    int agentIpPort;
    int port;
    String msg;

    public UDPClient(String ip, int port, String msg){
        this.ip = ip;
        this.port = port;
        this.msg = msg;
    }

    public String sendMessage() throws IOException {
        if ( isTracing() )
            System.err.println("UDP connection with device " + " connection: " + ip + ":" + port + ", message: " + msg);
        DatagramSocket clientSocket = new DatagramSocket(agentIpPort);
        InetAddress IPAddress = InetAddress.getByName(ip);
        String sentence = msg;
        byte[] sendData = sentence.getBytes();
        byte[] receiveData = new byte[1024];

        DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, port);
        clientSocket.send(sendPacket);
        DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
        clientSocket.receive(receivePacket);
        String modifiedSentence = new String(receivePacket.getData());
        if ( isTracing() )
            System.err.println("FROM SERVER message:" + modifiedSentence);
        /** Timeout in ms, waiting passing signal between FB's*/
        clientSocket.close();
        return modifiedSentence;

    }

/*    public static void main(String [] args) throws Throwable {
        UDPClient client = new UDPClient("device with IP:portAgent2" + "127.0.0.1" + ":" + 9876, "127.0.0.1", 55876, 9876, "getWPCoordinates()");
    }*/
}
