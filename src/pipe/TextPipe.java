package pipe;

import java.io.*;
import java.net.Socket;

public class TextPipe implements Pipe {
    
    Socket socket;
    PrintWriter pw;
    BufferedReader br;
    
    public TextPipe( Socket s ) {
        socket = s;
    }
    
    @Override
    public boolean open() {
        try {        
            pw = new PrintWriter( socket.getOutputStream() );
            br = new BufferedReader( new InputStreamReader( socket.getInputStream() ) );
            pw.flush();
            return true;
        }
        catch ( Exception e ) {
            e.printStackTrace();
            return false;
        }
    }
    
    @Override
    public boolean write( Object o ) {
        try {
            pw.println( o );
            pw.flush();
            return true;
        }
        catch( Exception e ) {
            return false;
        }
    }
    
    @Override
    public Object read() {
        try {
            return br.readLine();
        }
        catch( Exception e ) {
            return null;
        }
    }
    
    @Override
    public boolean close() {
        try {
            pw.close();
            br.close();
            return true;
        }
        catch( Exception e ) {
            return false;
        }
    }
    
}

