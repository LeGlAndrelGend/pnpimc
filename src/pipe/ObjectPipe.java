package pipe;

import java.io.*;
import java.net.Socket;

public class ObjectPipe implements Pipe {
    
    Socket socket;
    ObjectOutputStream oos;
    ObjectInputStream ois;

    public ObjectPipe( Socket s ) {
        socket = s;
    }
    
    @Override
    public boolean open() {
        try {
            
            oos = new ObjectOutputStream( socket.getOutputStream() );
            ois = new ObjectInputStream( socket.getInputStream() );
            return true;
        }
        catch ( Exception e ) {
            e.printStackTrace();
            return false;
        }
    }
    
    @Override
    public boolean write( Object o ) {
        try {
            oos.writeObject( o );
            oos.flush();
            return true;
        }
        catch( Exception e ) {
            return false;
        }
    }
    
    @Override
    public Object read() {
        try {
            return ois.readObject();
        }
        catch( Exception e ) {
            return null;
        }
    }
    
    @Override
    public boolean close() {
        try {
            ois.close();
            oos.close();
            return true;
        }
        catch( Exception e ) {
            return false;
        }
    }
    
}
