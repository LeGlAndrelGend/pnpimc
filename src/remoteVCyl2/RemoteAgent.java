package remoteVCyl2;

import com.intendico.data.Ref;
import com.intendico.gorite.BDIGoal;
import com.intendico.gorite.Data;
import com.intendico.gorite.Goal;
import com.intendico.gorite.Performer;
import com.intendico.gorite.addon.TimeTrigger;
import org.json.JSONObject;
import pipe.EndPoint;
import pipe.Request;
import pipe.Status;
import pipe.UDPClient;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import static pipe.Status.PASSED;

public class RemoteAgent extends Performer {
    
    public Status status;
    
    Goal getLengthVCyl2(String ipDevice, int portDevice) {
        return new Goal( "getLengthVCyl2" ) {
            @Override
            public States execute(Data d) {
                String rp = (String) d.getValue( "remoteVCyl2 process");

                // step 2 takes 1 seconds.
                if ( TimeTrigger.isPending( d, "deadline", 1*1000 ) )
                    return States.BLOCKED;

                /**GET length VCyl2*/
                UDPClient client = new UDPClient(ipDevice, portDevice, "getLengthVCyl2()");
                String responce = null;
                try{
                    responce = client.sendMessage();
                } catch (IOException e){System.err.println(e);}
                JSONObject jsonObject = new JSONObject(responce);
                int length = jsonObject.getInt("length");
                Map<String, Map<String, Object>> VCYLPARAMS = (Map<String, Map<String, Object>>) d.getValue("local VCYLPARAMS");
                VCYLPARAMS.put("VCyl2", new HashMap<>());
                VCYLPARAMS.get("VCyl2").put("length", length);

                System.err.println( "getLengthVCyl2 completed" );

                d.setValue( "remoteVCyl2 machine", "device4" );
                d.setValue( "remoteVCyl2 time", new Integer(4) );
                d.setValue("local VCYLPARAMS", VCYLPARAMS);
                status.outs = new Vector<Ref>();
                status.outs.add( new Ref( "remoteVCyl2 machine", "device4" ) );
                status.outs.add ( new Ref( "remoteVCyl2 time", new Integer( 4 ) ) );
                status.outs.add( new Ref("local VCYLPARAMS", VCYLPARAMS));
                status.state = PASSED;
                System.out.println(d.getValue("local VCYLPARAMS"));
                return States.PASSED;
            }
        };
    }

    Goal sendStateVCyl2(String ipDevice, int portDevice){
        return new Goal("sendStateVCyl2") {
            @Override
            public Goal.States execute(Data d) {
                Map<String, Map<String, Object>> HCYLPARAMS = (Map<String, Map<String, Object>>) d.getValue("local VCYLPARAMS");
                String agentStatus = HCYLPARAMS.get("VCyl2").get("state").toString();
                UDPClient client = new UDPClient(ipDevice, portDevice, agentStatus);
                System.out.println("Has been sent control signal('" + agentStatus + "') to device:" + ipDevice + portDevice);
                return States.PASSED;
            }
        };
    }

    Goal sendStateGripper1(String ipDevice, int portDevice){
        return new Goal("sendStateGripper1") {
            @Override
            public Goal.States execute(Data d) {
                if ( TimeTrigger.isPending( d, "deadline", 1*1000 ) )
                    return Goal.States.BLOCKED;

                String agentStatus = "true";
                UDPClient client = new UDPClient(ipDevice, portDevice, agentStatus);
                System.out.println("Has been sent control signal('" + agentStatus + "') to device:" + ipDevice + portDevice);
                return States.PASSED;
            }
        };
    }

    public void make( Request r ) {
        Data d = new Data();
        d.set( r.ins );
        d.set( r.outs );
        performGoal( new BDIGoal( "getLengthVCyl2" ), "make", d );
//        performGoal( new BDIGoal( "sendStateVCyl2" ), "make", d );
//        performGoal( new BDIGoal( "sendStateGripper1" ), "make", d );
    }
    
    public void setStatus( String g, Vector<Ref> o, String s ) {
        status = new Status( g, o, s );
    }
    
    public Status getStatus() {
        return status;
    }
    
    public RemoteAgent( String n, String ipDevice4, int portDevice4, String ipDevice5, int portDevice5) {
        super( n );      
        addGoal( getLengthVCyl2(ipDevice4, portDevice4) );
        addGoal( sendStateVCyl2(ipDevice4, portDevice4) );
        addGoal( sendStateGripper1(ipDevice5, portDevice5) );
    }
    
}
