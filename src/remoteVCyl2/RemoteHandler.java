package remoteVCyl2;

import pipe.Pipe;
import pipe.Request;

import static pipe.Request.*;
import static pipe.Status.IDLE;

public class RemoteHandler extends Thread  {

    Pipe pipe;
    RemoteAgent agent;
    
    private String timeStamp() {
        long now = System.currentTimeMillis();
        return String.format( "%tT: ", now );
    }
    
    public RemoteHandler(RemoteAgent ra, Pipe p ) {
        agent = ra;
        pipe = p;
    }
    
    @Override
    public void run() {
        try {
            boolean connected = true;
            while ( connected ) {
                Request r = Request.fromJson( (String) pipe.read() );
                switch( r.type ) {
                    case PERFORM:
                        agent.setStatus ( r.goal, r.outs, IDLE );
                        Thread t = new Thread( () -> { agent.make( r ); });
                        t.start();
                    case STATUS:
                        pipe.write( agent.getStatus().toJson() );
                        break;
                    case CLOSE:
                        connected = false;
                        break;
                }
            }
            pipe.close();
            System.err.println( timeStamp()+" RemoteHandler: Closed connection" );
        }
        catch( Exception e ){
            e.printStackTrace();
            System.exit( 1 );
        }
    }  
}
