package remoteVCyl1;

import com.intendico.data.Ref;
import com.intendico.gorite.BDIGoal;
import com.intendico.gorite.Data;
import com.intendico.gorite.Goal;
import com.intendico.gorite.Performer;
import com.intendico.gorite.addon.TimeTrigger;
import org.json.JSONObject;
import pipe.EndPoint;
import pipe.Request;
import pipe.Status;
import pipe.UDPClient;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import static pipe.Status.PASSED;
import static pipe.Status.STOPPED;

public class RemoteAgent extends Performer {
    
    public Status status;
    
    Goal getLengthVCyl1(String ipDevice, int portDevice) {
        return new Goal( "getLengthVCyl1" ) {
            @Override
            public States execute(Data d) {
                String rp = (String) d.getValue( "remoteVCyl1 process");

                // step 2 takes 1 seconds.
                if ( TimeTrigger.isPending( d, "deadline", 1*1000 ) )
                    return Goal.States.BLOCKED;

                /**GET length VCyl1*/
                UDPClient client = new UDPClient(ipDevice, portDevice, "getLengthVCyl1()");
                String responce = null;
                try{
                    responce = client.sendMessage();
                } catch (IOException e){System.err.println(e);}
                JSONObject jsonObject = new JSONObject(responce);
                int length = jsonObject.getInt("length");
                Map<String, Map<String, Object>> VCYLPARAMS = new HashMap<>();
                VCYLPARAMS.put("VCyl1", new HashMap<>());
                VCYLPARAMS.get("VCyl1").put("length", length);

                System.err.println( "getLengthVCyl1 completed" );

                d.setValue( "remoteVCyl1 machine", "device3" );
                d.setValue( "remoteVCyl1 time", new Integer(2) );
                d.setValue("local VCYLPARAMS", VCYLPARAMS);
                status.outs = new Vector<Ref>();
                status.outs.add( new Ref( "remoteVCyl1 machine", "device3" ) );
                status.outs.add ( new Ref( "remoteVCyl1 time", new Integer( 2 ) ) );
                status.outs.add( new Ref("local VCYLPARAMS", VCYLPARAMS));
                status.state = PASSED;
                System.out.println(d.getValue("local VCYLPARAMS"));
                return Goal.States.PASSED;
            }
        };
    }

    Goal sendStateVCyl1(String ipDevice, int portDevice){
        return new Goal("sendStateVCyl1") {
            @Override
            public Goal.States execute(Data d) {
                if ( TimeTrigger.isPending( d, "deadline", 1*1000 ) )
                    return Goal.States.BLOCKED;

                Map<String, Map<String, Object>> HCYLPARAMS = (Map<String, Map<String, Object>>) d.getValue("local VCYLPARAMS");
                String agentStatus = HCYLPARAMS.get("VCyl1").get("state").toString();
                UDPClient client = new UDPClient(ipDevice, portDevice, agentStatus);
                System.out.println("Has been sent control signal('" + agentStatus + "') to device:" + ipDevice + portDevice);
                return States.PASSED;
            }
        };
    }

    public void make( Request r ) {
        Data d = new Data();
        d.set( r.ins );
        d.set( r.outs );
        performGoal( new BDIGoal( "getLengthVCyl1" ), "make", d );
//        performGoal( new BDIGoal( "sendStateVCyl1" ), "make", d );
    }
    
    public void setStatus( String g, Vector<Ref> o, String s ) {
        status = new Status( g, o, s );
    }
    
    public Status getStatus() {
        return status;
    }
    
    public RemoteAgent( String n, String ipDevice, int portDevice) {
        super( n );      
        addGoal( getLengthVCyl1(ipDevice, portDevice) );
        addGoal( sendStateVCyl1(ipDevice, portDevice) );
    }
    
}
