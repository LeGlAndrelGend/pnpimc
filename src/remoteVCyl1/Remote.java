package remoteVCyl1;

import pipe.EndPoint;
import pipe.Pipe;
import pipe.TextPipe;

import java.net.ServerSocket;
import java.net.Socket;

public class Remote  {
    private static String timeStamp() {
        long now = System.currentTimeMillis();
        return String.format( "%tT: ", now );
    }
    
    public static void main( String args[] ) {
        try{
            System.out.println("In command-line passed next arguments:");
            System.out.println("1. Type of agent (possible: rm - remote): " + args[0]);
            System.out.println("2. Current agent pair IP and port (possible arg format: <192.168.3.1 55532>)): " + args[1] + ":" + args[2]);
            System.out.println("3 Nxt devices ports (in range 9876 - 9881):");
            System.out.println("3.1 Device 3 - VCyl1: " + args[3]);
            String agentType = args[0];
            String hostAgent = args[1];
            int portAgent = Integer.parseInt(args[2]);
            int portDevice3 = Integer.parseInt(args[3]);

            if (agentType.equals("rm") && (portAgent >= 55000 && portAgent <= 60000)){
                RemoteAgent ra = new RemoteAgent( "RA", hostAgent, portDevice3);
                ServerSocket ss = new ServerSocket( portAgent);
                while ( true ) {
                    System.err.println( timeStamp()+" Remote: Waiting for connection" );
                    Socket cs = ss.accept();
                    Pipe p = new TextPipe( cs );
                    p.open();
                    System.err.println( timeStamp()+" Remote: Connection received from " + cs.getInetAddress().getHostName());
                    // handle connection on a separate thread
                    RemoteHandler rh = new RemoteHandler( ra, p );
                    rh.start();
                }
            } else {
                System.out.println("The type of agent is not available or the selected port is not in range (55000-60000), the program will be terminated in 5 sec" );
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.exit(0);
            }
        }
        catch( Exception e ){
            e.printStackTrace();
            System.exit( 1 );
        }
    }
}
