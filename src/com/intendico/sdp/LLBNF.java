/*********************************************************************
Copyright 2012, Ralph Ronnquist.

This file is part of GORITE.

GORITE is free software: you can redistribute it and/or modify it
under the terms of the Lesser GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GORITE is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the Lesser GNU General Public
License along with GORITE.  If not, see <http://www.gnu.org/licenses/>.
**********************************************************************/

package com.intendico.sdp;

import java.lang.reflect.Constructor;
import java.util.Vector;
import java.util.Enumeration;
import java.util.Hashtable;

/**
 * Class LLBNF defines a standard recursive descent BNF, using "<" and
 * ">" to indicate non-terminals, and "'" to indicate terminals.
 */
public class LLBNF extends BNFGrammar {

    /**
     * Utility method to instantiate a Production object. The method
     * first tries to instantiate with the name given, thereafter by
     * assuming its an inner class of the target grammar
     */
    Class findClass(String clsname)
	throws ClassNotFoundException
    {
	ClassNotFoundException ex = null;
	// Try the name as given
	try {
	    return Class.forName( clsname );
	} catch (ClassNotFoundException e) {
	    ex = e;
	}
	// Trying the name as inner class of the target class, or
	// any of the target class base classes.
	for (Class tc = target.getClass(); tc != null;
	     tc = tc.getSuperclass() ) {
		 try {
		     return Class.forName( tc.getName() + "$" + clsname );
		 } catch (ClassNotFoundException e) {
		 }
	}

	// Trying the name as a class of the same package as the
	// target class or any of its base classes
	for (Class tc = target.getClass(); tc != null;
	     tc = tc.getSuperclass() ) {
	    String pkg = tc.getName();
	    int x = pkg.lastIndexOf( '.' );
	    if ( x > 0 ) {
		try {
		    return Class.forName(
			pkg.substring( 0, x ) + "." + clsname );
		} catch (ClassNotFoundException e) {
		}
	    }
	}
	throw ex;
    }

    Object create(String clsname)
	throws Exception
    {
	Class cls = findClass( clsname );
	Class ctx = cls.getDeclaringClass();
	if ( ctx == null ) 
	    return cls.newInstance();
	return cls.getConstructor( 
	    new Class [] { ctx } ).newInstance( new Object [] { target } );
    }

    Object createNamed(String clsname,String name)
	throws Exception
    {
	Class cls = findClass( clsname );
	Class ctx = cls.getDeclaringClass();
	if ( ctx == null ) 
	    return cls.getConstructor( 
		new Class [] { String.class } ).
		newInstance( new Object [] { name } );
	return cls.getConstructor( 
	    new Class [] { ctx, String.class } ).
	    newInstance( new Object [] { target, name } );
    }

    Hashtable grammars = new Hashtable();

    Grammar getGrammar(String grammar)
	throws Exception
    {
	Class c = findClass( grammar );
	grammar = c.getName();
	Grammar g = (Grammar) grammars.get( grammar );
	if ( g == null ) {
	    g = (Grammar) create( grammar );
	    grammars.put( grammar, g );
	}
	return g;
    }

    Link linkGrammar(String rule,String goal,String grammar)
	throws Exception
    {
	return target.new Link( rule, goal, getGrammar( grammar ) );
    }

    class BackLink {

	final Grammar grammar;
	final Link link;

	BackLink(String rule,String goal,String g,Grammar target)
	    throws Exception
	{
	    grammar = getGrammar( g );
	    link = grammar.new Link( rule, goal, target );
	}

	void addRule()
	{
	    grammar.addRule( link );
	}
    }

    BackLink backlinkGrammar(String rule,String goal,String grammar)
	throws Exception
    {
	return new BackLink( rule, goal, grammar, target );
    }

    /**
     * Generic Helper class that tags with a string.
     */
    public static class Tag extends Production {

	public final String tag;
	public final Grammar grammar;

	public Tag(Grammar g,String t)
	{
	    g.super();
	    tag = t;
	    grammar = g;
	}

	public Object action(Vector v)
	    throws Exception
	{
	    RuleAction action = (RuleAction) grammar.rule_actions.get( tag );
	    if ( action != null )
		return action.action( v );
	    if ( v.size() == 0 )
		return tag;
	    v.add( 0, tag );
	    return v;
	}

	public String toString() {
	    StringBuffer s = new StringBuffer();

	    for ( int i=0; i<tokens.length; i++ ) {
		s.append( tokens[i].toString() );
		s.append( " " );
	    }
	    s.append( "# '" );
	    s.append( tag );
	    return s.toString();
	}
    }

    
    Grammar target;

    public LLBNF()
    {
    }

    public void initialisation()
    {
	addRule(
	    new ProductionList(
		"rule",
		new Production [] {
		    new Production( new Token [] {
			new NonTerminal( "identifier" ),
			new Terminal( "::=" ),
			new NonTerminal( "productions" )
		    } ) {
			public Object action(Vector v) {
			    Vector p = (Vector) v.get( 1 );
			    Production [] px = new Production[ p.size() ];
			    return target.new ProductionList(
				(String) v.get( 0 ),
				(Production[]) p.toArray( px ) );
			} },
		    new Production( new Token [] {
			new NonTerminal( "identifier" ),
			new Terminal( "is" ),
			new Terminal( "lexical" ),
			new NonTerminal( "any" )
		    } ) {
			public Object action(Vector v)
			    throws Exception
			{
			    String name = (String) v.get( 0 );
			    String clsname = (String) v.get( 1 );
			    return (Lexical) createNamed( clsname, name );
			}
		    },

		    new Production( new Token [] {
			new NonTerminal( "identifier" ),
			new Terminal( "is" ),
			new Terminal( "linked" ),
			new Terminal( "to" ),
			new NonTerminal( "nonterminal" ),
			new Terminal( "in" ),
			new NonTerminal( "any" )
		    } ) {
			public Object action(Vector v)
			    throws Exception
			{
			    String rule = (String) v.get( 0 );
			    String goal = (String) v.get( 1 );
			    String grammar = (String) v.get( 2 );
			    return linkGrammar( rule, goal, grammar );
			}
		    },

		    new Production( new Token [] {
			new NonTerminal( "identifier" ),
			new Terminal( "of" ),
			new NonTerminal( "any" ),
			new Terminal( "is" ),
			new Terminal( "linked" ),
			new Terminal( "to" ),
			new NonTerminal( "nonterminal" ),

		    } ) {
			public Object action(Vector v)
			    throws Exception
			{
			    String rule = (String) v.get( 0 );
			    String goal = (String) v.get( 2 );
			    String grammar = (String) v.get( 1 );
			    return backlinkGrammar( rule, goal, grammar );
			}
		    }

		} ) );
	addRule(
	    new ProductionList(
		"rules",
		new Production [] {
		    new Production( new Token [] {
			new NonTerminal( "end" ),
		    } ) {
                        public Object action(Vector v) {
			    return v;
			}
		    },
		    new Production( new Token [] {
			new NonTerminal( "rule" ),
			new NonTerminal( "rules" )
		    } ) {
			public Object action(Vector v) {
			    Vector all = (Vector) v.get( 1 );
			    all.add( 0, v.get( 0 ) );
			    return all;
			}
		    }
		} ) );
	addRule(
            new ProductionList(
                "productions",
                new Production [] {
		    new Production( new Token [] {
                        new NonTerminal( "production" ),
                        new Terminal( "|" ),
                        new NonTerminal( "productions" )
                    } ) {
                        public Object action(Vector v) {
			    Vector all = (Vector) v.get( 1 );
			    all.add( 0, v.get( 0 ) );
			    return all;
			}
		    },
                    new Production( new Token [] {
                        new NonTerminal( "production" )
                    } ) {
                        public Object action(Vector v) {
			    return v;
			}
		    }
		} ) );
	addRule(
	    new ProductionList(
		"production",
		new Production [] {
		    new Production( new Token [] {
			new NonTerminal( "tokenlist" ),
			new Terminal( "#" ),
			new Terminal( "'" ),
			new NonTerminal( "any" ),
		    } ) {
			public Object action(Vector v)
			    throws Exception
			{
			    Vector x = (Vector) v.get( 0 );
			    Production p = new Tag(
				target, (String) v.get( 1 ) );
			    p.setTokens(
				(Token[]) x.toArray( new Token[x.size()] ) );
			    return p;
			}
		    },
		    new Production( new Token [] {
			new NonTerminal( "tokenlist" ),
			new Terminal( "#" ),
			new NonTerminal( "any" ),
		    } ) {
			public Object action(Vector v)
			    throws Exception
			{
			    Vector x = (Vector) v.get( 0 );
			    Production p = (Production)
				create( (String) v.get( 1 ) );
			    p.setTokens(
				(Token[]) x.toArray( new Token[x.size()] ) );
			    return p;
			}
		    },
		    new Production( new Token [] {
			new NonTerminal( "tokenlist" )
		    } ) {
			public Object action(Vector v) {
			    Vector x = (Vector) v.get( 0 );
			    return target.new Production(
				(Token[]) x.toArray( new Token [ x.size() ] )
				);
			}
		    }
		} ) );
	addRule(
	    new ProductionList(
		"tokenlist",
                new Production [] {
                    new Production( new Token [] {
			new NonTerminal( "token" ),
			new NonTerminal( "tokenlist" )
		    } ) {
                        public Object action(Vector v) {
			    Vector all = (Vector) v.get( 1 );
			    all.add( 0, v.get( 0 ) );
			    return all;
			}
		    },
                    new Production( new Token [] {
			new NonTerminal( "token" )
		    } ) {
			public Object action(Vector v) {
			    return v;
			}
		    }
		} ) );
	addRule(
            new ProductionList(
                "token",
                new Production [] {
                    new Production( new Token [] {
			new NonTerminal( "nonterminal" ),
		    } ) {
			public Object action(Vector v) {
			    return target.new NonTerminal(
				(String) v.get( 0 ) );
			}
		    },
                    new Production( new Token [] {
                        new NonTerminal( "terminal" ),
		    } ) {
			public Object action(Vector v) {
			    v = (Vector) v.get( 0 );
			    boolean option = "?".equals( v.get( 0 ) );
			    boolean preserve =
				option || "!".equals( v.get( 0 ) );
			    return target.new Terminal(
				(String) v.get( 1 ),
				preserve, option );
			}
		    }
                } ) );
        addRule(
            new ProductionList(
                "nonterminal",
                new Production [] {
                    new Production( new Token [] {
                        new Terminal( "<" ),
                        new NonTerminal( "identifier" ),
                        new Terminal( ">" ),
                    } )
                } ) );
        addRule(
            new ProductionList(
                "terminal",
                new Production [] {
                    new Production( new Token [] {
                        new Terminal( "!", true ),
                        new NonTerminal( "any" )
                    } ),
                    new Production( new Token [] {
                        new Terminal( "'", true ),
                        new NonTerminal( "any" )
		    } ),
                    new Production( new Token [] {
			new Terminal( "?", true ),
                        new NonTerminal( "any" )
                    } )
                } ) );
	addRule( new AnySymbol( "any" ) );
	addRule( new Identifier( "identifier" ) );
	addRule( new End( "end" ) );
    }

    /**
     * Utility method to define rules using LLBNF syntax.
     */
    public Grammar addRules(Grammar g,String rules)
	throws Throwable
    {
	try {
	    //trace = TRACE_STACK;
	    target = g;
	    grammars.put( g.getClass().getName(), target );

	    Vector v = (Vector) parseAndProcess( "rules", rules );
	    for ( Enumeration e = v.elements(); e.hasMoreElements(); ) {
		Object r = e.nextElement();
		if ( r instanceof BackLink )
		    ((BackLink) r).addRule();
		else
		    target.addRule( (Rule) r );
	    }
	    return target;
	} catch (Throwable t) {
	    throw hideParse( t, LLBNF.class.getName() );
	}
    }

    /**
     * A Test main method.
     */
    public static void main(String [] args)
	throws Throwable
    {
	LLBNF g = new LLBNF();
	String rules = g.toString() ;
	Grammar x = g.addRules( new Grammar(), rules );
	System.out.println( x );
    }
}
