/*********************************************************************
Copyright 2012, Ralph Ronnquist.

This file is part of GORITE.

GORITE is free software: you can redistribute it and/or modify it
under the terms of the Lesser GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GORITE is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the Lesser GNU General Public
License along with GORITE.  If not, see <http://www.gnu.org/licenses/>.
**********************************************************************/

package com.intendico.gorite;

import com.intendico.data.Query;
import com.intendico.data.Ref;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Vector;

/**
 * A ControlGoal is achieved by performing all its sub goals in
 * sequence, which results in a control effect on an enclosing
 * parallel execution by terminating all other branches when it
 * succeeds. Thus, the execution either fails, or continues after the
 * enclosing parallel goal.
 *
 * <p> The following code snippet illustrates an optional interrupt,
 * i.e., how to either interrupt or succeed (rather than either
 * interrupt or fail).
 * <pre>
 * new FailGoal( "not interrupting is fine", new Goal [] {
 *     new ControlGoal( "maybe interrupt", new Goal {
 *        // Goal sequence for causing interrupt if achieved successfully
 *     } );
 * }
 * </pre>
 *
 * <p> Note that the ControlGoal effect propagates up to the nearest
 * parallel type goal through {@link BDIGoal} execution, and it
 * therefore is not necessary for the ControlGoal to be explicitly
 * within the sub goal hiearachy of the parallel type goal of
 * concern.
 *
 * <p> In particular, a {@link Plan} of a {@link Team.Role} may
 * include a ControlGoal to express that a {@link TeamGoal} is
 * achieved by any one filler rather than requiring all of them.
 *
 * @see ParallelGoal
 * @see RepeatGoal
 * @see TeamGoal
 * @see FailGoal
 * @see Team.Role
 */
public class ControlGoal extends SequenceGoal {

    /**
     * Constructor.
     */
    public ControlGoal(String n,Goal [] sg) {
	super( n, sg );
    }

    /**
     * Convenience constructor without sub goals.
     */
    public ControlGoal(String n) {
	this( n, null );
    }

    /**
     * Creates and returns an instance object for achieving
     * a ControlGoal.
     */
    public Instance instantiate(String head,Data d) {
	return new ControlInstance( head );
    }

    /**
     * Implements a control action for parallel execution.
     */
    public class ControlInstance extends SequenceInstance {

	/**
	 * Constructor.
	 */
        public ControlInstance(String h) {
            super( h );
        }

        /**
         * Process all subgoals in sequence, then throw a
	 * LoopEndException if success. If a subgoals fail, then fail
	 * without throwing exception.
         */
        public States action(String head,Data d)
            throws LoopEndException, ParallelEndException {
	    States s = super.action( head, d );
	    if ( s == States.PASSED ) {
		throw new ParallelEndException( head );
	    }
	    return s;
	}
    }


}
