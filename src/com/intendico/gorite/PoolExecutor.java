/*********************************************************************
Copyright 2012, Ralph Ronnquist.

This file is part of GORITE.

GORITE is free software: you can redistribute it and/or modify it
under the terms of the Lesser GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GORITE is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the Lesser GNU General Public
License along with GORITE.  If not, see <http://www.gnu.org/licenses/>.
**********************************************************************/

package com.intendico.gorite;

/**
 * This is an Executor extension intended for applications containing
 * multiple models (i.e., Executors), and multiple threads to execute
 * them. The PoolExecutor is tied to a {@link
 * java.util.concurrent.Executor} on concstruction. It adds itself for
 * running whenever it gets runnable by signalling, or any performer
 * ended in STOPPED rather than BLOCKED. This approach allows an
 * execution pool of many threads, where one thread at a time is used
 * for executing all each Executor's performers once, then
 * interleaving the models in between these one-shot runs.
 */
public class PoolExecutor extends Executor {

    /**
     * Cache of the execution queue interface.
     */
    private java.util.concurrent.Executor runner;

    /**
     * Control flag for capturing signal calls during performer
     * execution. This is reset by the {@link #run()} method before it
     * executes performers, and set by the {@link #signal()} method
     * whenever called.
     */
    private boolean signaled = false;

    /**
     * Constructor with a given {@link java.util.concurrent.Executor}.
     */
    public PoolExecutor(java.util.concurrent.Executor x) {
	super( "PoolExecutor" );
	runner = x;
	x.execute( this );
    }

    /**
     * Utility method to "tickle" the executor with, just in case it
     * is idle. Unless marked as running, this Executor is entered
     * into the execution queue (and marked as running).
     */
    synchronized public void signal() {
	signaled = true;
	if ( ! running ) {
	    running = true;
	    runner.execute( this );
	    notifyAll();
	}
    }

    /**
     * Overrides {@link Executor#run()} for a one-shot execution of
     * all performers. If any performer ends in STOPPED rather than
     * BLOCKED, or there is a {@link #signal()} while the execution is
     * in progress, then this Executor is re-entered into the
     * execution pool.
     */
    public void run() {
	signaled = false;
	if ( runPerformersOnce() != Goal.States.BLOCKED ) {
	    runner.execute( this ); // continue asap
	    return;
	}
	boolean b;
	synchronized ( this ) {
	    b = running = signaled;
	}
	if ( b ) {
	    runner.execute( this ); // continue asap
	}
    }
}
