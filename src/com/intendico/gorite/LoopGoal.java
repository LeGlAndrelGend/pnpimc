/*********************************************************************
Copyright 2012, Ralph Ronnquist.

This file is part of GORITE.

GORITE is free software: you can redistribute it and/or modify it
under the terms of the Lesser GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GORITE is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the Lesser GNU General Public
License along with GORITE.  If not, see <http://www.gnu.org/licenses/>.
**********************************************************************/

package com.intendico.gorite;

import com.intendico.data.Query;
import com.intendico.data.Ref;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Vector;

/**
 * A LoopGoal is achieved by means of repeatedly achieving its sub
 * goals in sequence, until eventually an embedded {@link EndGoal} is
 * achieved. The usage pattern for this is as follows:
 *
 * <pre>
 * new LoopGoal( "loop", new Goal [] {
 *     .. // steps in the loop
 *     .. new EndGoal( "break if", new Goal [] {
 *            // optional sub goals for ending the loop if they succeed
 *        } )
 *     .. // more steps in the loop
 * } )
 * </pre>
 *
 * There may be any number of {@link EndGoal} to end the loop although
 * of course only one will take effect. The {@link EndGoal} may be at
 * any level within the LoopGoal goal hierarchy, except nested within
 * another LoopGoal.
 *
 * @see EndGoal
 * @see SequenceGoal
 */
public class LoopGoal extends SequenceGoal {

    /**
     * Constructor.
     */
    public LoopGoal(String n,Goal [] sg) {
	super( n, sg );
    }

    /**
     * Convenience constructor without sub goals.
     */
    public LoopGoal(String n) {
	this( n, null );
    }

    /**
     * Creates and returns an instance object for achieving
     * a LoopGoal.
     */
    public Instance instantiate(String head,Data d) {
	return new LoopInstance( head );
    }

    /**
     * Implements repeated, sequential exception.
     */
    public class LoopInstance extends SequenceInstance {

	/**
	 * Constructor.
	 */
        public LoopInstance(String h) {
            super( h );
        }

	public int count = 0;

        /**
         * Repeatedly process all subgoals in sequence, until a
	 * thrown LoopEndException is caught.
         */
        public States action(String head,Data d)
            throws LoopEndException, ParallelEndException {
	    for ( ;; count++ ) {
		States s;
		try {
		    s = super.action( head + ":" + count, d );
		} catch (LoopEndException e) {
		    // End this loop
		    break;
		}
		if ( s != States.PASSED )
		    return s;
		index = 0;
	    }
	    return States.PASSED;
	}
    }
}
