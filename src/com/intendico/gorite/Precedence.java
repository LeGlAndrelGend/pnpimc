/*********************************************************************
Copyright 2012, Ralph Ronnquist.

This file is part of GORITE.

GORITE is free software: you can redistribute it and/or modify it
under the terms of the Lesser GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GORITE is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the Lesser GNU General Public
License along with GORITE.  If not, see <http://www.gnu.org/licenses/>.
**********************************************************************/

package com.intendico.gorite;

import com.intendico.data.Query;

/**
 * The Precedence interface is intended to be implemented by a {@link
 * Goal} class that wants to suggest its precedence among alternative
 * {@link Plan} options for a {@link BDIGoal} goal.
 */
public interface Precedence {

    /**
     * The precedence method is invoked by {@link
     * BDIGoal.BDIInstance#action} in order to sort the collection of
     * plans such that higher precedence plans are attempted before
     * lower precedence plans.
     *
     * <p> A {@link Goal} without Precedence implementation is
     * assigned the precedence value of 5.
     *
     * <p> Note that this method is invoked for each of any multiple
     * bindings defined by a {@link Plan#context} {@link Query}, with
     * the relevant bindings assigned into the {@link Data} object
     * provided. The same {@link Data} object will be used for the
     * successive {@link #precedence} calls, but with the bindings
     * successively updated to pin point the relevant plan
     * alternative.
     */
    public int precedence(Data d);
}
