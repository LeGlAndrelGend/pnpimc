/*********************************************************************
Copyright 2012, Ralph Ronnquist.

This file is part of GORITE.

GORITE is free software: you can redistribute it and/or modify it
under the terms of the Lesser GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GORITE is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the Lesser GNU General Public
License along with GORITE.  If not, see <http://www.gnu.org/licenses/>.
**********************************************************************/

package com.intendico.gorite;

import com.intendico.data.Inquirable;
import com.intendico.data.Query;
import com.intendico.data.Rule;
import com.intendico.data.Store;
import com.intendico.gorite.addon.Reflector;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;

/**
 * A Capability is a container of {@link Goal} hierarchies, which are
 * accessible by their names, and understood to represent alternative
 * ways in which that named goal may be achieved. It provides lookup
 * context for {@link BDIGoal} goals.
 *
 * <p> A Capability may also contain {@link Rule} objects, which are
 * added by the {@link #addRule} method. The {@link Rule} objects are
 * collated into the owning {@link Performer} {@link
 * Performer#rule_set rule_set} to be applied as part of the goal
 * execution.
 */
public class Capability {

    /**
     * The goals of this capability, clustered by name.
     */
    private Hashtable/*<String,Vector<Goal>>*/ goals =
        new Hashtable/*<String,Vector<Goal>>*/();

    /**
     * The table of inqueriable belief structures of this capability,
     * primarily understood qua the {@link Inquirable} interface. This
     * table is initially built at construction time by {@link
     * #putInquirable(Inquirable)} calls, and by elevating
     * inqueriables of sub capabilities that are added to this
     * capability.  Eventually, there is also a {@link
     * #shareInquirables} invocation, which accepts incoming
     * inqueriables, and the distributes this {@link Inquirable}
     * collection downwards to sub capabilities. In general, all
     * capabilities form the same name-to-store mapping throughout a
     * performer, except where name conflicts arise.
     */
    private Hashtable/*<String,Inquirable>*/ inquirables =
        new Hashtable/*<String,Inquirable>*/();

    /**
     * The inner capabilities of this capability.
     */
    private Vector/*<Capability>*/ inner;

    /**
     * The {@link Rule} objects of this capability.
     */
    private Vector/*<Rule>*/ rules;

    /**
     * The performer that has this capability.
     */
    Performer performer;

    /**
     * This interface is implemented by the deferred text form
     * entities (rules or reflectors).
     */
    public interface Deferred {
	/**
	 * The method by which this entity installs itself.
	 */
	public void install();
    }

    /**
     * The collection of deferred entities. This is set to null by the
     * first {@link #shareInquirables(Collection)} call.
     */
    private Vector/*<Deferred>*/ deferred = new Vector/*<Deferred>*/();

    /**
     * Utility method to install a Deferred object subsequent to the
     * first invokation of {@link #shareInquirables(Collection)}
     */
    public void add(Deferred entity) {
	if ( deferred == null ) {
	    entity.install();
	} else {
	    deferred.add( entity );
	}
    }

    /**
     * Constructs a vector of all goals under the given name, by
     * considering the local table and recursively from inner
     * capabilities.
     */
    public Vector/*<Goal>*/ lookup(String name) {
	Vector/*<Goal>*/ g = new Vector/*<Goal>*/();
	if ( goals.get( name ) != null )
	    g.addAll( (Vector/*<Goal>*/) goals.get( name ) );
	if ( inner != null ) {
	    for ( Iterator/*<Capability>*/ e =
		      inner.iterator(); e.hasNext(); ) {
		g.addAll( ((Capability)e.next()).lookup( name ) );
	    }
	}
	return g;
    }

    /**
     * Add a goal to this capability.
     */
    public void addGoal(Goal g) {
	Vector/*<Goal>*/ v = (Vector/*<Goal>*/) goals.get( g.getGoalName() );
	if ( v == null ) {
	    v = new Vector/*<Goal>*/();
	    goals.put( g.getGoalName(), v );
	}
	v.add( g );
    }

    /**
     * Add a plan to this capability.
     */
    public void addPlan(Plan g) {
	addGoal( g );
    }

    /**
     * Add an inner capability. All {@link #inquirables} of the added
     * capability are added to the {@link #inquirables} of this
     * capability, unless their name starts with underscore, or an
     * equally named {@link Inquirable} is already defined.
     *
     * <p> Note that a capability tree built by adding children before
     * grand children yield a different, less complete elevation of
     * inquirables than if the grand children are added to the
     * children first.
     *
     * <p> When adding a capability late (i.e., after the first
     * shareInquirables invocation, or more precisely when {@link
     * #deferred} is null), then {@link #shareInquirables} is invoked;
     * it's only invoked on the added capability unless an inquirable
     * was gained, in which case {@link #shareInquirables} is invoked
     * on this capability, for distributing the gained inquirables
     * over the whole capability sub tree.
     *
     * @see #shareInquirables(Collection)
     */
    public void addCapability(Capability c) {
	if ( inner == null )
	    inner = new Vector/*<Capability>*/();
	if ( ! inner.contains( c ) )
	    inner.add( c );
	if ( performer != null ) {
	    c.setPerformer( performer );
	}
	boolean extended = false;
	for ( Iterator/*<Inquirable>*/ si = c.inquirables.values().iterator();
	      si.hasNext(); ) {
	    Inquirable inquirable = (Inquirable) si.next();
	    String name = inquirable.getName();
	    if ( ( ! name.startsWith( "_" ) ) &&
		 inquirables.get( name ) == null ) {
		putInquirable( inquirable );
		extended = true;
	    }
	}
	if ( deferred == null ) {
	    if ( extended ) {
		shareInquirables( null );
	    } else {
		c.shareInquirables( inquirables.values() );
	    }
	}
    }

    /**
     * Creates a new {@link Rule} object with given antecedent and
     * consequent {@link Query} objects.
     */
    public void addRule(Query a,Query c) {
	if ( rules == null )
	    rules = new Vector/*<Rule>*/();
	Rule r = new Rule( a, c );
	rules.add( r );
	if ( performer != null ) {
	    if ( Goal.isTracing() )
		System.err.println( "** Installing " + r + " for " + performer );
	    performer.rule_set.add( r );
	}
    }

    /**
     * Utility method to lookup a given goal name.
     */
    public boolean hasGoal(String name) {
	if ( goals.get( name ) != null )
	    return true;
	if ( inner == null )
	    return false;
	for ( Iterator/*<Capability>*/ ci = inner.iterator(); ci.hasNext(); ) {
	    if ( ((Capability) ci.next()).hasGoal( name ) )
		return true;
	}
	return false;
    }

    /**
     * Tells whether or not this capability provides some method or
     * methods for all named goals.
     */
    public boolean hasGoals(String [] gs) {
	if ( gs == null )
	    return true;
	for ( int i = 0; i < gs.length; i++ ) {
	    if ( ! hasGoal( gs[i] ) )
		return false;
	}
	return true;
    }

    /**
     * Returns an Inquirable qua Store, or null if it's not a Store.
     */
    public Store getStore(String name) {
	Object x = inquirables.get( name );
	if ( x instanceof Store )
	    return (Store) x;
	return null;
    }

    /**
     * Returns the Inquirable as mapped, or null if not found.
     */
    public Inquirable getInquirable(String name) {
	return (Inquirable) inquirables.get( name );
    }

    /**
     * Registers an {@link Inquirable} to be mapped. A Capability
     * extension should register all locally created inquirables.
     */
    public void putInquirable(Inquirable inquirable) {
	inquirables.put( inquirable.getName(), inquirable );
    }

    /**
     * Registers an {@link Inquirable} under an alias. A Capability
     * extension should register all locally created inquirables.
     */
    public void putInquirable(String name,Inquirable inquirable) {
	inquirables.put( name, inquirable );
    }

    /**
     * This method extends the inquirables collection with the given
     * collection, then propagates its map downwards to sub
     * capabilities. Finally all {@link Deferred} entities held in
     * {@link #deferred} are installed, before setting {@link
     * #deferred} to null.
     *
     * <p> Note that a {@link Performer} (which is a Capability) will
     * invoke this once the first time it is added to the {@link
     * Executor default executor}. A multi {@link Executor} model may
     * need additional, explicit initial calls to this method in order
     * to establish complete inquirables sharing.
     *
     * @see #addCapability(Capability)
     */
    public void shareInquirables(Collection/*<Inquirable>*/ shared) {
	if ( Goal.isTracing() ) {
	    System.err.println( "** Sharing inquirables in " + this );
	}
	// Install incoming inquirables unless defined locally
	if ( shared != null ) {
	    for ( Iterator/*<Inquirable>*/ si =
		      shared.iterator(); si.hasNext(); ) {
		Inquirable inquirable = (Inquirable) si.next();
		if ( inquirables.get( inquirable.getName() ) == null ) {
		    putInquirable( inquirable );
		}
	    }
	}
	// Propagate all local inquirables to inner capabilities
	if ( inner != null ) {
	    shared = inquirables.values();
	    for ( Iterator/*<Capability>*/ i = inner.iterator();
		  i.hasNext(); ) {
                Capability c = (Capability) i.next();
		c.shareInquirables( shared );
	    }
	}
	// Install deferred entities
	if ( deferred != null ) {
	    for ( Iterator/*<Deferred>*/ di = deferred.iterator();
		  di.hasNext(); ) {
		((Deferred) di.next()).install();
	    }
	}
	deferred = null;
    }

    /**
     * Sets the performer for this capability, and propagates it to
     * all inner capabilities.
     */
    public void setPerformer(Performer p) {
	if ( performer != null ) {
	    if ( performer != p ) {
		// The capability is moved
		throw new Error( "Invalid GORITE model" );
	    }
	    return;
	}
	performer = p;
	if ( rules != null ) {
	    for ( Iterator/*<Rule>*/ i = rules.iterator(); i.hasNext(); ) {
		Rule r = (Rule) i.next();
		performer.rule_set.add( r );
	    }
	}
	if ( inner != null ) {
	    for ( Iterator/*<Capability>*/ i =
		      inner.iterator(); i.hasNext(); ) {
		Capability c = (Capability) i.next();
		c.setPerformer( p );
	    }
	}
	initialize();
    }

    /**
     * Overridable method where to add Capability initialisation
     * code. This method is invoked when the performer is set up, and
     * all inner capabilites are initialized. This base implementation
     * does not do anything.
     */
    public void initialize() {
    }

    /**
     * Returns the performer that has this capability.
     */
    public Performer getPerformer() {
	return performer;
    }

    /**
     * Utility method that creates the goal of establishing a
     * task team.
     */
    public Goal deploy(String name) {
	return new Goal( "deploy " + name ) {
	    public States execute(Data d) {
		String ttn = getGoalName().substring( 7 );
		Team t = (Team) performer;
		Team.TaskTeam tt = t.getTaskTeam( ttn );
		if ( tt == null )
		    throw new Error( "Missing task team '" + ttn +
				     "' in team '" + t.getName() + "'" );
		if ( tt.establish( d ) )
		    return States.PASSED;
		return States.FAILED;
	    }
	};
    }

    /**
     * Adds a {@link Reflector} to this capability. The actual
     * addition is deferred until after the inquirables are shared, at
     * which time the (@link Performer} is known.
     */
    public void addReflector(
	final String goal,final String todo,final Query query) {
	add( new Deferred() {
	    /**
	     * Installs this as a proper {@link Reflector}.
	     */
	    public void install() {
		Reflector r = new Reflector( getPerformer(), goal, todo );
		r.addQuery( query );
	    }
	} );
    }

    /**
     * Returns the {@link #inquirables} collection.
     */
    public Hashtable/*<String,Inquirable>*/ getInquirables() {
	return inquirables;
    }

    /**
     * Return the goals table.
     */
    public Hashtable/*<String,Vector<Goal>>*/ getGoals() {
	return goals;
    }

    /**
     * Return the inner capabilities, if any.
     */
    public Vector/*<Capability>*/ getInner() {
	return inner;
    }
}
