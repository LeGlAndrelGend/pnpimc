/*********************************************************************
Copyright 2012, Ralph Ronnquist.

This file is part of GORITE.

GORITE is free software: you can redistribute it and/or modify it
under the terms of the Lesser GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GORITE is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the Lesser GNU General Public
License along with GORITE.  If not, see <http://www.gnu.org/licenses/>.
**********************************************************************/

package com.intendico.gorite;

import com.intendico.data.Query;
import com.intendico.data.Ref;
import com.intendico.data.Equals;

/**
 * The Plan class is a {@link Goal} that implements {@link Context}
 * and {@link Precedence}.
 *
 * <p> The typical use of Plan object is as the top level {@link Goal}
 * objects that get added to a {@link Capability}.
 *
 * @see BDIGoal
 * @see AnyGoal
 * @see AllGoal
 * @see DataGoal
 * @see TeamGoal
 */
public class Plan extends SequenceGoal implements Context, Precedence {

    /**
     * Holds a plan name. This name is not used for anything within
     * GORITE, but may serve as an identifier for a plan for a
     * developer.
     */
    private String plan_name;

    /**
     * Utility method that creates a context Query for defining a data
     * element value.
     */
    public static Query define(String name,Object value) {
	Ref r = new Ref( name );
	r.set( value );
	return new Equals( new Object [] { r } );
    }

    /**
     * Constructor for a given plan name, goal name and sub goals. The
     * given sub goals comprise the plan body. The goal name is that
     * which the plan is intended to achieve. The plan name is held in
     * {@link #plan_name}.
     */
    public Plan(String pn,String n,Goal [] sg) {
	super( n, sg );
	plan_name = pn;
    }

    /**
     * Constructor for a given plan name, goal name and without sub
     * goals. The goal name is that which the plan is intended to
     * achieve. The plan name is held in {@link #plan_name}.
     */
    public Plan(String pn,String n) {
	this( pn, n, null );
    }

    /**
     * Constructor for a given goal name and sub goals. The given sub
     * goals comprise the plan body. The goal name is that which the
     * plan is intended to achieve.
     */
    public Plan(String n,Goal [] sg) {
	super( n, sg );
    }

    /**
     * Constructor for a given goal name without sub goals. A body may
     * be assigned to this plan at initialisation or later, or
     * alternatively, this plan would be a <i>task</i> by virtue of an
     * overriding {@link Goal#execute} implementation. The goal name
     * is that which the plan is intended to achieve.
     */
    public Plan(String n) {
	this( null, n, null );
    }

    /**
     * Construction for a goal name given as Class. Actual goal name
     * is the name of the Class.
     */
    public Plan(Class goal) {
	this( null, goal.getName(), null );
    }

    /**
     * Construction for a goal name given as Class, with plan
     * name. Actual goal name is the name of the Class.
     */
    public Plan(String name,Class goal) {
	this( name, goal.getName(), null );
    }

    /**
     * Construction for a goal name given as Class, with sub
     * goals. Actual goal name is the name of the Class.
     */
    public Plan(Class goal,Goal [] sg) {
	this( null, goal.getName(), sg );
    }

    /**
     * Construction for a goal name given as Class, with plan name and
     * sub goals. Actual goal name is the name of the Class.
     */
    public Plan(String name,Class goal,Goal [] sg) {
	this( name, goal.getName(), sg );
    }

    /**
     * The default precedence level for goals.
     */
    public final static int DEFAULT_PRECEDENCE = 5;

    /**
     * Default context method.
     */
    public Query context(Data d) throws Exception {
	return null;
    }

    /**
     * Default precedence method. Returns {@link
     * #DEFAULT_PRECEDENCE}.
     */
    public int precedence(Data d) {
	return DEFAULT_PRECEDENCE;
    }

    /**
     * Returns a {@link String} representation of this object.
     */
    public String toString() {
	String n = "Plan:";
	if ( plan_name != null )
	    n += " " + plan_name;
	return n + "\n" + super.toString();
    }

    /**
     * Returns the {@link #plan_name} attribute.
     */
    public String getPlanName() {
	return plan_name;
    }

}
