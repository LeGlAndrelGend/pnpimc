/*********************************************************************
Copyright 2012, Ralph Ronnquist.

This file is part of GORITE.

GORITE is free software: you can redistribute it and/or modify it
under the terms of the Lesser GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GORITE is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the Lesser GNU General Public
License along with GORITE.  If not, see <http://www.gnu.org/licenses/>.
**********************************************************************/

package com.intendico.gorite;

import java.util.Vector;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Observable;
import java.util.Observer;

/**
 * The BranchingGoal class is an intermediate, abstract class that
 * provides common execution support for parallel branches.
 */
abstract public class BranchingGoal extends Goal {

    /**
     * Constructor.
     */
    public BranchingGoal(String n,Goal [] sg) {
	super( n, sg );
    }

    /**
     * Convenience constructor without sub goals.
     */
    public BranchingGoal(String n) {
	this( n, null );
    }

    /**
     * Helper class that implements suport for parallel goal execution
     * with interrupt control. 
     */
    abstract public class MultiInstance extends Instance {

	/**
	 * States of execution; the number of branches remaining.
	 */
	public int counter;

	/**
	 * Currently active branches.
	 */
	public Vector/*<Branch>*/ branches = null;

	/**
	 * Names of data elements that are output from any branch.
	 */
	public HashSet/*<String>*/ outs = new HashSet/*<String>*/();

	/**
	 * Utility method for counting branches when they are created.
	 */
	public synchronized void increment() {
	    counter++;
	}

	/**
	 * Constructor.
	 */
	public MultiInstance(String h) {
	    super( h );
	}

	/**
	 * A class to monitor the combined runnability of all the
	 * branches, by the logic that this intention is runnable if
	 * any of the branch intentions are runnable.
	 */
	class RunnableMonitor extends Observable implements Observer {

	    private boolean some_runnable = false;

	    public void update(Observable x,Object y) {
		if ( ! some_runnable ) {
		    some_runnable = true;
		    setChanged();
		    notifyObservers();
		}
		if ( x != null ) {
		    x.deleteObserver( this );
		}
	    }
	}

	/**
	 * Keeps the monitor for branch runnability state.
	 */
	private RunnableMonitor runnable_monitor = null;

	/**
	 * Utility class to set up runnability monitoring.
	 */
	private void setupMonitor(Data d) {
	    runnable_monitor = new RunnableMonitor();
	    d.setTrigger( runnable_monitor );
	    boolean some = false;
	    for ( Iterator/*<Branch>*/ i =
		      branches.iterator(); i.hasNext(); ) {
		some |= ((Branch)i.next()).setupObserver( d );
	    }
	    if ( some ) {
		removeMonitor( d );
		d.clearTrigger();
	    }
	}

	/**
	 * Utility method to remove the runnability monitoring.
	 */
	private void removeMonitor(Data d) {
	    runnable_monitor = null;
	    for ( Iterator/*<Branch>*/ i =
		      branches.iterator(); i.hasNext(); ) {
                ((Branch)i.next()).removeObserver( d );
	    }
	}

	/**
	 * Branch head and execution.
	 */
	public class Branch {

	    /**
	     * Collecting all outputs.
	     */
	    public HashSet/*<String>*/ outputs;

	    /**
	     * The goal instance to execute.
	     */
            public Instance instance;

	    /**
	     * Branch thread name. This needs to be distinct from the
	     * actual branch Instance in order to join data properly.
	     */
	    public String thread_name;

	    /**
	     * The Data operated on.
	     */
	    public Data data;

	    /**
	     * Propagate cancellation to actual branch.
	     */
	    public void cancel() {
		if ( instance != null ) {
		    instance.cancel();
		}
	    }

	    /**
	     * Constructor, tying the branch to a goal instance.
	     */
	    public Branch(int ix,Instance i,Data d,HashSet/*<String>*/ outs) {
		instance = i;
		data = d;
		outputs = outs;
		increment();
		thread_name = instance.thread_name + "(branch)";
		data.fork( getGoalControl(), ix, thread_name );
	    }

	    /**
	     * Branch main method: performs the associated goal
	     * instance, and manages current "thread name".
	     */
	    public States action()
		throws LoopEndException, ParallelEndException {
		String old = data.setThreadName( thread_name );
		try {
		    States s = instance.perform( data );
		    // TODO: collate all result data for join at end
		    if ( s == States.PASSED ) {
			//data.join( getGoalControl(), outputs, thread_name );
			data.join( getGoalControl(), null, thread_name );
		    } else if ( s == States.FAILED ) {
			data.join( getGoalControl(), null, thread_name );
		    }
		    return s;
		} catch (ParallelEndException e) {
		    //data.join( getGoalControl(), outputs, thread_name );
		    data.join( getGoalControl(), null, thread_name );
		    throw e;
		} finally {
		    data.setThreadName( old );
		}
	    }

	    /**
	     * Utility method to set up the runnable_monitor as
	     * observer of runnability change for this branch, and
	     * then return the current runnability state.
	     */
	    boolean setupObserver(Data d) {
		d.addObserver( instance.thread_name, runnable_monitor );
		return d.isRunning( instance.thread_name );
	    }

	    /**
	     * Utility method to remove the runnable_monitor as
	     * runnability observer.
	     */
	    void removeObserver(Data d) {
		d.deleteObserver( instance.thread_name, runnable_monitor );
	    }
	}

	/**
	 * Cancels the MultiInstance (Parallel or Repeat)
	 */
	public void cancel() {
	    super.cancel();
	    propagateCancel( null );
	}

	/**
	 * Propagates cancel to all branches
	 */
	public void propagateCancel(Branch skip)
	{
	    if ( branches == null )
		return;
	    for ( Iterator/*<Branch>*/ i =
		      branches.iterator(); i.hasNext(); ) {
		Branch b = (Branch) i.next();
		if ( b != skip )
		    b.cancel();
	    }
	}

	/**
	 * Process all subgoals in parallel, and complete when they
	 * complete.
	 */
	public States action(String head,Data d)
            throws LoopEndException, ParallelEndException {
	    if ( branches == null ) {
		branches = new Vector/*<Branch>*/();
		for ( int i = 0; more( i, d ); i++ ) {
		    Instance instance = getBranch( i, head + ":" + i, d );
		    Branch b = new Branch( i, instance, d, outs );
		    branches.add( b );
		}
	    }

	    if ( runnable_monitor != null ) {
		removeMonitor( d );
	    }

	    int icount = 0;
	    int bcount = 0;

	    try {
		while ( branches.size() > 0 ) {
		    States s = ((Branch)branches.get( 0 )).action();
		    if ( s == States.STOPPED || s == States.BLOCKED ) {
			branches.add( branches.remove( 0 ) );
			icount += 1;
			if ( s == States.BLOCKED )
			    bcount += 1;
			if ( icount < branches.size() )
			    continue;
			if ( bcount != icount )
			    return States.STOPPED;
			setupMonitor( d );
			return States.BLOCKED;
		    }
		    icount = bcount = 0;
		    if ( s == States.PASSED ) {
			branches.remove( 0 );
			continue;
		    }
		    propagateCancel( (Branch)branches.get( 0 ) );
		    return s;
		}
	    } catch (LoopEndException e) {
		propagateCancel( null );
		throw e;
	    } catch (ParallelEndException e) {
		propagateCancel( (Branch) branches.get( 0 ) );
		// Discard data from cancelled branches
	    } finally {
		// Signal elevated branch outputs in calling data context.
		for ( Iterator/*<String>*/ i =
			  outs.iterator(); i.hasNext(); ) {
		    String key = (String) i.next();
		    d.ready( key );
		}
	    }
	    // TODO join collated data
	    return States.PASSED;
	}

	/**
	 * The method for determining whether there is another branch.
	 */
        abstract public boolean more(int i,Data d);

	/**
	 * Utility method to obtain the actual branch instance.
	 */
        abstract public Instance getBranch(int i,String head,Data d);

    }

}
