/*********************************************************************
Copyright 2012, Ralph Ronnquist.

This file is part of GORITE.

GORITE is free software: you can redistribute it and/or modify it
under the terms of the Lesser GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GORITE is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the Lesser GNU General Public
License along with GORITE.  If not, see <http://www.gnu.org/licenses/>.
**********************************************************************/

package com.intendico.gorite;

import com.intendico.data.*;
import java.util.Vector;

/**
 * This is a utility class used by BDIGoal execution, to associate a
 * particular binding context with performing a goal.  It is not
 * intended for explicit use within goal hierarchies.
 *
 * <p> The {@link BDIGoal} creates individual ContextualGoal objects
 * for each applicable plan option where the plans {@link
 * Context#context} method returns a {@link Query} that offers
 * multiple bindings (i.e., not null, {@link Context#EMPTY} or {@link
 * Context#FALSE}). Plan options whose {@link Context#context} method
 * returns null are collated without the wrapping of a ContextualGoal
 * object, and plan options whose {@link Context#context} method
 * returns {@link Context#EMPTY} or {@link Context#FALSE} are not
 * applicable.
 *
 * <p> The ContextualGoal captures the entry data settings for the
 * variables as well as the proposed settings that defines the plan
 * option.  It then manages the set up of the proposed settings into
 * the data prior to executing the plan body, and the restoration of
 * these settings if the plan body execution fails. (The variable
 * settings are not restored if the ContextualGoal execution is
 * cancelled)
 *
 * @see BDIGoal
 */
public class ContextualGoal extends Goal implements Precedence {

    /**
     * Holds the Data object for the calling BDIInstance that created this
     * ContextualGoal.
     */
    public Data goal_data;

    /**
     * Holds the actual {@link Ref} objects of the context
     * {@link Query}.
     */
    public Vector/*<Ref>*/ variables;

    /**
     * Holds {@link Ref} objects with values representing this
     * invocation context.
     */
    public Vector/*<Ref>*/ context;

    /**
     * The original assignments for the variables.
     */
    public Vector/*<Ref>*/ original;

    /**
     * Two ContextualGoal are equal if they are of the same goal and
     * have the same context.
     */
    public boolean equals(Object c) {
	return c instanceof ContextualGoal && equals( (ContextualGoal) c );
    }

    /**
     * Two ContextualGoal are equal if they are of the same goal
     * and have the same context.
     */
    public boolean equals(ContextualGoal c) {
	if ( ! c.getGoalSubgoals()[0].equals( getGoalSubgoals()[0] ) )
	    return false;
	if ( context == null ) {
	    return c.context == null;
	}
	return Ref.equals( c.context, context );
    }

    /**
     * Constructor.
     */
    public ContextualGoal(Vector/*<Ref>*/ orig,Vector/*<Ref>*/ vars,
	Goal goal,Data data) {
	super( goal.getGoalName() );
	goal_data = data;
	original = orig;
	variables = vars;
	if ( variables != null )
	    context = Ref.copy( variables );
	setGoalSubgoals( new Goal[] { goal } );
    }

    /**
     * A wrapping for invoking precedence of the wrapped goal. If the
     * wrapped goal provides a precedence method, then it is invoked
     * using the cached {@link #goal_data}, transiently establishing
     * the invocation context, which is restored upon return.
     */
    public int precedence(Data data) {
	if ( ! ( getGoalSubgoals()[ 0 ] instanceof Precedence ) )
	    return Plan.DEFAULT_PRECEDENCE;
	if ( context == null )
	    return ((Precedence) getGoalSubgoals()[ 0 ])
		.precedence( goal_data );
	goal_data.set( context );
	Ref.bind( variables, context );
	int x = ((Precedence) getGoalSubgoals()[ 0 ]).precedence( goal_data );
	Ref.bind( variables, original );
	goal_data.get( context, false );
	return x;
    }

    /**
     * Return the intention instance to be executed.
     */
    public Instance instantiate(String head,Data d) {
	// Assuming data == d without checking it
	return new ContextualGoalInstance( head, d );
    }

    /**
     * Utility class to represent the intention
     */
    public class ContextualGoalInstance extends Instance {

	/**
	 * The target intention instance
	 */
	public Instance instance;

	/**
	 * Constructor. This established the invocation context in the
	 * intention {@link Data}.
	 */
	public ContextualGoalInstance(String h,Data d) {
	    super( h );
	    if ( context != null ) {
		d.set( context );
		Ref.bind( variables, context );
	    }
	    String sub = context != null? Ref.toString( context ) : "{}";
	    instance = getGoalSubgoals()[ 0 ].instantiate( h + sub, d );
	}

	/**
	 * Control callback whereby the intention gets notified that
	 * it is cancelled. This will forward the cancellation to the
	 * currently progressing instance, if any.
	 */
	public void cancel() {
	    if ( instance != null )
		instance.cancel();
	}

	/**
	 * Excution of this goal, which means to execute the wrapped
	 * goal. If successful, the invocation {@link Ref} objects are
	 * updated from the final {@link Data}.  Execution catches
	 * LoopEndException, but not ParallelEndException, and makes
	 * it cause a failure rather then propagate up. If execution
	 * fails, the {@link Data} context is restored by removing the
	 * local context.
	 */
	public States action(String head,Data d)
	    throws LoopEndException, ParallelEndException {
	    States s = States.FAILED;
	    try {
		s = instance.perform( d );
	    } catch (LoopEndException e) {
		s = States.FAILED;
	    } catch (ParallelEndException e) {
		s = States.FAILED;
		throw e; // Re-thrown
	    } finally {
		if ( variables != null ) {
		    if ( s == States.PASSED ) {
			d.get( variables, true );
		    } else if ( s == States.FAILED ) {
			Ref.bind( variables, original );
			d.get( context, false );
		    }
		}
	    }
	    return s;
	}

    } // End of class ContextualGoalInstance

    /**
     * Textual representation of this goal.
     */
    public String toString() {
	return ( context != null? context.toString() : "{}" ) +
	    getGoalSubgoals()[ 0 ].toString();
    }
}
