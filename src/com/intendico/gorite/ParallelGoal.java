/*********************************************************************
Copyright 2012, Ralph Ronnquist.

This file is part of GORITE.

GORITE is free software: you can redistribute it and/or modify it
under the terms of the Lesser GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GORITE is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the Lesser GNU General Public
License along with GORITE.  If not, see <http://www.gnu.org/licenses/>.
**********************************************************************/

package com.intendico.gorite;

import com.intendico.data.Query;
import com.intendico.data.Ref;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Vector;

/**
 * A ParallelGoal is achieved by means of achieving all its sub goals
 * in parallel. If any sub goal execution fails, then all other sub
 * goal executions are interrupted by means of throwing an {@link
 * java.lang.InterruptedException InterruptedException}, and
 * thereafter the parallel goal execution fails (when all the sub goal
 * executions have terminated).
 *
 * <p> A ParallelGoal execution can also be interrupted to end in
 * success, by means of throwing a {@link ParallelEndException} within
 * one of its branches. In this case all other sub goal executions are
 * interrupted by means of throwing an {@link
 * java.lang.InterruptedException InterruptedException}, and
 * thereafter the parallel goal execution succeeds (when all the sub
 * goal executions have terminated).
 *
 * <p> See also {@link ControlGoal}, which throws a {@link
 * ParallelEndException} if its sub goals succeed. The following is a
 * code snippet to illustrate the use of an embedded {@link
 * ControlGoal} for an optional premature interupt of an indefinite
 * intention.
 * 
 * <pre>
 * new ParallelGoal( ..., new Goal [] {
 *     new BDIGoal( "do something indefinitely" ),
 *     new FailGoal( "not interrupting is fine", new Goal [] {
 *         new ControlGoal( "interrupt if desired", new Goal [] {
 *             new BDIGoal( "decide if and when to interrupt" )
 *         }
 *     }
 * } )
 * </pre>
 */
public class ParallelGoal extends BranchingGoal {

    /**
     * Constructor.
     */
    public ParallelGoal(String n,Goal [] sg) {
	super( n, sg );
    }

    /**
     * Convenience constructor without sub goals.
     */
    public ParallelGoal(String n) {
	this( n, null );
    }

    /**
     * Creates and returns an instance object for achieving
     * a ParallelGoal.
     */
    public Instance instantiate(String head,Data d) {
	return new ParallelInstance( head );
    }

    /**
     * Implements parallel execution of sub goals.
     */
    public class ParallelInstance extends MultiInstance {

	/**
	 * Constructor.
	 */
        public ParallelInstance(String h) {
            super( h );
        }

	/**
	 * There is one branch for each sub goal.
	 */
	public boolean more(int i,Data d) {
	    return getGoalSubgoals() != null && i < getGoalSubgoals().length;
	}

	/**
	 * The sub goal branch is obtained by instantiating the sub
	 * goal.
	 */
	public Instance getBranch(int i,String head,Data d) {
	    return getGoalSubgoals()[ i ].instantiate( head, d );
	}
    }
}
