/*********************************************************************
Copyright 2012, Ralph Ronnquist.

This file is part of GORITE.

GORITE is free software: you can redistribute it and/or modify it
under the terms of the Lesser GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GORITE is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the Lesser GNU General Public
License along with GORITE.  If not, see <http://www.gnu.org/licenses/>.
**********************************************************************/

package com.intendico.gorite.addon;

import com.intendico.gorite.*;
import com.intendico.gorite.Performer.TodoGroup;
import java.util.Vector;

/**
 * This plan is a {@link TodoGroup} meta goal handler to cycle through
 * the stacked intentions, by putting the top one last, except when it
 * is newly added. The actual goal name for the meta goal is given at
 * construction time, and unless it is the default {@link
 * Performer#TODOGROUP_META_GOAL}, the actual {@link TodoGroup}
 * instances to be managed by this goal need to be set up explicitly
 * using {@link Performer#addTodoGroup}.
 *
 * <p> Usage example:
 * <pre>
 * new Performer( "example" ) {
 *     addGoal( new TodoGroupRoundRobin( "round robin todogroup meta goal" ) );
 *     addTodoGroup( "example", "round robin todogroup meta goal" );
 * }
 * </pre>
 *
 * <p> Another usage example, to make it the default todogroup meta goal:
 * <pre>
 * new Performer( "example" ) {
 *     addGoal( new TodoGroupRoundRobin( TODOGROUP_META_GOAL ) );
 * }
 * </pre>
 */
public class TodoGroupRoundRobin extends Goal {

    /**
     * Constructor for providing {@link TodoGroup} management through
     * the named meta goal.
     */
    public TodoGroupRoundRobin(String name) {
	super( name );
    }

    /**
     * Overrides {@link Goal#execute} to provide the meta goal
     * implementation. This expects a {@link Data} object with
     * elements named by {@link Performer#TODOGROUP}, "coming" and
     * "top". The implementation cycles the todogroup unless the "top"
     * intention is in the "coming" vector.
     */
    public States execute(Data d) {
	TodoGroup todogroup = (TodoGroup) d.getValue( Performer.TODOGROUP );
	Vector coming = (Vector) d.getValue( "coming" );
	Goal.Instance top = (Goal.Instance) d.getValue( "top" );
	if ( ! coming.contains( top ) )
	    todogroup.rotate();
	return States.PASSED;
    }
}
