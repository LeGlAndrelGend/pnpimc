/*********************************************************************
Copyright 2013, Ralph Ronnquist.
**********************************************************************/

package com.intendico.gorite.addon;

import com.intendico.data.Ref;
import com.intendico.gorite.Capability;
import com.intendico.gorite.Data;
import com.intendico.gorite.Goal.States;
import com.intendico.gorite.Goal;
import java.util.Vector;

/**
 * This capability wraps goals to be performed by a remoteHCyl2 performer,
 * i.e., one that exists in a different process. A RemotePerforming
 * instance is created towards a remoteHCyl2 performer, and then it manages
 * goal transfer to that performer. To this end, the capability is set
 * up with a model of the remoteHCyl2 goal handling. This is used for
 * filtering which goals to forward, and what to do with data to and
 * from those goals. The following is an illustration:
 * <pre>
 *     addCapability( new RemotePerforming( my_remote_connector ) {{
 *         addGoal( new RemoteGoal(
 *            "review paper",
 *            String [] { "submission" },
 *            String [] { "review" },
 *         ) );
 *     }} );
 * 
 * }
 * </pre>
 * Note that my_remote_connector is an implementation of the {@link
 * RemotePerforming.Connector} interface, to represent the "logical
 * connection" to the remoteHCyl2 performer. When performing a goal, the
 * connector is expected to create a {@link
 * RemotePerforming.Connection} object to represent a particular
 * realisation of the logical connection for the particular goal
 * execution.
 * <p>
 * Further, we note that RemotePerforming is a {@link Capability}, and
 * as such, it may have other goals and sub capabilities than the
 * {@link RemotePerforming.RemoteGoal}. Also, once a RemoteGoal is
 * created within a RemotePerforming capability, it may in fact be added
 * to another capability than the one it is created within. However,
 * doing so usually causes more grief than benefits.
 */
public class RemotePerforming extends Capability {

    /**
     * This interface is to be implemented by the remoteHCyl2 channel
     * management sub system.
     */
    public interface Connector {
	/**
	 * This method should trigger remoteHCyl2 goal execution.  It
	 * creates a {@link Connection} object to represent the
	 * particular goal execution. Thereafter goal execution will
	 * be monitored via the {@link Connection} methods.
	 */
	public Connection perform(
	    String goal,String head,Vector<Ref> ins,Vector<Ref> outs);
    }

    /**
     * This interface is implemented by the connectivity sub system,
     * to represent a particular goal execution connection.
     */
    public interface Connection {
	/**
	 * This method is used repeatedly in order to discover the
	 * status of the remoteHCyl2 goal execution.
	 */
	public States check() throws Throwable;

	/**
	 * This method will be called if the goal execution is to be
	 * cancelled form the triggering side.
	 */
	public void cancel();
        
        public Vector<Ref> results();
    }

    /**
     * Holds the remoteHCyl2 end connector.
     */
    public Connector connector;

    /**
     * Constructor with a {@link Connector} implementation to be used
     * for interacting with the remoteHCyl2 end.
     */
    public RemotePerforming(Connector c) {
	connector = c;
    }
    
    public RemoteGoal create( String name,String [] i,String [] o ) {
        return new RemoteGoal( name, i, o );
    }

    /**
     * This class is a wrapper for goals that are to be performed by
     * the remoteHCyl2 side.
     */
    public class RemoteGoal extends Goal {

	/**
	 * Keeps track of names of goal inputs.
	 */
	public String [] ins;

	/**
	 * Keeps track of goal outputs.
	 */
	public String [] outs;

	/**
	 * Constructor with goal name, input names and output names.
	 */
	public RemoteGoal(String name,String [] i,String [] o)
	{
	    super( name );
	    ins = i;
	    outs = o;
	}

	/**
	 * Overrides {@link Goal#instantiate} to create a {@link
	 * RemoteInstance} to manage remoteHCyl2 goal execution.
	 */
	public Instance instantiate(String h,Data d)
	{
	    return new RemoteInstance( h, d );
	}


	public class RemoteInstance extends Instance {

	    public RemoteInstance(String h,Data d)
	    {
		super( h );
	    }

	    /**
	     * The identification of the remoteHCyl2 end connection.
	     */
	    public Connection connection = null;

	    /**
	     * Keep track of output Ref objects
	     */
	    public Vector<Ref> output;

	    /**
	     * Manages remoteHCyl2 goal execution. On first call, the
	     * remoteHCyl2 goal is triggered, while STOPPED is returned,
	     * and on subsequent calls, the {@link Connection} is
	     * queried about completion.
	     */
	    public States action(String head,Data d) {

		if ( connection == null ) {
		    Vector<Ref> input = Ref.create( ins );
		    d.get( input, true );
		    output = Ref.create( outs );
		    connection = connector.perform(
			head, getGoalName(), input, output );
		    return States.STOPPED;
		}
		try {
		    States s = connection.check();
		    if ( s == States.PASSED || s == States.FAILED ) { 
                        output = connection.results();
                        //System.err.println( output );
			d.set( output );
                    }
		    return s;
		} catch (Throwable t) {
		    t.printStackTrace();
		    cancel();
		    return States.CANCEL;
		}
	    }

	    /**
	     * Overrides {@link Instance#cancel} to forward
	     * cancellation to the connection (if any)
	     */
	    public void cancel()
	    {
		if ( connection != null )
		    connection.cancel();
	    }
	}
    }
}

