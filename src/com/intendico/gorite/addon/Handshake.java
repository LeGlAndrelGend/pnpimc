/*********************************************************************
Copyright 2012, Ralph Ronnquist.

This file is part of GORITE.

GORITE is free software: you can redistribute it and/or modify it
under the terms of the Lesser GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GORITE is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the Lesser GNU General Public
License along with GORITE.  If not, see <http://www.gnu.org/licenses/>.
**********************************************************************/

package com.intendico.gorite.addon;

import com.intendico.gorite.Goal;
import com.intendico.gorite.Data;
import java.util.Observable;

/**
 * Utility class to deal with request-response protocols between
 * in-core performers with BellBoy capability.  The requester would
 * include a {@link Handshake#create()} step in the requesting plan,
 * with data elements "responder" and "request" set up, and the
 * responder, who gets a Handshake object as BellBoy percept, uses the
 * {@link Handshake#reply(Object)} method to issue their reply.
 *
 * <p> Note: a null response is not a response, but will make the
 * requester keep waiting.
 */
public class Handshake extends Observable {

    /**
     * The request message of this Handshake.
     */
    public Object message;

    /**
     * The response message of this Handshake.
     */
    public Object response;

    /**
     * Java method to set the reply and notify to the requester
     * intention.
     */
    public void reply(Object r) {
        response = r;
        setChanged();
        notifyObservers();
    }

    /**
     * Returns a Goal object for performing a Handshake. It uses the
     * data elements "responder" and "request" for input details, the
     * data element "pending handshake" transiently, and the data
     * element "response" for output, i.e. the reply object.
     *
     * Note that all incoming "pending handshake" values are
     * forgotten, but not upon a completed handshake.
     */
    public static Goal create() {
	return new Goal( "Perform handshake" ) {
	    /**
	     * Issue the request when the goal is instantiated.
	     */
	    public Instance instantiate(String head,Data data) {
		String who = (String) data.getValue( "responder" );
		Handshake h = new Handshake();
		h.message = data.getValue( "request" );
		data.forget( "pending handshake" );
		if ( BellBoy.send( who, h ) ) {
		    data.setValue( "pending handshake", h );
		}
		return super.instantiate( head, data );
	    }

	    /**
	     * Keep blocking until there is a non-null reply
	     */
	    public States execute(Data data) {
		Handshake h = (Handshake) data.getValue( "pending handshake");
		if ( h == null ) {
		    return States.FAILED;
		}
		if ( h.response == null ) {
		    data.setTrigger( h );
		    return States.BLOCKED;
		}
		data.setValue( "response", h.response );
		return States.PASSED;
	    }
	};
    }
}
