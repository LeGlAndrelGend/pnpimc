/*********************************************************************
Copyright 2012, Ralph Ronnquist.

This file is part of GORITE.

GORITE is free software: you can redistribute it and/or modify it
under the terms of the Lesser GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GORITE is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the Lesser GNU General Public
License along with GORITE.  If not, see <http://www.gnu.org/licenses/>.
**********************************************************************/

package com.intendico.gorite;

import com.intendico.data.Query;
import com.intendico.data.Ref;

import java.util.Enumeration;
import java.util.Formatter;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;
import java.util.Observable;
import java.util.Observer;

/**
 * This is a container class for data elements that goals use and
 * produce.
 *
 * <p> The data that a business process is instantiated for has an
 * internal structure which allows references to individual data
 * elements, and that a data element is either singular or a plurality
 * of other data elements.
 *
 * <p> A model includes data references, primarily in connection with
 * task goals where the data references identify the data elements
 * that the associated tasks are to operate on, but also in connection
 * with repeat goals, which get instantiated for the constituent
 * elements of a plural element.
 *
 * <p> Any actual transformation of data is done in tasks, and the
 * business process model is limited to data reference.
 *
 * <p> A task is associated with two collections of data elements,
 * known as inputs and results. The association is defined by means of
 * using data element names, which are symbols that refer to
 * individual or groups of data elements.
 *
 * <p> A data object is a container for data references, which are
 * names of inidividual or groups of data elements.
 */
public class Data {

    /**
     * This is a base class for data elements.
     */
    public class Element {

	/**
	 * The name of this data element.
	 */
	public final String name;

	/**
	 * The values of this data element.
	 */
	public Vector/*<Object>*/ values = new Vector/*<Object>*/();

	/**
	 * Constructor with initial value
	 */
	public Element(String n,Object v) {
	    name = n;
	    if ( v != null )
		values.insertElementAt( v, 0 );
	}

	/**
	 * Constructor.
	 */
	public Element(String n) {
	    this( n, null );
	}

	/**
	 * Tracking whether this element is ready or not.
	 */
	public boolean ready = false;

	/**
	 * Mark data as ready.
	 */
	public synchronized void markReady() {
	    ready = true;
	}

	/**
	 * Utility method to set value and mark ready.
	 */
	public void set(Object v)
	{
	    set( v, true );
	}

	/**
	 * Utility method to set value and maybe mark ready.
	 */
	public void set(Object v,boolean mark)
	{
	    values.insertElementAt( v, 0 );
	    if ( goldfish )
		forget( 1 );
	    if ( mark )
		markReady();
	}

	/**
	 * Utility method to return top of values.
	 */
	public Object get() {
	    return values.size() > 0? values.get( 0 ) : null ;
	}

	/**
	 * Utility method to return and remove top of values.
	 */
	public Object pop() {
	    if ( values.size() == 0 )
		return null;
	    return values.remove( 0 );
	}

	/**
	 * Utility method to forget all but N values. A given N less
	 * than 1 results in silently using 1.
	 */
	public void forget(int n) {
	    if ( n >= 0 ) {
		while ( values.size() > n ) {
		    values.remove( n );
		}
	    } else {
		while ( n++ < 0 && values.size() > 0 ) {
		    values.remove( 0 );
		}
	    }
	}

	/**
	 * Returns the i:th sub element, if any, or null otherwise.
	 */
	public Object get(int i)
	{
	    return ( i < values.size() )? values.get( i ): null;
	}

	/**
	 * Returns the number of values.
	 */
	public int size() {
	    return values.size();
	}

	/**
	 * Text representation of this data element.
	 */
	public String toString() {
	    /********* 1.5
	    Formatter f = new Formatter( new StringBuilder() );
	    f.format( "\"%s\" ", name );
	    if ( ready ) {
		f.format( "%s", values.toString() );
	    } else {
		f.format( "[not ready]" );
	    }
	    return f.toString();
	    **********/
	    StringBuilder s = new StringBuilder();
	    s.append( "\"" );
	    s.append( name );
	    s.append( "\" " );
	    if ( ready ) {
		s.append( values.toString() );
	    } else {
		s.append( "[not ready]" );
	    }
	    return s.toString();
	}
    }

    /**
     * Utility method to attach an observer to a given observable, to
     * be used for triggering continuation of blocked intention.
     */
    public void setTrigger(Observable s) {
	getBindings().setTrigger( s );
    }

    /**
     * Utility method to attach an observer to a given query, to be
     * used for triggering continuation of blocked intention. The
     * query is reset and re-tested upon notification, and if true,
     * the blocked intention is allowed to continue.
     */
    public void setTrigger(Query q) {
	getBindings().setTrigger( q );
    }

    /**
     * Utility method to remove a trigger.
     */
    public void clearTrigger() {
	getBindings().clearTrigger();
    }

    /**
     * Utility method to check whether a goal instance execution is
     * running or not. All instance executions are considered running
     * unless there a trigger is set via a {@link #setTrigger
     * setTrigger} call, and still armed.
     */
    public boolean isRunning(String thread_name) {
	return ! ((Bindings) bindings.get( thread_name )).monitoring ;
    }

    /**
     * Utility method to attach an observer on the intention-local
     * bindings. The observer will then be notified when any trigger
     * (see {@link #setTrigger setTrigger}) fires.
     */
    public void addObserver(String thread_name,Observer x) {
	((Bindings) bindings.get( thread_name )).addObserver( x );
    }

    /**
     * Utility method to remove a trigger observer.
     */
    public void deleteObserver(String thread_name,Observer x) {
	((Bindings) bindings.get( thread_name )).deleteObserver( x );
    }
    
    /**
     * A class for allowing transient name space split.
     */
    public class Bindings extends Observable {

	/**
	 * The intention thread name that caused this Bindings
	 * object.
	 */
	private String name;

	/**
	 * A bindings-local flag to indicate that the associated
	 * intention is has an unblocking monitor.
	 */
	public boolean monitoring = false;

	/**
	 * Utility method to clear the monitoring flag.
	 */
	void clearTrigger() {
	    monitoring = false;
	}

	/**
	 * Utility method to clear the monitoring flag and propagate
	 * notification to any observer.
	 */
	void clearMonitoring() {
	    if ( monitoring ) {
		if ( Goal.isTracing() ) {
		    System.err.println(
			"** Clearing monitoring  for " + name );
		}
		monitoring = false;
		setChanged();
		notifyObservers();
		setFlag(); // and wake executor....
		Performer p = (Performer) getValue( Goal.PERFORMER );
		if ( p != null ) {
		    p.signalExecutor();
		}
	    }
	}

	/**
	 * Utility method to attach an Observer to the given
	 * Observable, to capture its notification as a trigger.
	 */
	public void setTrigger(Observable s) {
	    monitoring = true;
	    s.addObserver( new Observer() {
		    public void update(Observable x,Object y) {
			if ( Goal.isTracing() ) {
			    System.err.println(
				"** Observer updated by " + x +
				" for " + name );
			}
			if ( monitoring )
			    clearMonitoring();
			x.deleteObserver( this );
		    }
		} );
	}

	/**
	 * Utility class to observe a query, to allow an intention to
	 * continue when the query becomes true.
	 */
	private class QueryMonitor implements Observer {

	    /**
	     * The query being observed.
	     */
	    Query query;

	    /**
	     * Constructor.
	     */
	    QueryMonitor(Query q) {
		monitoring = true;
		query = q;
		if ( q != null )
		    q.addObserver( this );
		update( null, null );
	    }

	    /**
	     * The Observer callback method. This will call reset()
	     * and next() on the given query, and optionally reset
	     * monitoring and call setFlag().
	     */
	    synchronized public void update(Observable x,Object y) {
		if ( Goal.isTracing() ) {
		    System.err.println(
			"** Query " + query + " updated by " + x );
		}
		if ( ! monitoring ) {
		    if ( query != null )
			query.deleteObserver( this );
		    return;
		}
		boolean test = false;
		try {
		    if ( Goal.isTracing() ) {
			System.err.println( "** Query reset on " + query );
		    }
		    if ( query != null )
			query.reset();
		    if ( Goal.isTracing() ) {
			System.err.println( "** Query next on " + query );
		    }
		    test = ( query != null )? query.next() : true;
		    if ( Goal.isTracing() ) {
			System.err.println(
			    "** Query " + query + " is " + test );
		    }
		} catch (Exception e) {
		    e.printStackTrace();
		}
		if ( test ) {
		    clearMonitoring();
		    if ( query != null )
			query.deleteObserver( this );
		}
		if ( Goal.isTracing() ) {
		    System.err.println(
			"** Query data: " + Data.this.toString() );
		}
	    }

	}

	/**
	 * Utility method to attach an Observer to the given Query, to
	 * capture its becoming true as a trigger.
	 */
	public void setTrigger(Query q) {
	    new QueryMonitor( q );
	}

	/**
	 * This is the transient data store for an execution path.
	 */
	public Hashtable/*<String,Element>*/ elements =
	    new Hashtable/*<String,Element>*/();

	/**
	 * Link to a prior Bindings object; the base of this one.
	 */
	Bindings prior = null;

	/**
	 * Constructor.
	 */
	public Bindings(String n,Bindings p)
	{
	    name = n;
	    prior = p;
	}

	/**
	 * Special constructor to transiently bind a given name to its
	 * i:th sub element, if it exists.
	 */
	public Bindings(String n,Bindings p,String name,int i)
	{
	    this.name = n;
	    prior = p;
	    if ( name != null ) {
		Element e = Data.this.find( p, name );
		if ( e != null ) {
		    Object v = e.get( i );
		    if ( v != null ) {
			Element x = new Element( name, v );
			x.markReady();
			elements.put( name, x );
		    }
		}
	    }
	}

	/**
	 * Returns the named data elements, if any, or null otherwise.
	 */
	public Element find(String name)
	{
	    return (Element) elements.get( name );
	}

	/**
	 * Utility method to create an Element instance.
	 */
	public Element create(String name) {
	    Element e = (Element) elements.get( name );
	    if ( e == null ) {
		e = new Element( name );
		elements.put( name, e );
	    }
	    return e;
	}

	/**
	 * Utility method that pushes all data except the given one
	 * into the prior bindings.
	 */
	public void push(String except,HashSet/*<String>*/ h) {
	    if ( prior == null )
		return;
	    boolean gf = goldfish;
	    goldfish = false;
	    for ( Enumeration/*<String>*/ i = elements.keys();
		  i.hasMoreElements(); ) {
		String key = (String) i.nextElement();
		if ( key.equals( except ) )
		    continue;
		h.add( key );
		Element e = prior.create( key );
		e.set( ((Element) elements.get( key )).get(), false );
	    }
	    goldfish = gf;
	}

	/**
	 * Returns a String representation of this Bindings object.
	 */
	public String toString() {
	    /************ 1.5
	    Formatter f = new Formatter( new StringBuilder() );
	    f.format( "Data.Bindings %s", name );
	    if ( prior != null )
		f.format( " [prior = %s]", prior.name );
	    for (Iterator/*<String>* / i =
		   elements.keySet().iterator(); i.hasNext(); ) {
	    f.format( "\n  %s", elements.get( i.next() ).toString() );
	    }
	    return f.toString();
	    ***************/

	    StringBuilder s = new StringBuilder();
	    s.append( "Data.Bindings " );
	    s.append( name );
	    if ( prior != null ) {
		s.append( " [prior = " );
		s.append( prior.name );
		s.append( "]" );
	    }
	    for (Iterator/*<String>*/ i =
		     elements.keySet().iterator(); i.hasNext(); ) {
		s.append( "\n  " );
		s.append( elements.get( i.next() ).toString() );
	    }
	    return s.toString();
	}
    }

    /**
     * Flags this data into goldfish mode, which means for data
     * elements to forget past values when new values are set.
     */
    public boolean goldfish = false;

    /**
     * Keeps the 'global' bindings object.
     */
    public Hashtable/*<String,Bindings>*/ bindings =
        new Hashtable/*<String,Bindings>*/();

    /**
     * Utility method that links a thread name with the current
     * bindings. This may be overridden later by parallel goals, which
     * get their local bindings.
     */
    public void link(String thread_name) {
	bindings.put( thread_name, getBindings() );
    }

    /**
     * Utility method to remove a bindings entry.
     */
    public void dropBindings(String thread_name) {
	bindings.remove( thread_name );
    }

    /**
     * Utility method to locate an element from within a "thread".
     */
    public Element getLocalElement(String thread_name,String name) {
	Bindings b = (Bindings) bindings.get( thread_name );
	return find( b != null? b : getBindings(), name );
    }

    /**
     * Utility method that creates a thread-local binding for the
     * given name to its i:th sub element. If name is null, or the
     * i:th sub element missing, a thread local bindings object is
     * still created, but without initialising the name.
     */
    public synchronized void fork(String name,int i,String thread_name)
    {
	bindings.put( thread_name,
		      new Bindings( thread_name, getBindings(), name, i ) );
    }

    /**
     * Utility method to push data. All names of the given HashSet
     * except the exception are pushd down to be sub elements of the
     * equally named element of the prior bindings object.
     */
    public synchronized void join(
	String except,HashSet/*<String>*/ h,String thread_name) {
	if ( h != null )
	    getBindings().push( except, h );
	dropBindings( thread_name );
    }
	
    /**
     * Obtains the bindings object chain for the calling thread.
     */
    public Bindings getBindings() {
	Bindings b = (Bindings) bindings.get( thread_name );
	if ( b == null ) {
	    b = new Bindings( thread_name, null );
	    bindings.put( thread_name, b );
	}
	return b;
    }

    //
    // Tracking of threads and data dependencies.
    //

    /**
     * Holds the current "thread name".
     */
    public String thread_name = "X";

    /**
     * Change thread name and return the prior name.
     */
    public String setThreadName(String n) {
	String old = thread_name;
	thread_name = n;
	return old;
    }

    /**
     * Determine the call data element name from a given head string
     * or thread_name string.
     */
    public String getArgsName(String name) {
	int i = name.indexOf( '"' );
	if ( i < 0 )
	    i = name.length();
	i = name.lastIndexOf( '*', i );
	if ( i < 0 ) 
	    i = name.length();
	return name.substring( 0, i ) + "-ARG";
    }

    /**
     * Determine the call data element name from the thread_name, and
     * return its value.
     * @see BDIGoal
     */
    public Object getArgs(String head) {
	return getValue( getArgsName( head ) );
    }

    /**
     * The most recently assigned arg.
     */
    public Object arg;

    /**
     * Set up the call data agrument for the given instance head name
     * as given, adding the "-ARG" suffix.
     */
    public void setArgs(String name,Object x) {
	arg = x;
	if ( x != null ) {
	    setValue( name + "-ARG", x );
	}
    }

    //
    // Normal mode accessors
    //

    /**
     * Utility method to obtain data element value.
     */
    public Object getValue(String name) {
	Element e = find( name );
	return e != null? e.get() : null ;
    }

    /**
     * Utility method to obtain data element value as seen by a given
     * "thread".
     */
    public Object getValue(String name,String th_name) {
	Bindings b = (Bindings) bindings.get( th_name );
	if ( b == null )
	    return getValue( name );
	Element e = find( b, name );
	return e != null? e.get() : getValue( name ) ;
    }

    /**
     * Utility method to set data element value.
     */
    public Data setValue(String name,Object v) {
	create( name ).set( v );
	return this;
    }

    /**
     * Utility method to set data element value relative a given
     * "thread".
     */
    public Data setValue(String th_name, String name,Object v) {
	String old = setThreadName( th_name );
	create( name ).set( v );
	setThreadName( old );
	return this;
    }

    /**
     * Utility method to delete all elements for a name along the
     * whole binding chain.
     */
    public void forget(String name) {
        for ( Bindings b = getBindings(); b != null; b = b.prior ) {
	    b.elements.remove( name );
	}
    }

    /**
     * Utility method to forget all but n values for an element (i.e.,
     * its "innermost" Binding occurrence).  If n < 0, then -n top
     * values are forgotten. If n == 0, then everyhing is forgotten,
     * except that the element remains.
     */
    public void forget(String name,int n)
    {
	Element e = find( name );
	if ( e != null ) {
	    e.forget( n );
	}
    }

    /**
     * Utility method to replace top data element value.
     */
    public Data replaceValue(String name,Object v)
    {
	Element e = create( name );
	e.pop();
	e.set( v );
	return this;
    }

    /**
     * Utility method to restore to a prior value. 
     */
    public Data restoreValue(String name,Object v) {
	Element e = create( name );
	e.pop();
	if ( goldfish || e.size() == 0 ) {
	    e.set( v );
	}
	return this;
    }

    /**
     * Find a given data element following the bindings chain of the
     * calling thread.
     */
    public synchronized Element find(String name) {
	return find( getBindings(), name );
    }

    /**
     * Find a given data element following the bindings chain of the
     * calling thread.
     */
    public Element find(Bindings b,String name) {
	for ( ; b != null; b = b.prior ) {
	    Element e = (Element) b.elements.get( name );
	    if ( e != null )
		return e;
	}
	return null;
    }

    /**
     * Tells whether there is an element of a given name in the
     * bindings object chain of the calling thread or not
     */
    public boolean hasElement(String name)
    {
	return find( name ) != null;
    }

    /**
     * Utility to tell how many values a data element has.
     */
    public int size(String name) {
	Element e = find( name );
	return e != null? e.values.size() : 0;
    }

    /**
     * Utility method to retrieve an Element instance on the calling
     * thread's bindings chain, or create one at the head of the chain
     * if missing.
     */
    public synchronized Element create(String name) {
	Element e = find( name );
	if ( e == null ) {
	    e = getBindings().create( name );
	}
	return e;
    }

    /**
     * This method is used for querying about readiness of a data
     * element. The method returns immediately, with true if the
     * element is missing or not ready, and false otherwise.
     */
    public boolean pending(String name) {
	Element e = find( name );
	return e == null || ! e.ready ;
    }

    /**
     * Marks a named element as ready. If no such element exists, then
     * this method will create a new one.
     */
    public void ready(String name) {
        create( name ).markReady();
    }

    /**
     * Set data elements from the {@link Ref} objects of the given
     * {@link Query}.
     */
    public void set(Query query) {
	set( query.getRefs( new Vector/*<Ref>*/() ) );
    }

    /**
     * Set data elements from the given {@link Ref} objects, using
     * {@link #replaceValue}.
     */
    public void set(Vector/*<Ref>*/ refs) {
	for ( Iterator/*<Ref>*/ i = refs.iterator(); i.hasNext(); ) {
	    Ref ref = (Ref) i.next();
	    //setValue( ref.getName(), ref.get() );
	    replaceValue( ref.getName(), ref.get() );
	}
    }

    /**
     * Set the {@link Ref} objects of a {@link Query} from data
     * elements.
     */
    public void get(Query query,boolean keep) {
	get( query.getRefs( new Vector/*<Ref>*/() ), keep );
    }

    /**
     * Set the given {@link Ref} objects from data elements.
     */
    //@SuppressWarnings("unchecked")
    public void get(Vector/*<Ref>*/ refs,boolean keep) {
        for ( Iterator/*<Ref>*/ i = refs.iterator(); i.hasNext(); ) {
            Ref ref = (Ref) i.next();
	    Element e = find( ref.getName() );
	    if ( e != null ) {
		ref.set( keep? e.get() : e.pop() );
	    }
        }
    }

    /**
     * Set the given {@link Ref} object from its data element, without
     * popping the data element value.
     */
    //@SuppressWarnings("unchecked")
    public void get(Ref/*...*/ [] refs) {
	for ( int i = 0; i < refs.length; i++ ) {
	    Element e = find( refs[ i ].getName() );
	    if ( e != null )
		refs[ i ].set( e.get() );
	}
    }

    //
    // Thread control support
    //

    /**
     * A rendezvous flag.
     */
    public boolean flag = true;

    /**
     * Utility method to raise the {@link #flag}.
     */
    public synchronized void setFlag() {
	if ( ! flag ) {
	    flag = true;
	    notifyAll();
	}
    }

    /**
     * Utility method to wait for the rendezvous {@link #flag} to be
     * set.
     */
    public synchronized void waitFlag() {
	while ( ! flag ) {
	    try {
		wait();
	    } catch (InterruptedException e) {
	    }
	}
	flag = false;
    }

    /**
     * Returns a String representation of this Data object.
     */
    public String toString() {
	/************ 1.5
	Formatter f = new Formatter( new StringBuilder() );
	boolean nl = false;
	HashSet/*<String>* / done = new HashSet/*<String>* /();
	for ( Iterator/*<String>* / i =
		  bindings.keySet().iterator(); i.hasNext(); ) {
	    String tn = (String) i.next();
	    if ( nl )
		f.format( "\n" );
	    Bindings b = bindings.get( tn );
	    String h = "Data.Bindings " + b.name;
	    if ( ! done.contains( b.name ) )
		h = b.toString();
	    f.format( "Intention %s --> %s", tn, h );
	    done.add( b.name );
	    nl = true;
	}
	return f.toString();
	**************/
	StringBuilder s = new StringBuilder();
	boolean nl = false;
	HashSet/*<String>*/ done = new HashSet/*<String>*/();
	for ( Iterator/*<String>*/ i =
		  bindings.keySet().iterator(); i.hasNext(); ) {
	    String tn = (String) i.next();
	    if ( nl )
		s.append( "\n" );
	    Bindings b = (Bindings) bindings.get( tn );
	    String h = "Data.Bindings " + b.name;
	    if ( ! done.contains( b.name ) )
		h = b.toString();
	    s.append( "Intention " );
	    s.append( tn );
	    s.append( " --> " );
	    s.append( h );
	    done.add( b.name );
	    nl = true;
	}
	return s.toString();
    }
}
