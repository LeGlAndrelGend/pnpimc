/*********************************************************************
Copyright 2012, Ralph Ronnquist.

This file is part of GORITE.

GORITE is free software: you can redistribute it and/or modify it
under the terms of the Lesser GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GORITE is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the Lesser GNU General Public
License along with GORITE.  If not, see <http://www.gnu.org/licenses/>.
**********************************************************************/

package com.intendico.gorite;

import com.intendico.data.Query;
import com.intendico.data.Ref;
import com.intendico.gorite.Goal.States;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Vector;

/**
 * A FailGoal is achieved by means of failing any sub goal, which are
 * attempted in sequence. The goal fails when and if all sub goals
 * have been attempted and succeeded. In particular, a fail goal
 * without sub goals always fail immediately, and is a way to induce
 * failure. Failure may also be induced by tasks, which can succeed or
 * fail.
 *
 * @see ConditionGoal
 */
public class FailGoal extends SequenceGoal {

    /**
     * Constructor.
     */
    public FailGoal(String n,Goal [] sg) {
	super( n, sg );
    }

    /**
     * Convenience constructor without sub goals.
     */
    public FailGoal(String n) {
	this( n, null );
    }

    /**
     * Creates and returns an instance object for achieving
     * a FailGoal.
     */
    public Instance instantiate(String head,Data d) {
	return new FailInstance( head );
    }

    /**
     * Represents the process of achieving a FailGoal
     */
    public class FailInstance extends SequenceInstance {

	/**
	 * Constructor.
	 */
	public FailInstance(String h) {
	    super( h );
	}

	/**
	 * Process all sub goals in sequence, then reverse {@link
	 * States#PASSED} and {@link States#FAILED}.
	 */
	public States action(String head,Data d)
            throws LoopEndException, ParallelEndException {
	    States s = super.action( head, d );
	    if ( s == States.PASSED )
		return States.FAILED;
	    if ( s == States.FAILED )
		return States.PASSED;
	    return s;
	}
    }
}
