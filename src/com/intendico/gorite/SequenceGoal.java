/*********************************************************************
Copyright 2012, Ralph Ronnquist.

This file is part of GORITE.

GORITE is free software: you can redistribute it and/or modify it
under the terms of the Lesser GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GORITE is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the Lesser GNU General Public
License along with GORITE.  If not, see <http://www.gnu.org/licenses/>.
**********************************************************************/

package com.intendico.gorite;

import com.intendico.data.Query;
import com.intendico.data.Ref;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Vector;

/**
 * A SequenceGoal is achieved by means of achieving its sub goals in
 * order. The goal fails when and if any sub goal fails.
 */
public class SequenceGoal extends Goal {

    /**
     * Constructor.
     */
    public SequenceGoal(String n,Goal [] sg) {
	super( n, sg );
    }

    /**
     * Convenience constructor without sub goals.
     */
    public SequenceGoal(String n) {
	this( n, null );
    }

    /**
     * Creates and returns an instance object for achieving
     * a SequenceGoal.
     */
    public Instance instantiate(String head,Data d) {
	return new SequenceInstance( head );
    }

    /**
     * Implements sequential goal execution.
     */
    public class SequenceInstance extends Instance {

	/**
	 * Constructor.
	 */
	public SequenceInstance(String h) {
	    super( h );
	}

	/**
	 * Keeps the currently ongoing subgoal instance
	 */
	public Instance ongoing = null;

	/**
	 * Index of the currently ongoing sub goal.
	 */
	public int index = 0;

	/**
	 * Cancels execution.
	 */
	public void cancel() {
	    super.cancel();
	    if ( ongoing != null )
		ongoing.cancel();
	}

	/**
	 * Instantiates and performs sub goals in sequence.
	 */
	public States action(String head,Data d)
	    throws LoopEndException, ParallelEndException {
	    Goal [] subgoals = getGoalSubgoals();
	    if ( subgoals == null )
		return super.action( head, d );
	    while ( index < subgoals.length ) {
		if ( ongoing == null ) {
		    ongoing = subgoals[ index ].instantiate(
			head + "." + index, d );
		}
		States s = ongoing.perform( d );
		if ( s != States.PASSED )
		    return s;
		index += 1;
		ongoing = null;
	    }
	    return States.PASSED;
	}
    }

}
