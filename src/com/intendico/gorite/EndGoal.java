/*********************************************************************
Copyright 2012, Ralph Ronnquist.

This file is part of GORITE.

GORITE is free software: you can redistribute it and/or modify it
under the terms of the Lesser GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GORITE is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the Lesser GNU General Public
License along with GORITE.  If not, see <http://www.gnu.org/licenses/>.
**********************************************************************/

package com.intendico.gorite;

import com.intendico.data.Query;
import com.intendico.data.Ref;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Vector;

/**
 * An EndGoal is achieved by means of achieving its sub goals in
 * sequence, and it will then cause an enclosing {@link LoopGoal} goal
 * to be achieved. If any sub goal fails, the EndGoal will just
 * succeed without causing the enclosing {@link LoopGoal} to
 * succeed. An EndGoal never fails; either it succeeds or it causes
 * the enclosing {@link LoopGoal} to succeed.
 *
 * The usage pattern for {@link LoopGoal} and EndGoal is as follows:
 *
 * <pre>
 * new LoopGoal( "loop", new Goal [] {
 *     .. // steps in the loop
 *     .. new EndGoal( "break if", new Goal [] {
 *            // optional sub goals for ending the loop if they succeed
 *        } )
 *     .. // more steps in the loop
 * } )
 * </pre>
 *
 * <p> There may be any number of {@link EndGoal} to end the loop
 * although of course only one will take effect. The {@link EndGoal}
 * may be at any level within the LoopGoal goal hierarchy, except
 * nested within another LoopGoal.
 *
 * <p> The EndLoop effect may be thought of as throwing an {@link
 * Exception} that the nearest enclosing {@link LoopGoal} catches,
 * interrupting the progress of any intermediate composite goal,
 * including {@link ParallelGoal} and {@link RepeatGoal}.  Ongoing
 * branches of intermediate parallel goals will be cancelled.
 *
 * <p> The EndLoop effect does not propagate up through a {@link
 * BDIGoal} execution of a {@link Plan} with non-null {@link
 * Context#context(Data)} {@link Query}. I.e., the EndGoal must
 * typically be explicitely within the sub goal hierarchy of the
 * {@link LoopGoal} of concern.
 *
 * <p> However, the EndGoal effect does propagate up through a {@link
 * BDIGoal} execution if the {@link Plan} has a null {@link
 * Context#context(Data)}, or is an implementing {@link Goal}
 * hierarchy without {@link Context}.
 * 
 * @see LoopGoal
 * @see SequenceGoal
 * @see ParallelGoal
 * @see RepeatGoal
 */
public class EndGoal extends SequenceGoal {

    /**
     * Constructor.
     */
    public EndGoal(String n,Goal [] sg) {
	super( n, sg );
    }

    /**
     * Convenience constructor without sub goals.
     */
    public EndGoal(String n) {
	this( n, null );
    }

    /**
     * Creates and returns an instance object for achieving
     * a EndGoal.
     */
    public Instance instantiate(String head,Data d) {
	return new EndInstance( head );
    }

    /**
     * Implements a loop-end action ("break")
     */
    public class EndInstance extends SequenceInstance {

	/**
	 * Constructor.
	 */
        public EndInstance(String h) {
            super( h );
        }

        /**
         * Process all subgoals in sequence, then throw a
	 * LoopEndException if success. If subgoals fail, then succeed
	 * without throwing exception.
         */
        public States action(String head,Data d)
            throws LoopEndException, ParallelEndException {
	    States s = super.action( head, d );
	    if ( s == States.PASSED )
		throw new LoopEndException( head );
	    if ( s == States.FAILED )
		return States.PASSED;
	    return s;
	}
    }
}
