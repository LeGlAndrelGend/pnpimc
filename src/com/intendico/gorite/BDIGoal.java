/*********************************************************************
Copyright 2012, Ralph Ronnquist.

This file is part of GORITE.

GORITE is free software: you can redistribute it and/or modify it
under the terms of the Lesser GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GORITE is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the Lesser GNU General Public
License along with GORITE.  If not, see <http://www.gnu.org/licenses/>.
**********************************************************************/

package com.intendico.gorite;

import com.intendico.data.Query;
import com.intendico.data.Ref;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Vector;
import java.util.Random;

/**
 * A BDIGoal is achieved by means of finding and performing a goal
 * hierarchy as named by the BDI goal in the {@link Capability} that
 * is the value of the {@link Data.Element} named by the BDIGoal's
 * {@link #control} attribute. By default, the {@link #PERFORMER} data
 * name is used, which is maintained by the execution machinery to
 * resolve to the current performer. The BDIGoals created by the
 * {@link TeamGoal} execution uses the role name as data name, which
 * then is set up to be the role filling.
 *
 * <p> A BDIGoal is executed in a sequence of steps in an attempt to
 * find a goal hierarchy for the named goal that acheives it, or more
 * precisely, that executes without returning FAILED. The goal
 * hierarchy alternatives are found via the {@link
 * Capability#lookup(String)} method of the executing {@link
 * Performer}. Any such alternative that implements the {@link
 * Context} interface is considered a plan whose {@link
 * Context#context(Data)} method defines its applicable variants.
 *
 * <p> The BDIGoal collates all applicable plan options, then selects
 * one of them as an attempt to achieve the goal. If that fails, then
 * the plan option is remembered, and the BDIGoal again collates all
 * applicable, non-failed plan options, picks one and tries that. This
 * repeats until either one plan execution succeeds (returns PASSED),
 * or the plan options are exhausted, in which case the BDIGoal fails.
 * 
 * <p> The choice of which among the applicable plan options to use
 * can be modified via the plan choice settings. By default, the plan
 * options are queried for their {@link Precedence#precedence(Data)}
 * values, and the first of highest precedence option is
 * choosen. However, if the goal is associated with a {@link Random}
 * object, then the option is choosen by a random draw among the
 * highest precedence options. Further, if the goal is associated with
 * a plan choice goal name, then that plan choice goal is performed
 * for effectuating the plan choice (instead of using precedence
 * values).
 *
 * <p> A BDIGoal that has a {@link #specific} object set, makes that
 * object available for instance execution as a data element named by
 * the BDIInstance head name, which for invoked sub goals is
 * everything up to the last '*' of their heads.
 *
 * @see Plan
 * @see PlanChoiceGoal
 * @see ContextualGoal
 * @see Performer#getPlanChoice(String)
 * @see Performer#setPlanChoice(String,Object)
 * @see TeamGoal
 */
public class BDIGoal extends Goal {

    /**
     * The name of the data element whose value is the name of the
     * current role for the executing performer.
     */
    public static final String ROLE = "current role";

    /**
     * Constructor. 
     */
    public BDIGoal(String n) {
	super( n );
	setGoalControl( PERFORMER );
    }

    /**
     * Cache of construction object (when not a String).
     */
    public Object specific;

    /**
     * Constructor using an object other than String. Then the class
     * name of the object is used as goal name, and the object is held
     * as {@link #specific}. However, if the given object is a {@link
     * String}, then its value (rather than its type) is used as goal
     * name anyhow.
     */
    public BDIGoal(Object x) {
	this( ( x instanceof String )? (String) x : x.getClass().getName() );
	specific = x instanceof String? null : x;
    }

    /**
     * Creates and returns an instance object for achieving
     * a BDIGoal.
     */
    public Instance instantiate(String head,Data d) {
	return new BDIInstance( head );
    }

    /**
     * Utility method to add a Goal unless it's contained among
     * failed.
     */
    static public void maybeAdd(
	Goal g,Vector/*<Goal>*/ v,Vector/*<Goal>*/ failed) {
	if ( failed == null || ! failed.contains( g ) ) {
	    v.add( g );
	}
    }

    /**
     * Expand context sensitive alternative plans. This method gets
     * invoked with the current selection of plans matching to the
     * BDIGoal to achieve, the current collection of tried but failed
     * plan variants, and the current {@link Data}. It processes all
     * plan contexts so as to produce the currently possible
     * alternative contextual plan invocations, by determining
     * validating bindings for the plan's context queries.
     *
     * <p> The class {@link ContextualGoal} is used to represent a
     * plan variant, which consists of the plan together with the
     * query {@link Ref} object binding. This takes care of
     * presenting the binding in the {@link Data} when the plan is
     * invoked, to unset this binding from the {@link Data} if the
     * plan execution fails, and to extract new bindings from the
     * {@link Data} when the plan succeeds.
     *
     * <p> This method recognises the {@link Context#EMPTY} query
     * as marker that a plan does not have any applicable variant,
     * and it also catches the {@link Context.None} exception for
     * the same purpose.
     *
     * <p> Plan variants are filtered against the failed set, to
     * avoid the same plan variant be attempted more than once.
     *
     * @see Context
     * @see ContextualGoal
     * @see BDIInstance#action
     */
    static public Vector/*<Goal>*/ applicable(
	Vector/*<Goal>*/ plans,	Vector/*<Goal>*/ failed, Data data) {
	Vector/*<Goal>*/ v = new Vector/*<Goal>*/();

	for ( Iterator/*<Goal>*/ i = plans.iterator(); i.hasNext(); ) {
	    Goal goal = (Goal) i.next();
	    if ( ! ( goal instanceof Context ) ) {
		maybeAdd(
		    new ContextualGoal( null, null, goal, data ),
		    v, failed );
		continue;
	    }
	    try {
		Query query = ((Context) goal).context( data );
		if ( query == null ) {
		    maybeAdd(
			new ContextualGoal( null, null, goal, data ),
			v, failed );
		    continue;    
		}
		if ( query != Context.EMPTY ) {
		    Vector/*<Ref>*/ vars =
			query.getRefs( new Vector/*<Ref>*/() );
		    Vector/*<Ref>*/ orig = Ref.copy( vars );
		    query.reset();
		    while ( query.next() ) {
			maybeAdd(
			    new ContextualGoal( orig, vars, goal, data ),
			    v, failed );
		    }
		}
	    } catch (Context.None e) {
		// No applicable context
	    } catch (Throwable e) {
		e.printStackTrace();
		System.err.println( "Ignored plan " + goal );
	    }
	}
	return v;
    }

    /**
     * Return the plan choice for this goal, relative a given root
     * capability executing the goal. This treats 
     */
    public Object getPlanChoice(Capability root) {
	return root.getPerformer().getPlanChoice( getGoalName() );
    }

    /**
     * Implements a BDI method choice.
     */
    public class BDIInstance extends Instance {

	/**
	 * The goal alternatives. (Called "relevant set" in BDI
	 * terminology)
	 */
	public Vector/*<Goal>*/ relevant;

	/**
	 * The capability hierarchy offering goal methods.
	 */
	public Capability root;

	/**
	 * The tried and failed goals.
	 */
	public Vector/*<Goal>*/ failed = new Vector/*<Goal>*/();

	/**
	 * The count of attempts.
	 */
	public int count = 0;

	/**
	 * The current goal.
	 */
	Goal goal;

	/**
	 * The current goal instance.
	 */
	Instance instance = null;

	/**
	 * Determine the context sensitive alternatives by invoking
	 * {@link #applicable}
	 *
	 * <p> At end, this method invokes {@link #precedenceOrder} on
	 * the collection of applicable plan variants, which sorts the
	 * plan variants by descending precedence.
	 * @see Context
	 * @see ContextualGoal
	 * @see #precedenceOrder
	 * @see BDIInstance#action
	 */
	public Goal contextual(
	    Vector/*<Goal>*/ set,Vector/*<Goal>*/ failed,Data data) {
	    Vector/*<Goal>*/ v = applicable( set, failed, data );

	    // If there is a plan choice goal for this goal, then let
	    // it make the choice.
	    if ( plan_choice instanceof String ) {
		return new PlanChoiceGoal(
		    root.getPerformer(), (String) plan_choice, v, failed );
	    }
	    return precedenceOrder( v, data );
	}

	/**
	 * Apply precedence ordering destructively to the given goal
	 * set.
	 *
	 * @see Precedence
	 */
	public Goal precedenceOrder(Vector/*<Goal>*/ set,Data data) {
	    int level = 0;
	    Vector/*<Goal>*/ best = new Vector();
	    for ( Iterator/*<Goal>*/ i = set.iterator(); i.hasNext(); ) {
		Goal g = (Goal) i.next();
		int p = g instanceof Precedence? 
		    ((Precedence) g).precedence( data ) :
		    Plan.DEFAULT_PRECEDENCE ;
		if ( best.size() == 0 ) {
		    level = p;
		    best.add( g );
		} else if ( p > level ) {
		    best.clear();
		    level = p;
		    best.add( g );
		} else if ( p == level ) {
		    best.add( g );
		}
	    }
	    if ( best.size() == 0 )
		return null;
	    if ( best.size() == 1 || ! ( plan_choice instanceof Random ) )
		return (Goal) best.get( 0 );
	    return (Goal) best.get(
		((Random) plan_choice).nextInt( best.size() ) );
	}

	/**
	 * Holds the plan choice goal, or null.
	 */
	public Object plan_choice;

	/**
	 * Constructor.
	 */
        public BDIInstance(String h) {
            super( h );
        }

	/**
	 * Cancel the execution.
	 */
	public void cancel() {
	    if ( instance != null )
		instance.cancel();
	}

	/**
	 * Instantiates and performs sub goals in sequence, until the
	 * first one that does not fail. If a sub goal fails, then
	 * that is caught, and the next sub goal in sequence is
	 * instantiated and performed.
	 */
	public States action(String head,Data data)
            throws LoopEndException, ParallelEndException {
	    if ( root == null ) {
		root = (Capability) data.getValue( getGoalControl() );
		if ( root == null ) {
		    System.err.println(
			"** Missing capability '" + getGoalControl() +
			"' for " + BDIGoal.this.toString( head ) );
		    return States.FAILED;
		}
		plan_choice = getPlanChoice( root );
	    }
	    if ( goal == null ) {
		if ( isTracing() ) {
		    System.err.println(
			"** Lookup \"" + getGoalName() + "\"" );
                }
		data.setArgs( head, specific );
		goal = contextual(
		    root.lookup( getGoalName() ), failed, data );
		if ( goal == null && isTracing() ) {
		    System.err.println(
			"** Goal \"" + getGoalName() + "\" unknown " );
		}
	    }

	    while ( goal != null ) {
		if ( instance == null ) {
		    instance = goal.instantiate( head + "*" + count, data );
		    count += 1;
		}
		if ( isTracing() ) {
		    System.err.println(
			"** " + nameString( getGoalName() ) +
			" attempt " + count );
		}
		String was_role = (String) data.getValue( ROLE );
		States b;
		data.setValue( ROLE, getGoalControl() );
		try {
		    b = instance.perform( data );
		} finally {
		    data.restoreValue( ROLE, was_role );
		}
		//
		// Note, the following doesn't happen if instance.perform()
		// above throws an exception.
		//
		if ( b != States.FAILED )
		    return b;
		instance = null;
		if ( goal instanceof PlanChoiceGoal ) {
		    PlanChoiceGoal pg = (PlanChoiceGoal) goal;
		    if ( pg.choice == null )
			break;
		    if ( pg.done == States.PASSED )
			failed.add( pg.choice );
		} else {
		    failed.add( goal );
		}
		goal = contextual(
		    root.lookup( getGoalName() ), failed, data );
	    }
	    return States.FAILED;
	}
    }
}
