/*********************************************************************
Copyright 2012, Ralph Ronnquist.

This file is part of GORITE.

GORITE is free software: you can redistribute it and/or modify it
under the terms of the Lesser GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GORITE is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the Lesser GNU General Public
License along with GORITE.  If not, see <http://www.gnu.org/licenses/>.
**********************************************************************/

package com.intendico.gorite;

import com.intendico.data.Query;

/**
 * The TransferGoal class is utility class for wrapping a goal that is
 * transferred into a different target performer context.
 */
public class TransferGoal extends Plan {

    /**
     * Holds the target performer.
     */
    public Performer target;

    /**
     * Holds the transferred goal.
     */
    public Goal goal;

    /**
     * Constructor.
     */
    public TransferGoal(Performer to,Goal g) {
	super( g.getGoalName() );
	target = to;
	goal = g;
    }

    /**
     * Constructor without Performer, which instead is looked up
     * at instantiation using the control string.
     */
    public TransferGoal(Goal g) {
	super( g.getGoalName() );
	target = null;
	goal = g;
    }

    /**
     * Overrides the base class method {@link Goal#instantiate} to
     * provide a wrapper instance for the transferred goal.
     */
    public Instance instantiate(String h,Data d) {
	return new TransferInstance( h, d );
    }

    /**
     * Transfer goals are equals by the goals they transfer.
     */
    public boolean equals(Object x) {
	return x instanceof TransferGoal && equals( (TransferGoal) x );
    }

    /**
     * Transfer goals are equals by the goals they transfer.
     */
    public boolean equals(TransferGoal x) {
	return goal.equals( x.goal );
    }

    /**
     * Wrapper class for transfer goal instances.
     */
    public class TransferInstance extends Instance {

	/**
	 * Holds the instance of the transferred goal.
	 */
	public Instance instance;

	public Performer into;
	/**
	 * Constructor. Constructs an instance of the transferred
	 * goal while temporarily shifting into the target
	 * performer. 
	 */
	public TransferInstance(String h,Data d) {
	    super( h );
	    instance = goal.instantiate( h + "|", d );
	}

	/**
	 * Control callback whereby the intention gets notified that
	 * it is cancelled. This will forward the cancellation to the
	 * currently progressing instance, if any.
	 */
	public void cancel() {
	    if ( instance != null )
		instance.cancel();
	}

	/**
	 * Implements the instance action, which sets the target
	 * performer as {@link Goal#PERFORMER}, then invokes the
	 * target instance, and thereafter restores the performer.
	 */
	public States action(String h,Data d)
	    throws LoopEndException, ParallelEndException {
	    Performer from = (Performer) d.getValue( PERFORMER );
	    try {
		if ( target == null ) {
		    into = (Performer) d.getValue( getGoalControl() );
		} else {
		    into = target;
		}
		// Note the use of setValue() rather than
		// replaceValue() below. This is done to ensure
		// that PERFORMER is set in the local data
		// context, without replacing PERFORMER in the
		// parent context.
		//
		// Also, the new PERFORMER is pushed on top of
		// any existing value, showing the chain of
		// performers involved in the computation.  Unless
		// the data context is in "goldfish" mode of
		// course.
		d.setValue( PERFORMER, into );
		if ( isTracing() )
		    System.err.println( ">> " + into );
		return instance.perform( d );
	    } finally {
		// Note that restoreValue only affects the local
		// data context.
		d.restoreValue( PERFORMER, from );
		if ( isTracing() )
		    System.err.println( "<< " + into );
	    }
	}
    }

    /**
     * Implements the {@link Context#context} method by forwarding to
     * the wrapped goal.
     */
    public Query context(Data d) throws Exception {
	if ( goal instanceof Context ) {
	    Capability from = (Capability) d.getValue( PERFORMER );
	    try {
		if ( isTracing() ) {
		    System.err.println( "~(context)> (" + target + ")" );
		}
		d.setValue( PERFORMER, target );
		return ((Context) goal).context( d ) ;
	    } finally {
		if ( isTracing() ) {
		    System.err.println( "<(context)~ (" + target + ")" );
		}
		d.restoreValue( PERFORMER, from );
	    }
	} else {
	    return null ;
	}
    }

    /**
     * Implements the {@link Precedence#precedence} method by
     * forwarding to the wrapped goal.
     */
    public int precedence(Data d) {
	if ( goal instanceof Precedence ) {
	    Capability from = (Capability) d.getValue( PERFORMER );
	    try {
		if ( isTracing() ) {
		    System.err.println( "~(precedence)> (" + target + ")" );
		}
		d.setValue( PERFORMER, target );
		return ((Precedence) goal).precedence( d );
	    } finally {
		if ( isTracing() ) {
		    System.err.println( "<(precedence)~ (" + target + ")" );
		}
		d.replaceValue( PERFORMER, from );
	    }
	} else {
	    return DEFAULT_PRECEDENCE ;
	}
    }
}
