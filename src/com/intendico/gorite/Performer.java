/*********************************************************************
Copyright 2012, Ralph Ronnquist.

This file is part of GORITE.

GORITE is free software: you can redistribute it and/or modify it
under the terms of the Lesser GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GORITE is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the Lesser GNU General Public
License along with GORITE.  If not, see <http://www.gnu.org/licenses/>.
**********************************************************************/

package com.intendico.gorite;

import java.util.Vector;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Observable;
import java.util.Observer;
import com.intendico.data.Ref;
import com.intendico.data.RuleSet;
import com.intendico.data.Rule;
import com.intendico.data.Query;
import com.intendico.gorite.Goal.Instance;
import com.intendico.gorite.addon.TodoGroupSkipBlocked;
import com.intendico.gorite.addon.TodoGroupRoundRobin;
import com.intendico.gorite.addon.TodoGroupParallel;

/**
 * The Performer class is base class for {@link Team} member
 * implementations. Conceptually a performer is considered to be the
 * agent for {@link Goal} executions, and it contains the belief
 * structures, if any, that agent reasoning may refer to.
 *
 * <p> A Performer is a {@link Capability} in that it has {@link Goal}
 * hierarchies, and inner capabilities that define its reasoning
 * processes.
 *
 * <p> The goals of performer may be grouped to belong to named "to-do
 * groups", which are represented by the inner class {@link
 * Performer.TodoGroup}. The goals of a TodoGroup are managed such
 * that only one at a time is performed, and the group is associated
 * with meta-level reasoning for deciding which goal to perform next.
 */
public class Performer extends Capability {

    /**
     * The {@link Data.Element} name of the Todogroup object concerned
     * when performing TodoGroups meta-level reasoning.
     */
    public static final String TODOGROUP = "todogroup";

    /**
     * Holds the Performer's name.
     */
    public String name;

    /**
     * Default constructor. When this is used, the {@link #name} field
     * should be assigned before the performer is made to fill a role.
     */
    public Performer() {
	this( "nobody" );
    }

    /**
     * Constructor with {@link #name} given.
     */
    public Performer(String n) {
	name = n;
	performer = this;
    }

    /**
     * Returns a text representation of this performer.
     */
    public String toString() {
	return getClass().getName() + ": " + name;
    }

    /**
     * Overrides {@link Capability#addCapability} so as to link up
     * added capabilities with this performer.
     */
    public void addCapability(Capability c) {
	c.setPerformer( this );
	super.addCapability( c );
    }

    //
    // The performer's rule set, if any.
    //

    /**
     * The Performer's rule set.
     */
    public RuleSet rule_set = new RuleSet() {{
	addObserver( new Observer() {
		public void update(Observable x,Object y) {
		    signalExecutor();
		}
	    } );
    }};

    /**
     * Utility method to apply all rules exhaustively.
     */
    public void propagateBeliefs() {
	rule_set.run();
    }

    //
    // TodoGroup based execution support
    //

    /**
     * A constant goal name for a generic TodoGroup meta goal.
     * Presently "todogroup meta goal".
     */
    public final static String TODOGROUP_META_GOAL =
	TODOGROUP + " meta goal";

    /**
     * This performer's {link TodoGroup} objects.
     */
    public Hashtable/*<String,TodoGroup>*/ todogroups =
        new Hashtable/*<String,TodoGroup>*/();

    /**
     * Utility method to find a named {@link TodoGroup}. This method
     * will create a new {@link TodoGroup} rather than return
     * <tt>null</tt>, and the new {@link TodoGroup} will have its
     * {@link TodoGroup#meta_goal} string set to <tt>null</tt> by
     * default (meaning no meta goal), unless the system property
     * <tt>gorite.todo.classic</tt> is set, in which case {@link
     * #TODOGROUP_META_GOAL} is used as meta goal name.
     * @param rn the {@link TodoGroup} name
     * @return the {@link TodoGroup} object of that name among this
     * performer's {@link #todogroups}.
     */
    public TodoGroup getTodoGroup(String rn) {
	if ( rn == null ) 
	    return null;
	TodoGroup r = (TodoGroup) todogroups.get( rn );
	if ( r == null ) {
            if ( Goal.isTracing() ) {
		System.err.println( "-- " + name + " adds TodoGroup " + rn );
	    }
	    String mg = null;
	    if ( System.getProperty( "gorite.todo.classic" ) != null )
		mg = TODOGROUP_META_GOAL;
	    r = new TodoGroup( rn, mg );
	    addTodoGroup( r );
	}
	return r;
    }

    /**
     * Utility method to add a TodoGroup with given name and meta goal.
     */
    public void addTodoGroup(String n,String mg) {
	addTodoGroup( new TodoGroup( n, mg ) );
    }
    
    /**
     * Utility method to add a given TodoGroup.
     */
    public void addTodoGroup(TodoGroup r) {
	if ( todogroups == null )
	    todogroups = new Hashtable/*<String,TodoGroup>*/();
	todogroups.put( r.name, r );
    }

    /**
     * Control flag for TodoGroup execution.
     */
    private long todo_added = 0;

    /**
     * Utility method to add an instance to its todogroup.
     */
    public Goal.States execute(
	boolean reentry,Goal.Instance instance,String group)
	throws LoopEndException, ParallelEndException
    {
	if ( Goal.isTracing() )
	    System.err.println( "-- execute: " + group + " " + reentry );
	//todo_added += 1;
	return getTodoGroup( group ).execute( reentry, instance );
    }

    /**
     * Index of TodoGroup to execute next.
     */
    private int todo_index = 0;

    /**
     * Control method to run all todogroups until stopped or blocked.
     */
    synchronized public Goal.States runPerformer() {
	if ( Goal.isTracing() )
	    System.err.println(
		"-- " + name + " running " + 
		( todogroups == null? 0 : todogroups.size() ) );
	propagateBeliefs();
	if ( todogroups == null || todogroups.size() == 0 )
	    return Goal.States.BLOCKED;
	boolean running = true;
	Goal.States s = Goal.States.BLOCKED;
	while ( running ) {
	    long todo_added_cache = todo_added;
	    TodoGroup [] groups = (TodoGroup []) todogroups.values().toArray(
		new TodoGroup [ todogroups.size() ] );
	    running = false;
	    propagateBeliefs();
	    for ( int i = 0; i < groups.length; i++ ) {
		if ( todo_index >= groups.length )
		    todo_index = 0;
		s = groups[ todo_index++ ].runGroup();
		if ( Goal.isTracing() )
		    System.err.println( "-- runGroup = " + s );
		propagateBeliefs();
		running = todo_added != todo_added_cache;
		if ( s == Goal.States.BLOCKED ) {
		    continue;
		}
		if ( s == Goal.States.STOPPED )
		    break;
		running = true;
		s = Goal.States.BLOCKED;
	    }
	}
	if ( Goal.isTracing() )
	    System.err.println( "-- " + name + " (" + s + ")" );
	return s;
    }

    /**
     * The TodoGroup class provides a meta-level goal execution
     * facility, allowing multiple parallel executions be
     * synchronised.
     *
     * <p> A Goal that has its {@link Goal#group} attribute set will
     * be executed by adding its execution instance to the nominated
     * TodoGroup. The instance execution will be progressed as a
     * TodoGroup execution task, when the execution thread gets around
     * to it.
     *
     * <p> The TodoGroup is associated with a meta goal {@link
     * #meta_goal}, which is performed by the execution machinery when
     * the TodoGroup state changes: when a goal instance is added,
     * when its execution gets blocked, and when it completes. The
     * meta goal execution may inspect the TodoGroup and promote any
     * of the pending goal instance executions to be the one to
     * progress next.
      */
    public class TodoGroup implements Observer {

	/**
	 * A name attribute for the group. There is no particular
	 * significance in this name except that it uniquely
	 * identifies a TodoGroup object for a {@link Performer}.
	 */
	public String name = null;

	/**
	 * This names the {@link BDIGoal} to be invoked when {@link
	 * Goal.Instance} objects are added to, or removed from the
	 * TodoGroup, as well as when the intention execution returns
	 * blocked. The meta goal is invoked for the purpose of
	 * deciding which intention to pursue next.
	 * 
	 * <p> The default name is {@link
	 * Performer#TODOGROUP_META_GOAL}.
	 *
	 * <p> When the meta goal is invoked, it is provided with the
	 * following data elements:
	 * <dl>
	 * <dt>named by {@link Goal#PERFORMER}</dt><dd>this {@link
	 * Performer} object</dd>
	 * <dt>named by {@link #TODOGROUP}</dt><dd>this TodoGroup
	 * object</dd>
	 * <dt>"added"</dt><dd>{@link Vector} of the {@link
	 * Goal.Instance} objects just added to the group, if any.</dd>
	 * <dt>"removed"</dt><dd>{@link Vector} of the {@link
	 * Goal.Instance} objects just removed from the group, if any.</dd>
	 * <dt>"top"</dt><dd>the current top-of-stack {@link
	 * Goal.Instance} object. When an instance terminates, it is
	 * added to the "removed" set, then removed from the stack
	 * before the meta goal gets invoked.</dd>
	 * </dl>
	 *
	 * <p> Note that the "added" and "removed" collections have
	 * been processed, and their contents have been added and
	 * removed respectively before the meta goal is performed.
	 *
	 * <p> A meta goal plan typically uses {@link #promote} to
	 * change the {@link #stack}. Still, no other code is supposed
	 * to operate on the stack concurrently, so the meta goal plan
	 * may change the stack in more complex ways.
	 *
	 * @see TodoGroupSkipBlocked
	 * @see TodoGroupRoundRobin
	 * @see TodoGroupParallel
	 */
	public String meta_goal = null;

	/**
	 * Constructor given name and meta goal name. If the meta goal
	 * name is null, then the default meta goal name ({@link
         * Performer#TODOGROUP_META_GOAL}) is used.
	 */
	public TodoGroup(String n,String mg) {
	    name = n;
	    meta_goal = mg;
	}

	/**
	 * This is the stack of executions in this TodoGroup.
	 */
	public Vector/*<Goal.Instance>*/ stack =
	    new Vector/*<Goal.Instance>*/();

        public Vector/*<Goal.Instance>*/ added =
            new Vector/*<Goal.Instance>*/();

        public Vector/*<Goal.Instance>*/ removed =
            new Vector/*<Goal.Instance>*/();

	/**
	 * Keeping track of an ongoing meta goal execution. This is in
	 * particular used if a meta goal execution is blocked or
	 * stopped, in which case the pursuit of TodoGroup tasks is
	 * stopped. The ongoing meta goal execution must terminate
	 * (passed, failed or cancel) before the next TodoGroup task
	 * is pursued.
	 *
	 * <p> Note that new TodoGroup tasks can be added during an
	 * ongoing meta goal execution, and that this will not trigger
	 * any new meta goal execution. However, it will cause {@link
	 * #added_while_ongoing} to be set, which in turn results in
	 * an additional meta goal execution to follow the ongoing
	 * one.
	 */
	public Goal.Instance ongoing_meta_goal = null;

	/**
	 * A flag that indicates that a TodoGroup task was added
	 * during an ongoing meta goal execution. This will cause a
	 * subsequent meta goal execution before any TodoGroup task is
	 * progressed.
	 */
	public boolean added_while_ongoing = false;

	/**
	 * Invoked prior to pursuing the top element of the
	 * stack. This method adds the added instances onto the stack,
	 * cleares out the removed instances. It creates a meta goal
	 * instance if either forced is true, or the some instance was
	 * added or removed.
	 *
	 * <p> Note that this method will change the {@link #stack} by
	 * removing completed tasks, and actually adding tasks pending
	 * to be added. This happens before the invocation of the meta
	 * goal (if any). 
	 */
	private Goal.Instance enterGroup(boolean forced) {
	    Vector/*<Goal.Instance>*/ coming = null;
	    Vector/*<Goal.Instance>*/ gone = null;

	    // pick up the added and removed instances
	    // the added vector changes by parallel threads
	    synchronized ( added ) {
		coming = added;
		added = new Vector/*<Goal.Instance>*/();
		gone = removed;
		removed = new Vector/*<Goal.Instance>*/();
	    }

	    // handle removed instances
	    for ( Iterator/*<Goal.Instance>*/ i = gone.iterator();
		  i.hasNext(); ) {
		Goal.Instance instance = (Goal.Instance) i.next();
		instance.data.deleteObserver( instance.thread_name, this );
	    }

	    // handle added instances
	    for ( Iterator/*<Goal.Instance>*/ i = coming.iterator();
		  i.hasNext(); ) {
                Goal.Instance instance = (Goal.Instance) i.next();
		stack.add( instance );
		instance.data.addObserver( instance.thread_name, this );
	    }

	    if ( meta_goal == null )
		return null;

	    if ( coming.size() + gone.size() == 0 ) {
		if ( ! forced )
		    return null;
		if ( System.getProperty( "gorite.todo.classic" ) != null )
		    return null;
	    }

	    if ( Goal.isTracing() ) {
		System.err.println(
		    "-- " + Performer.this.name + "/" + name +
		    " meta goal " + added + " \"" + meta_goal + "\"" );
	    }

	    Goal.Instance mg = new BDIGoal( meta_goal )
		.instantiate( name, null );
	    mg.performer = Performer.this;
	    mg.data = new Data()
		.setValue( Goal.PERFORMER, Performer.this )
		.setValue( TODOGROUP, this )
		.setValue( "added", coming )
		.setValue( "removed", gone )
		.setValue( "top", stack.size() == 0? null : stack.get( 0 ) );
	    mg.data.link( mg.thread_name );

	    return mg;
	}

	/**
	 * A transient handle to the most recent blocking intention on
	 * the actual stack.
	 */
	public Goal.Instance blocking = null;

	/**
	 * Run the group. This means to run its top-of-stack goal
	 * instance. If that passes or fails, it is removed from the
	 * group, and the meta goal is called upon to possibly
	 * re-order remaining goal instances. If it returns STOPPED,
	 * the group execution returns STOPPED. If it returns BLOCKED,
	 * the meta goal is invoked in order to possibly re-arrange
	 * the group, and if so, the group execution continues with
	 * the new top goal instance. Otherwise the group execution
	 * returns blocked.
	 */
	public Goal.States runGroup() {
            if ( Goal.isTracing() ) {
		System.err.println(
		    "-- TodoGroup " + Performer.this.name + "/" + name +
		    " " + stack.size() );
	    }
	    for ( ;; ) {
		if ( ongoing_meta_goal != null ) {
		    if ( Goal.isTracing() ) {
			System.err.println(
			    "-- TodoGroup " + Performer.this.name +
			    "/" + name + " ongoing meta goal " +
			    stack.size() );
		    }
		    Goal.States s = ongoing_meta_goal.action();
		    if ( Goal.isTracing() ) {
			System.err.println(
			    "-- " + Performer.this.name + "/" + name +
			    " meta goal " + " \"" + meta_goal + "\" " +
			    s );
		    }
		    if ( s == Goal.States.STOPPED ) {
			continue; // Don't interrupt meta goal
		    }
		    if ( s == Goal.States.BLOCKED ) {
			return s;
		    }
		}
		ongoing_meta_goal = enterGroup( ongoing_meta_goal == null );
		if ( ongoing_meta_goal != null )
		    continue;
		if ( stack.size() == 0 )
		    return Goal.States.BLOCKED;

		Goal.Instance x = (Goal.Instance) stack.get( 0 );

		if ( x == blocking ) {
		    if ( ! x.data.isRunning( x.thread_name ) ) {
			return Goal.States.BLOCKED;
		    }
		    if ( Goal.isTracing() ) {
			System.err.println( "-- BLOCKING " + x.head +
					    " without trigger" );
		    }
		    blocking = null;
		    //return Goal.States.BLOCKED;
		}
		blocking = null;
		Goal.States s = x.action();
		if ( Goal.isTracing() ) {
		    System.err.println( "-- noting " + x.head + " " + s );
		}
		if ( s == Goal.States.STOPPED ) {
		    return s;
		}
		if ( s == Goal.States.BLOCKED ) {
		    blocking = x;
		    continue;
		}
		// s == Goal.States.PASSED
		// s == Goal.States.FAILED
		// s == Goal.States.CANCEL
		x.setMonitoring( s );
		if ( stack.remove( x ) )
		    removed.add( x );
	    }
	}

	/**
	 * Adds a goal for execution as part of this todo
	 * group. Returns the instance monitoring state.
	 */
	public Goal.States execute(
	    boolean reentry,Goal.Instance instance)
	    throws LoopEndException, ParallelEndException
	{
	    if ( ! reentry ) {
		todo_added += 1;
		signalExecutor();
		synchronized ( added ) {
		    instance.setMonitoring( Goal.States.STOPPED );
		    added.add( instance );
		}
	    } else {
		instance.throwSavedException();
	    }
	    return instance.state;
	}

	/**
	 * Utility method to obtain stack size.
	 */
	public int size() {
	    return stack.size();
	}

	/**
	 * Utility method to access the i:th stack element.
	 * @throws ArrayIndexOutOfBoundsException if the stack is
	 * empty, or i is negative.
	 */
	public Instance get(int i) {
	    return (Instance) stack.get( i );
	}

	/**
	 * Utility method to remove the i:th stack element.
	 * @throws ArrayIndexOutOfBoundsException if appropriate
	 */
	public Instance remove(int i) {
	    return (Instance) stack.remove( i );
	}

	/**
	 * Utility method to insert an element to be the i:th stack
	 * element.
	 * @throws ArrayIndexOutOfBoundsException if appropriate
	 */
	public void add(int i,Instance x) {
	    stack.add( i, x );
	}

	/**
	 * Utility method to move a selected {@link Goal.Instance} to
	 * be the one to pursue next.
	 */
	public void promote(int index) {
	    if ( index != 0 )
		stack.insertElementAt( stack.remove( index ), 0 );
	}

	/**
	 * Utility method to move the top {@link Goal.Instance} to
	 * stack bottom.
	 */
	public void rotate() {
	    if ( stack.size() > 1 )
		stack.add( stack.remove( 0 ) );
	}

	///
	/// Observer interface implementation
	///

	/**
	 * Implements {@link Observer#update} by pretending that a
	 * todo group is added, as way of signalling that the
	 * performer should process its todogroups again
	 */
	public void update(Observable x,Object y) {
	    todo_added += 1;
	}

    } // End class TodoGroup

    /**
     * The {@link Executor} for this performer.
     */
    public Executor executor = null;

    /**
     * Utility method to signal the {@link #executor} that this
     * performer wants some attention. This will use the current
     * executor, and attach to the default executor if none is
     * assigned.
     */
    public void signalExecutor() {
	if ( executor == null ) {
	    executor = Executor.getDefaultExecutor();
	    shareInquirables( null );
	    executor.addPerformer( this );
	}
	executor.signal();
    }

    /**
     * Utility method to instantiate the goal for given the head, and
     * then keep invoking the perform method with the given data
     * context, until it passes or fails.
     */
    synchronized public boolean performGoal(Goal goal,String head,Data d) {
	signalExecutor();
	try {
	    Goal.Instance instance = goal.instantiate( head, d );
	    Goal.States s = Goal.States.PASSED;
	    for ( ;; ) {
		if ( Goal.isTracing() )
		    System.err.println( "** perform " + instance.head );
		d.replaceValue( Goal.PERFORMER, this );
		s = instance.perform( d );
		if ( s == Goal.States.PASSED || s == Goal.States.FAILED )
		    break;
		boolean idle = s == Goal.States.BLOCKED;
		if ( executor != null ) {
		    if ( executor.runPerformersBlocked() )
			idle = false;
		}
		if ( idle )
		    instance.waitDone(); // ignore result
	    }
	    if ( executor != null ) {
		while ( executor.runPerformersBlocked() )
		    ;
	    }
	    return s == Goal.States.PASSED;
	} catch (LoopEndException e) {
	    e.printStackTrace();
	    return false;
	} catch (ParallelEndException e) {
	    e.printStackTrace();
	    return false;
	}
    }

    /**
     * Alternative performGoal method, with input and output held as
     * Vector<Ref> collections.
     */
    public boolean performGoal(
	Goal goal,String head, Vector/*<Ref>*/ ins,Vector/*<Ref>*/ outs) {
	Data d = new Data();
	d.set( ins );
	boolean b = performGoal( goal, head, d );
	d.get( outs, true );
	return b;
    }

    /**
     * Alternative performGoal method, with goal given as a {@link
     * String}, implying a BDI goal, and input and output held as
     * Vector<Ref> collections.
     */
    public boolean performGoal(
	String goal,String head, Vector/*<Ref>*/ ins,Vector/*<Ref>*/ outs) {
	return performGoal( new BDIGoal( goal ), head, ins, outs );
    }

    /**
     * Returns true if the performer execution should change focus.
     */
    public boolean changeFocus() {
	return executor != null && executor.changeFocus();
    }

    /**
     * Returns a {@link RoleFilling} object for filling a given {@link
     * Team.Role}. The object represents that this performer fills the
     * given {@link Team.Role}. This method may be overridden in a
     * specific performer, either as way of providing an extended
     * {@link RoleFilling} object, or to possibly refuse the {@link
     * Team.Role} filling, by returning <em>null</em>.
     */
    public RoleFilling fillRole(Team.Role r) {
	return new RoleFilling( r );
    }

    /**
     * This class is used for representing this performer when acting
     * in a given {@link Team.Role}. It in particular provides a
     * {@link #lookup(String)} method that provides a combined plan
     * lookup in both the filled {@link Team.Role} (qua {@link
     * Capability}) and the enclosing {@link Performer} (qua {@link
     * Capability}), and thereby extending the performer's capability
     * with the role plans.
     */
    public class RoleFilling extends Capability {

	/**
	 * The {@link Team.Role} being filled.
	 */
	public Team.Role role;

	/**
	 * Constructor. The enclosing performer is set to fill the
	 * given role.
	 */
	public RoleFilling(Team.Role r) {
	    role = r;
	}

	/**
	 * Extends the base class method {@link Capability#lookup} by
	 * wrapping the result goals into {@link TransferGoal} goals.
	 */
	public Vector/*<Goal>*/ lookup(String name) {
	    Vector/*<Goal>*/ v = Performer.this.lookup( name );
	    Vector/*<Goal>*/ vx = role.lookup( name );
	    vx.addAll( super.lookup( name ) );
	    for ( Iterator/*<Goal>*/ gi = v.iterator(); gi.hasNext(); ) {
		vx.add( goalForPerformer( (Goal) gi.next() ) );
	    }
	    return vx;
	}

	/**
	 * Extends the base class method {@link Capability#hasGoals} by
	 * formarding to the performer
	 */
	public boolean hasGoal(String name) {
	    return role.hasGoal( name ) ||
		super.hasGoal( name ) ||
		Performer.this.hasGoal( name );
	}

	/**
	 * Returns the {@link Performer} of this RoleFilling.
	 */
	public Performer getPerformer() {
	    return Performer.this;
	}

	/**
	 * Returns a text representation of this performer object.
	 */
	public String toString()
	{
	    return "role filler " + Performer.this;
	}
    }

    /**
     * Utility method to obtain the {@link Team} that has established
     * a {@link Team.Role} filler under a given name in the {@link
     * Data}.
     */
    static public Team team(String rolename,Data d) {
	RoleFilling r = (RoleFilling) d.getValue( rolename );
	return r.role.team();
    }

    /**
     * Utility method to create a {@link TransferGoal} object aimed
     * at this performer.
     */
    public Goal goalForPerformer(Goal g) {
	return new TransferGoal( this, g );
    }

    /**
     * A method that returns {@link #name}.
     */
    public String getName() {
	return name;
    }

    ///
    /// Plan choice support
    ///

    /**
     * The association between (BDI) goals and their plan choice
     * modifiers. These take effect in the BDI plan choice processing,
     * to provide the decision for choosing a next applicable plan
     * instance. The modifier should be either a {@link
     * java.util.Random Random} object
     * (for making a random draw among highest precedence plan
     * options0, or a {@link String} (for making the choice by meta
     * level plan processing).
     *
     * @see BDIGoal
     */
    public Hashtable/*<String,Object>*/ plan_choices = null;

    /**
     * Utility method to declare a plan choice modifier for a
     * goal. The modifier should be either a {@link java.util.Random
     * Random} object, or a {@link String}.
     *
     * @see BDIGoal
     */
    public void setPlanChoice(String goal,Object choice_method) {
	if ( plan_choices == null ) {
	    plan_choices = new Hashtable/*<String,String>*/();
	}
	plan_choices.put( goal, choice_method );
    }

    /**
     * Utility method to obtain the plan choice modifier, if any,
     * associated with the given goal.
     */
    public Object getPlanChoice(String goal) {
	return plan_choices == null? null : plan_choices.get( goal );
    }
}
