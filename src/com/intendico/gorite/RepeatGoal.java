/*********************************************************************
Copyright 2012, Ralph Ronnquist.

This file is part of GORITE.

GORITE is free software: you can redistribute it and/or modify it
under the terms of the Lesser GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GORITE is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the Lesser GNU General Public
License along with GORITE.  If not, see <http://www.gnu.org/licenses/>.
**********************************************************************/

package com.intendico.gorite;

import com.intendico.data.Query;
import com.intendico.data.Ref;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Vector;

/**
 * A RepeatGoal is achieved by means of achieving all its
 * instantiations in parallel, where each instantiation is achieved by
 * achieving the given sub goals in sequence. The goal definition
 * includes a context specification, which enumerates the
 * instantiations of the goal arising. Each instantiation refers to
 * the same sequence of sub goals, but with different focus in the
 * data.
 *
 * <p> The following code snippet illustrates the use of a RepeatGoal
 * to process a multi-valued data element (named "options" in the
 * example).
 *
 * <pre>
 * new RepeatGoal( "process options", new Goal [] {
 *     // This goal sequence has its "options" data focussed
 *     // on one of the (previosuly multi-valued) options.
 *     new BDIGoal( "do this with options" ),
 *     new BDIGoal( "do that with options" ),
 *     new BDIGoal( "do more with options" ),
 * } ) {{ control = "options"; }} // Replicate for all the values of "options"
 * </pre>
 *
 * @see LoopGoal
 * @see BDIGoal
 */
public class RepeatGoal extends BranchingGoal {

    /**
     * Constructor with a given control name, goal name and sub goals.
     */
    public RepeatGoal(String c,String n,Goal [] sg) {
	super( n, sg );
	setGoalControl( c );
    }

    /**
     * Constructor with a given goal name and sub goals.
     */
    public RepeatGoal(String n,Goal [] sg) {
	super( n, sg );
    }

    /**
     * Convenience constructor without sub goals.
     */
    public RepeatGoal(String n) {
	this( n, null );
    }

    /**
     * Creates and returns an instance object for achieving
     * a RepeatGoal.
     */
    public Instance instantiate(String head,Data d) {
	return new RepeatInstance( head );
    }

    /**
     * Implements re-instatiation of this goal relative something.
     */
    public class RepeatInstance extends MultiInstance {

	/**
	 * Constructor.
	 */
        public RepeatInstance(String h) {
            super( h );
        }

	/**
	 * To decide whether yet another branch should be made.
	 */
        public boolean more(int i,Data d) {
	    return i < d.size( getGoalControl() );
        }

	/**
	 * The branch is created be instantiating this goal as a
	 * sequence goal.
	 */
        public Instance getBranch(int i,String head,Data d) {
	    Goal g = new SequenceGoal( getGoalName(), getGoalSubgoals() );
	    g.setGoalControl( getGoalControl() );
	    g.setGoalGroup( getGoalGroup() );
	    return g.instantiate( head, d );
        }
    }

}
