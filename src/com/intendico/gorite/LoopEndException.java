/*********************************************************************
Copyright 2012, Ralph Ronnquist.

This file is part of GORITE.

GORITE is free software: you can redistribute it and/or modify it
under the terms of the Lesser GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GORITE is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the Lesser GNU General Public
License along with GORITE.  If not, see <http://www.gnu.org/licenses/>.
**********************************************************************/

package com.intendico.gorite;

/**
 * This exception is thrown by an {@link EndGoal} goal, to be caught
 * by an enclosing {@link LoopGoal} goal, which then terminates
 * successfully.
 */
public class LoopEndException extends Exception {
    /**
     * Version identity required for serialization.
     */
    public static final long serialVersionUID = 1L;

    /**
     * Cosntructor.
     */
    public LoopEndException(String s) {
	super( s );
    }
}
