/*********************************************************************
Copyright 2012, Ralph Ronnquist.

This file is part of GORITE.

GORITE is free software: you can redistribute it and/or modify it
under the terms of the Lesser GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GORITE is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the Lesser GNU General Public
License along with GORITE.  If not, see <http://www.gnu.org/licenses/>.
**********************************************************************/

package com.intendico.gorite;

import com.intendico.data.Query;
import com.intendico.data.Ref;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Vector;

/**
 * A TeamGoal is a {@link Goal} that is achieved as a {@link BDIGoal}
 * by a given {@link Team.Role}. The TeamGoal looks up {@link
 * Team.Role} fillers in the {@link Data} under the given role name,
 * and presents itself as a {@link BDIGoal} to be performed by the
 * fillers.
 *
 * <p> Note that the TeamGoal distributes like a {@link RepeatGoal}
 * over the role name data, requesting all fillers to achieve the goal
 * in parallel. The TeamGoal succeeds when all the fillers succeed,
 * and fails if any role filler fails. In the latter case, the
 * branches for the fillers in progress are cancelled.
 *
 * <p> If the TeamGoal is implemented by a {@link Team.Role} plan that
 *
 * @see Team
 * @see Team.TaskTeam
 * @see Performer#fillRole
 * @see ControlGoal
 */
public class TeamGoal extends BranchingGoal {

    /**
     * Constructor for TeamGoal.
     */
    public TeamGoal(String role,String n) {
	super( n, null );
	setGoalControl( role );
    }

    /**
     * Cache of construction object (when not a String).
     */
    public Object specific;

    /**
     * Constructor using an object other than String. Then the class
     * name of the object is used as goal name, and the object is held
     * as {@link #specific}. However, if the given object is a {@link
     * String}, then its value (rather than its type) is used as goal
     * name anyhow.
     * @see BDIGoal#BDIGoal(Object)
     */
    public TeamGoal(String role,Object n) {
	super( ( n instanceof String )? (String) n : n.getClass().getName() );
	specific = n;
	setGoalControl( role );
    }

    /**
     * Make an Instance for executing this goal.
     */
    public Instance instantiate(String head,Data d) {
	return new TeamInstance( head );
    }

    /**
     * Creates and returns an instance object for achieving
     * a TeamGoal.
     */
    public class TeamInstance extends MultiInstance {

	/**
	 * Constructor.
	 */
        public TeamInstance(String h) {
            super( h );
        }

	/**
	 * To decide whether yet another branch should be made.
	 */
        public boolean more(int i,Data d) {
	    return i < Math.max( 1, d.size( getGoalControl() ) );
        }

	/**
	 * The branch is created be instantiating a {@link BDIGoal}.
	 */
        public Instance getBranch(int i,String head,Data d) {
	    Goal g = specific != null?
		new BDIGoal( specific ) : new BDIGoal( getGoalName() );
	    g.setGoalGroup( getGoalGroup() );
	    g.setGoalControl( getGoalControl() );
            return g.instantiate( head, d );
        }

    }

}
