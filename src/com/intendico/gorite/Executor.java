/*********************************************************************
Copyright 2012, Ralph Ronnquist.

This file is part of GORITE.

GORITE is free software: you can redistribute it and/or modify it
under the terms of the Lesser GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GORITE is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the Lesser GNU General Public
License along with GORITE.  If not, see <http://www.gnu.org/licenses/>.
**********************************************************************/

package com.intendico.gorite;

import java.util.Vector;
import java.util.Iterator;
import com.intendico.data.Ref;

/**
 * The Executor class implements functions for managing goal execution
 * for a collection of performers. More precisely, it implements the
 * dispatch thread control to the {@link Performer.TodoGroup} goal
 * executions.
 *
 * <p> System execution may be controlled by invoking the {@link
 * Performer#performGoal} method for the top level team. This will
 * lead the execution thread into the Executor for progressing all
 * {@link Performer.TodoGroup} executions.
 *
 * <p> The Executor class also includes a control loop, the {@link
 * #run} method, for using a separate thread to run performers. This
 * is an alternative execution principle to the use of {@link
 * Performer#performGoal}. It's not a good idea to use both.
 *
 * <p> Ordinarily the use of the Executor class is transparent to an
 * application developer. A single Executor object is created so as to
 * provide thread control dispatch to all performers, and the top
 * level {@link Performer#performGoal} utilizes this as needed.
 *
 * <p> An application may split the performers amongst several
 * executors, and create background threads for {@link
 * Performer.TodoGroup} executions. In that case, the developer must
 * take precaution against multi-threading interferences, and in
 * particular refrain from using {@link Performer#performGoal}.
 *
 * <p> The transfer of control between goal executions uses the {@link
 * #steps} variable, which tells how many goal steps an execution may
 * advance before being stopped to allow the thread to pursue another
 * goal execution.
 */
public class Executor implements Runnable {

    /**
     * This is a callback interface for monitoring the executor.
     */
    public interface Listener {
	/**
	 * This method is invoked when the Executor is going idle.
	 */
	public void goingIdle(Executor e);
	/**
	 * This method is invoked when the Executor is waking up.
	 */
	public void wakingUp(Executor e);
    }


    /**
     * Holds the current default executor that performers add
     * themselves to by default. Initially <em>null</em>, but one is
     * set up on demand if needed by {@link #getDefaultExecutor()}.
     */
    public static Executor default_executor = null;

    /**
     * Returns the default executor, and creates one if needed.
     */
    public static Executor getDefaultExecutor() {
	if ( default_executor == null )
	    default_executor = new Executor( "[gorite]" );
	return default_executor;
    }

    /**
     * A name for the executor. This is also used as thread name for
     * the executor's dedicated thread.
     */
    public String name;

    /**
     * Configuration value for how many {@link #changeFocus()}
     * invocations to count between it returning <em>true</em>.
     */
    public int steps = 40;

    /**
     * Dynamic counter of how many {@link #changeFocus()} invocations
     * remain until it will return <em>true</em>.
     */
    public int count = steps;

    /**
     * The {@link Performer} objects managed by this executor.
     */
    public Vector/*<Performer>*/ performers = new Vector/*<Performer>*/();

    /**
     * A dynamic state variable.
     */
    public boolean running = true;

    /**
     * A dynamic state variable. Used by the {link #stop} method for
     * telling the {@link #run} method to stop and exit when
     * convenient. Value 1 is a "hard stop", that takes effect even
     * while performers have things to do, and value 2 is a "soft
     * stop" that takes effect when all performers are blocking.
     */
    public int stopping = 0;

    /**
     * A dynamic state variable. Index of the performer to re-run
     * next.
     */
    public int index = 0;

    /**
     * The collection of listeners, which get notify about the
     * thread going to sleep and waking up.
     */
    public Vector/*<Listener>*/ listeners = new Vector/*<Listener>*/();

    /**
     * Utility method to notify all listeners
     */
    public void notifyGoingIdle() {
	Vector/*<Listener>*/ handlers = new Vector/*<Listener>*/();
	synchronized ( this ) {
	    handlers.addAll( listeners );
	}
	for ( Iterator/*<Listener>*/ i = handlers.iterator();
	      i.hasNext(); ) {
	    Listener x = (Listener) i.next();
	    x.goingIdle( this );
	}
    }

    /**
     * Utility method to notify all listeners
     */
    public void notifyWakingUp() {
	Vector/*<Listener>*/ handlers = new Vector/*<Listener>*/();
	synchronized ( this ) {
	    handlers.addAll( listeners );
	}
	for ( Iterator/*<Listener>*/ i = handlers.iterator();
	      i.hasNext(); ) {
	    Listener x = (Listener) i.next();
	    x.wakingUp( this );
	}
    }

    /**
     * Adds a listener to the Executor to be notified when the
     * Executor goes idle and wakes up from idle
     * @param listener
     */
    synchronized public void addListener(Listener listener) {
	listeners.add( listener );
    }

    /**
     * Removes a listener from the executor.
     * @param listener
     */
    synchronized public void removeListener(Listener listener) {
	listeners.remove( listener );
    }

    /**
     * Constructor. The given name is also used as thread name for the
     * thread that is started.
     */
    public Executor(String n) {
	name = n;
    }

    /**
     * Returns the executor's name.
     */
    public String toString() {
	return name;
    }

    /**
     * Tells whether it is time to change focus or not. This is used
     * by the {@link Goal.Instance#perform} method to possibly stop
     * the current computation in order to let the executor shift
     * focus to another computation. Whenever the {@link #count}
     * arrives at 0, then it it is time to shift focus, and to reset
     * the count from {@link #steps}. Though, if the latter is 0 or
     * negative, then a change of focus will never be signalled from
     * here.
     */
    public boolean changeFocus() {
	if ( steps <= 0 )
	    return false;
	if ( count-- > 0 )
	    return false;
	if ( Goal.isTracing() )
	    System.err.println( "CHANGE FOCUS" );
	count = steps;
	return true;
    }

    /**
     * Utility method to "preempt" current intention by forcing {@link
     * #count} to zero. This only has effect if {@link #steps} is
     * non-zero, i.e., when using intention interleaving.
     */
    public void preempt() {
	count = 0;
    }

    /**
     * Utility method to add a {@link Performer} to be managed by this
     * executor. If the performer already belongs to an executor, it
     * is first removed from that one.
     */
    synchronized public void addPerformer(Performer performer) {
	if ( performer.executor != null ) {
	    // The performer already belongs to an executor
	    if ( performer.executor != this )
		performer.executor.removePerformer( performer );
	}
	if ( ! performers.contains( performer ) )
	    performers.add( performer );
	performer.executor = this;
    }

    /**
     * Utility method to remove a {@link Performer} from the
     * collection of performers managed by this executor. This does
     * not interrupt an ongoing computation, but means that the
     * executor will not re-run the performer.
     */
    synchronized public void removePerformer(Performer performer) {
	if ( performer.executor == this ) {
	    performers.remove( performer );
	    performer.executor = null;
	}
    }

    /**
     * Utility method to localize the first {@link Performer} by its
     * name.
     */
    synchronized public Performer findPerformer(String name)
    {
	for ( Iterator/*<Performer>*/ i =
		  performers.iterator(); i.hasNext(); ) {
	    Performer performer = (Performer) i.next();
	    if ( performer.name.equals( name ) )
		return performer;
	}
	return null;
    }

    /**
     * Utility method to "tickle" the executor with, just in case it
     * is idle.
     */
    synchronized public void signal() {
	running = true;
	notifyAll();
    }

    /**
     * A utility method for obtaining the next performer to re-run, or
     * <em>null</em>, in between having told about them all.
     */
    synchronized public Performer getNext() {
	if ( index < performers.size() )
	    return (Performer) performers.get( index++ );
	index = 0;
	return null;
    }

    /**
     * A utility method for going idle when there doesn't seem to be
     * anything to do.
     */
    public void waitRunning() {
	boolean idle = false;
	synchronized ( this ) {
            if ( ! running && stopping == 0 )
		idle = true;
	}
	if ( idle )
	    notifyGoingIdle();
	synchronized ( this ) {
	    if ( idle ) {
		while ( ! running && stopping == 0 ) {
		    if ( Goal.isTracing() )
			System.err.println(
			    "** executor " + this + " idle..." );
		    try {
			wait();
		    } catch (InterruptedException e) {
		    }
		}
	    }
	    running = false;
	}
	if ( idle ) {
	    if ( Goal.isTracing() )
		System.err.println( "** executor " + this + " active..." );
	    notifyWakingUp();
	}
    }

    /**
     * Utility method to run all {@link Performer Performers} once,
     * and then report if they all reported blocking or not.
     */
    public Goal.States runPerformersOnce() {
	if ( Goal.isTracing() )
	    System.err.println( "** runPerformersOnce" );
	boolean stopped = false;
	Performer performer = getNext();
	while ( performer != null ) {
	    if ( performer.runPerformer() != Goal.States.BLOCKED )
		stopped = true;
	    performer = getNext();
	}
	if ( Goal.isTracing() )
	    System.err.println( "** runPerformersOnce stopped " + stopped );
	return stopped? Goal.States.STOPPED : Goal.States.BLOCKED ;
    }

    /**
     * Utility method to keep running {@link Performer Performers}
     * while not all are blocking. This method invokes {@link
     * #runPerformersOnce} repeatedly until it reports that all
     * performers are blocked.
     */
    public boolean runPerformersBlocked() {
        if ( Goal.isTracing() )
            System.err.println( "** runPerformersBlocked" );
	boolean done_something = false;
	while ( runPerformersOnce() != Goal.States.BLOCKED ) {
	    done_something = true;
	    if ( stopping > 0 )
		break;
	}
        if ( Goal.isTracing() )
            System.err.println(
		"** runPerformersBlocked done something " + done_something );
	return done_something;
    }

    /**
     * This is the main execution loop of the executor. It keeps
     * running its performers until they are all blocked, in which
     * case it waits for a signal to re-run them.
     */
    public void run() {
	if ( Goal.isTracing() )
	    System.err.println( "** starting executor " + this );
	try {
	    index = 0;
	    for ( ;; ) {
		if ( runPerformersBlocked() ) 
		    running = true;
		if ( running ) {
		    if ( stopping == 1 )
			break;
		    running = false;
		} else {
		    waitRunning();
		    if ( stopping > 0 )
			break;
		}
	    }
	    
	} catch (Throwable t) {
	    t.printStackTrace();
	    System.err.println( "** terminating executor " + this );
	}
	stopping = 0;
    }

    /**
     * Tell the {@link #run} method to stop.
     */
    public void stop(boolean hard) {
	stopping = hard? 1 : 2;
	signal();
    }

    /**
     * Service interface to request a goal to be performed by a named
     * performer, with thread synchronisation on this Executor.
     */
    synchronized public boolean performGoal(
	String performer,Goal goal,String head,Data d) {
	return findPerformer( performer ).performGoal( goal, head, d );
    }
    
    /**
     * Service interface to request a goal to be performed by a named
     * performer, with thread synchronisation on this Executor.
     */
    synchronized public boolean performGoal(
	String performer,Goal goal,String head,
	Vector/*<Ref>*/ ins,Vector/*<Ref>*/ outs) {
	return findPerformer( performer ).performGoal( goal, head, ins, outs );
    }

    /**
     * Service interface to request a goal to be performed by a named
     * performer.
     */
    public boolean performGoal(
	String performer,String goal,String head,
	Vector/*<Ref>*/ ins,Vector/*<Ref>*/ outs) {
	return findPerformer( performer ).performGoal( goal, head, ins, outs );
    }
}

