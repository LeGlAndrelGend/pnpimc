/*********************************************************************
Copyright 2012, Ralph Ronnquist.

This file is part of GORITE.

GORITE is free software: you can redistribute it and/or modify it
under the terms of the Lesser GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GORITE is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the Lesser GNU General Public
License along with GORITE.  If not, see <http://www.gnu.org/licenses/>.
**********************************************************************/

package com.intendico.gorite;

import com.intendico.data.Query;
import com.intendico.data.Ref;
import com.intendico.data.Relation;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;

/**
 * The Team class is a base class for a structured task performer,
 * i.e. a {@link Performer} that combine sub performers. A particular
 * team type is defined by extending the Team class. The structure of
 * an instance of a team type is defined by calls to the {@link
 * #addPerformer} method. This provides a lookup context for inner
 * {@link TaskTeam} objects, which are used to define particular sub
 * team constallations for particular team tasks.
 */

public class Team extends Performer {

    /**
     * Default constructor.
     */
    public Team() {
    }

    /**
     * Constructor for team of given name.
     */
    public Team(String name)
    {
	super( name );
    }

    /**
     * This is the current role filling for this team -- it's
     * obligation structure.
     */
    public Vector/*<Performer>*/ performers = new Vector/*<Performer>*/();

    /**
     * This is a table of TaskTeam objects, to support longer term
     * constallations.
     */
    public Hashtable/*<String,TaskTeam>*/ groupings =
        new Hashtable/*<String,TaskTeam>*/();

    /**
     * Adds a particular performer in a named role filling.
     */
    synchronized public void addPerformer(Performer performer)
    {
	performers.add( performer );
	for ( Iterator/*<TaskTeam>*/ ti = groupings.values().iterator();
	      ti.hasNext(); ) {
	    ((TaskTeam) ti.next()).updateFillers( performer );
	}
    }

    /**
     * Adds multiple, named performers.
     */
    public void addPerformers(Performer [] p)
	throws Exception
    {
	if ( p != null ) {
	    for ( int i = 0; i < p.length; i++ )
		addPerformer( p[i] );
	}
    }

    /**
     * Utility method to pick a performer. Return null when the index
     * is out of bounds.
     */
    synchronized public Performer getPerformer(int i) {
	if ( i < 0 || i >= performers.size() )
	    return null;
	return (Performer) performers.get( i );
    }

    /**
     * Utility method to pick a performer by name. Returns the first
     * Performer of this team with the given name, or null.
     */
    public Performer getPerformer(String name) {
	for ( Iterator/*<Performer>*/ i =
		  performers.iterator(); i.hasNext(); ) {
	    Performer p = (Performer) i.next();
	    if ( name.equals( p.name ) ) {
		return p;
	    }
	}
	return null;
    }

     /**
      * Utility method to remove a performer by name. Removes and
      * returns the first Performer with the given name, or null;
      */
     public Performer removePerformer(String name) {
	 for ( Iterator/*<Performer>*/ i =
		   performers.iterator(); i.hasNext(); ) {
	     Performer p = (Performer) i.next();
	     if ( name.equals( p.getName() ) ) {
		 i.remove();
		 return p;
	     }
	 }
	 return null;
     }

    /**
     * Utility method to access a long term TaskTeam by name.
     */
    public TaskTeam getTaskTeam(String name) {
	return (TaskTeam) groupings.get( name );
    }

    /**
     * Utility method to define a long term TaskTeam by unique name.
     */
    public void setTaskTeam(String name,TaskTeam group) {
	groupings.put( name, group );
	group.updateFillers( performers );
    }

    /**
     * A TaskTeam represents a sub-grouping of a team's performers
     * into roles as appropriate for particular team activities. Each
     * TaskTeam is populated to hold a selection of performers that
     * are nominated for the task team roles according to their
     * abilities. This role filling defines the candidates for acting
     * in the roles when the TaskTeam is "established" in a particular
     * intention.
     *
     * <p> The role filling in a TaskTeam as well as the established
     * role filling in an intention may be changed at any time. When
     * the TaskTeam is installed for a {@link Team}, the current
     * {@link #performers} collection is revide and performers are
     * nominated to roles as appropriate. Thereafter each subsequently
     * added {@link Performer} is automatically considered, and
     * nominated to roles of installed TaskTeams as appropriate. The
     * detail nomination decision is made by the overridable {@link
     * Role#canFill(Performer)} method, which by default ensures
     * distinct, singular role filling, and that the nominated
     * {@link Performer} has plans for all required goals.
     *
     * <p> The TaskTeam is deployed on demand for intentions,
     * typically by by means of {@link #deploy(String)} goal in team
     * plans. When deployed in this way, the TaskTeam role filling is
     * established in the intention {@link Data} via {@link
     * #establish(Data)}, with a further filtering through the
     * overridable {@link Role#canAct(Data,Performer)} method, which
     * by default is "true".
     */
    public class TaskTeam {

	/**
	 * The roles of the task team.
	 */
	public Vector/*<Role>*/ roles = new Vector/*<Role>*/();

	/**
	 * The population of this task team, held as a {@link
	 * Relation} beteen roles and performers.
	 */
	public Relation fillers = new Relation( "fillers", 2 );

	/**
	 * Utility method to add a role to the task team.
	 */
	public void addRole(Role r) {
	    r.group = this;
	    roles.add( r );
	}

	/**
	 * Utility method to remove a role from the task team. This
	 * also removes any population for the role.
	 */
	public void removeRole(Role r) {
	    r.group = this;
	    roles.remove( r );
	    filling( r, new Ref( "$p" ) ).remove();
	}

	/**
	 * Utility method to return a role filling {@link Query}
	 * object for a given role and performer, either of which is
	 * given as a constant of its type, null, or a {@link Ref}
	 * object, which may be (appropriately) bound or unbound.
	 */
	public Query filling(Object r,Object p) {
	    try {
		return fillers.get( new Object [] { r, p } );
	    } catch (Exception e) {
		e.printStackTrace();
		return null;
	    }
	}

	/**
	 * Utility method to define a role filler for a role.
	 */
	public void fillRole(Role r,Performer p) {
	    filling( r, p ).add();
	}

	/**
	 * Utility method to clear all filling.
	 */
	public void clearFilling() {
	    filling( null, null ).remove();
	}

	/**
	 * Updates the role nominations for the given {@link Performer
	 * performers}. This method simply iterates over the
	 * collection and updates each performer one by one via {@link
	 * #updateFillers(Performer)}.
	 */
	public void updateFillers(Vector/*<Performer>*/ performers) {
	    for ( Iterator/*<Performer>*/ pi = performers.iterator();
		  pi.hasNext(); ) {
		updateFillers( (Performer) pi.next() );
	    }
	}

	/**
	 * Updates the role nominations for the given {@link
	 * Performer} as appropriate for roles in this taks team.
	 * Which roles it is nominated to, if any, is determined by
	 * {@link Role#canFill(Performer)}.
	 */
	public void updateFillers(Performer p) {
	    for ( Iterator/*<Role>*/ ri = roles.iterator(); ri.hasNext(); ) {
		Role r = (Role) ri.next();
		Query q = filling( r, p );
		if ( r.canFill( p ) ) {
		    if ( Goal.isTracing() ) {
			System.err.println(
			    "** can Fill: " + r.name + " " + p );
		    }
		    q.add();
		} else {
		    if ( Goal.isTracing() ) {
			System.err.println(
			    "** cannot Fill: " + r.name + " " + p );
		    }
		    q.remove();
		}
	    }
	}

	/**
	 * Utility method to establish the role filling in the given
	 * intention data. This will establish the roles one by one in
	 * declaration order, using the current fillers as filtered by
	 * the {@link Role#canAct} method.
	 *
	 * <p> Note that role filling is set up as data elements using
	 * the role names. The value for a role name is the collection
	 * of fillers that can act in the role, qua {@link
	 * Capability}, but actually represented by {@link
	 * Performer.RoleFilling} objects as returned by the {@link
	 * Performer#fillRole(Team.Role)} method.
	 * @return true if all roles have some filling
	 */
	public boolean establish(Data d) {
	    if ( Goal.isTracing() ) {
		System.err.println( "** establish task team" );
	    }
	    for ( Iterator/*<Role>*/ ri = roles.iterator();
		  ri.hasNext(); ) {
		Role role = (Role) ri.next();
		Ref $p = new Ref( "$p" );
		Query q = filling( role, $p );
		d.forget( role.name );
		boolean filled = false;
		HashSet actors = Team.getActors( role.name, d );
		try {
		    while ( q.next() ) {
			Performer candidate = (Performer) $p.get();
			if ( actors.contains( candidate ) ) {
			    if ( Goal.isTracing() ) {
				System.err.println(
				    "** already Acting: " + role.name +
				    " " + candidate );
			    }
			} else if ( role.canAct( d, candidate ) ) {
			    if ( Goal.isTracing() ) {
				System.err.println(
				    "** can Act: " + role.name +
				    " " + candidate );
			    }
			    Capability c = candidate.fillRole( role );
			    if ( c != null ) {
				d.setValue( role.name, c );
				filled = true;
			    } else if ( Goal.isTracing() ) {
				System.err.println(
				    "** refuses to Act: " + role.name +
				    " " + candidate );
			    }
			} else if ( Goal.isTracing() ) {
			    System.err.println(
				"** cannot Act: " + role.name +
				" " + candidate );
			}
		    }
		} catch (Exception e) {
		    e.printStackTrace();
		}
		if ( ! filled ) {
		    if ( Goal.isTracing() ) {
			System.err.println(
			    "** Role not established: " + role.name );
		    }
		    return false; // couldn't establish the role at
				  // all
		}
	    }
	    return true;
	}

	/**
	 * Utility method that creates a {@link Goal} of establishing
	 * this task team in the intention data when the goal is to be
	 * achieved.
	 * @see #establish(Data)
	 */
	public Goal deploy(String name) {
	    return new Goal( "deploy " + name ) {
		    public States execute(Data d) {
			return establish( d )? States.PASSED : States.FAILED;
		    }
		};
	}

	/**
	 * Utility method to collate all actors of this TaskTeam's
	 * roles in the given Data.
	 * @return a Hashtable of {@link Performer} HashSets, keyed by
	 * the {@link Role} names of the TaskTeam's roles.
	 */
	public Hashtable/*<String,HashSet<Performer>>*/ getActors(Data d) {
	    Hashtable result = new Hashtable();
	    for ( Iterator/*<Role>*/ ri = roles.iterator(); ri.hasNext(); ) {
		Role r = (Role) ri.next();
		HashSet/*<Performer>*/ actors = Team.getActors( r.name, d );
		if ( actors != null )
		    result.put( r.name, actors );
	    }
	    return result;
	}

	/**
	 * Utility method to return the enclosing team wherein this
	 * TaskTeam object is created.
	 */
	public Team team() {
	    return Team.this;
	}
    }

    /**
     * Utility method to collate the performers defined to act in the
     * given role in a given Data.
     * @see TaskTeam#getActors(Data)
     */
    static public HashSet/*<Performer>*/ getActors(String r,Data d) {
	HashSet/*<Performer>*/ result = new HashSet/*<Performer>*/();
	Data.Element e = d.find( r );
	if ( e != null ) {
	    for ( Iterator/*<Object>*/ vi = e.values.iterator();
		  vi.hasNext(); ) {
		result.add( ((Capability) vi.next()).getPerformer() );
	    }
	}
	return result;
    }

    /**
     * The Role class is a base class for representing team members in
     * a team. Role extends {@link Capability}, so as to hold {@link
     * Goal} methods that are tied to the role within the context of a
     * {@link Team}. It further contains an attribute {@link
     * #required}, which indicate goals that are required by a {@link
     * Performer} of the role.
     */
    public class Role extends Capability {

	/**
	 * The reference name for this role.
	 */
	public String name;

	/**
	 * The goal names required by a role filler.
	 */
	public String [] required;

	/**
	 * The {@link TaskTeam} that this role is a role of. This is
	 * set by the {@link TaskTeam#addRole} method when the role is
	 * added.
	 */
	public TaskTeam group;

	/**
	 * Constructor that takes an array of required goals.
	 */
	public Role(String n,String [] r) {
	    name = n;
	    required = r;
	    setPerformer( Team.this );
	}

	/**
	 * Overrides {@link Capability#addCapability} so as to link up
	 * added capabilities with this team qua performer.
	 */
	public void addCapability(Capability c) {
	    c.setPerformer( Team.this );
	    super.addCapability( c );
	}

	/**
	 * Returns the team of the role.
	 */
	public Team team() {
	    return Team.this;
	}

	/**
	 * Overridable method that determines if the given performer
	 * can fill this role (within its {@link TaskTeam} {@link
	 * #group}). The default decision implemented by this
	 * method ensures that: <ol>
	 *
	 * <li> the performer is not already filling another role,
	 *
	 * <li> the role is not already filled by another performer,
	 * and
	 *
	 * <li> the performer has some plans to achieve the required
	 * goals.
	 * 
	 * </ol>
	 */
	public boolean canFill(Performer p) {
	    try {
		if ( group.filling( null, p ).next() )
		    return false;
		if ( group.filling( this, null ).next() )
		    return false;
		return p.hasGoals( required );
	    } catch (Exception e) {
		e.printStackTrace();
		return false;
	    }
	}

	/**
	 * Overridable method that determines whether the given {@link
	 * Performer} can act in this role within the {@link
	 * Goal.Instance intention} represented by the given {@link
	 * Data}. The default decision is "yes".
	 * @see Team#getActors(String,Data)
	 */
	public boolean canAct(Data d,Performer p) {
	    return true;
	}

    }

}
