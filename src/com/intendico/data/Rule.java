/*********************************************************************
Copyright 2012, Ralph Ronnquist.

This file is part of GORITE.

GORITE is free software: you can redistribute it and/or modify it
under the terms of the Lesser GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GORITE is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the Lesser GNU General Public
License along with GORITE.  If not, see <http://www.gnu.org/licenses/>.
**********************************************************************/

package com.intendico.data;

import java.util.ConcurrentModificationException;

/**
 * The Rule class defines a belief propagation relationship
 * between a pair of {@link Query} objects that share {@link Ref}
 * objects. The Rule provides a forward directed implication such
 * that when the antecedent comes true for a binding, then the
 * consequent is made true for that binding.
 *
 * <p> A Rule object is operated by first calling {@link #reset},
 * which results in a {link Snapshot#reset} that collects all new
 * bindings for the antecedent {@link Query}. The second step is a
 * subsequent call to {@link #propagate}, which results in calls to
 * {@link Query#add} on the consequent {@link Query} with bindings as
 * given by the antecedent snapshot.
 */
public class Rule {

    /**
     * The rule antecedent.
     */
    public Snapshot antecedent;

    /**
     * The rule consequent.
     */
    public Query consequent;

    /**
     * Constructor.
     */
    public Rule(Query a,Query c) {
	if ( a != null )
	    antecedent = new Snapshot( a );
	consequent = c;
    }

    /**
     * Invoked to restart the Rule from the current belief state.
     */
    public void reset() throws Exception {
	try {
	    antecedent.reset();
	} catch (ConcurrentModificationException e) {
	    System.err.println( "Rule: " + this );
	    throw e;
	}
    }

    /**
     * Invoked to propagate antecedent bindings into consequent
     * additions.
     */
    public void propagate() throws Exception {
	while ( antecedent.next() ) {
	    consequent.reset();
	    if ( ! consequent.next() )
		consequent.add();
	}
    }

    /**
     * Returns a textual representation of a Rule.
     */
    public String toString() {
	return antecedent.query + " => " + consequent;
    }
}
