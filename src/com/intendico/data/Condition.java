/*********************************************************************
Copyright 2012, Ralph Ronnquist.

This file is part of GORITE.

GORITE is free software: you can redistribute it and/or modify it
under the terms of the Lesser GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GORITE is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the Lesser GNU General Public
License along with GORITE.  If not, see <http://www.gnu.org/licenses/>.
**********************************************************************/

package com.intendico.data;

import java.util.Observer;
import java.util.Vector;
import java.util.Iterator;

/**
 * A utility class for including a boolean method in query
 * processing. The typical use is as an inline extension that
 * implements the {@link #condition} method.
 */
abstract public class Condition implements Query {

    /**
     * An always true Condition query.
     */
    public static Condition TRUE = new Condition() {
	    public boolean condition() {
		return true;
	    }
	};

    /**
     * Control flag to mark whether the condition has been tested or
     * not.
     */
    public boolean tested = false;

    /**
     * Cache of initially unbound variables. These are captured on
     * construction, just in case there is an extension that takes
     * variables. In that case, the next() method on failing must
     * restore the incoming unbound variables to be unbound.
     */
    public Vector variables;

    /**
     * The {@link Query#reset} method implemented by noting the
     * condition as not tested.
     */
    public void reset() throws Exception {
	tested = false;
    }

    /**
     * The {@link Query#copy} method implemented by using this same
     * object.
     */
    public Query copy(Vector/*<Ref>*/ newrefs) throws Exception {
	return this;
    }

    /**
     * The {@link Query#next} method implemented by returning as per
     * the {@link #condition} method on its first invocation, then
     * returning false without invoking the {@link #condition} method,
     * until {@link #reset()}. This method also captures any unbound
     * variables (an extension class may have variables), and clears
     * these before returning false.
     */
    public boolean next() throws Exception {
	if ( ! tested ) {
	    variables = getRefs( new Vector() );
	    if ( variables.size() > 0 ) {
		for ( Iterator i = variables.iterator(); i.hasNext(); ) {
		    if ( ((Ref) i.next()).get() != null ) {
			i.remove();
		    }
		}
		if ( variables.size() == 0 )
		    variables = null;
	    } else {
		variables = null;
	    }
	    tested = true;
	    if ( condition() )
		return true;
	}
	if ( variables != null ) {
	    for ( Iterator i = variables.iterator(); i.hasNext(); ) {
		((Ref) i.next()).clear();
	    }
	}
	return false;
    }

    /**
     * The {@link Query#getRefs} method implemented by adding
     * nothing. An actual condition that uses {@link Ref} objects
     * should override this method.
     */
    public Vector/*<Ref>*/ getRefs(Vector/*<Ref>*/ v) {
	return v;
    }

    /**
     * The {@link Query#addObserver} method implemented by doing
     * nothing. An actual condition that is triggering should override
     * this method as well as {@link #deleteObserver}.
     */
    public void addObserver(Observer x) {
    }

    /**
     * The {@link Query#deleteObserver} method implemented by doing
     * nothing. An actual condition that is triggering should override
     * this method as well as {@link #addObserver}.
     */
    public void deleteObserver(Observer x) {
    }

    /**
     * The {@link Query#addable} method is implemented by a reset
     * followed by next.
     */
    public boolean addable() {
	try {
	    reset();
	    return next();
	} catch (Exception e) {
	    e.printStackTrace();
	    return false;
	}
    }

    /**
     * The {@link Query#add} method implemented by returning false. An
     * actual condition that gets updated should override this method.
     */
    public boolean add() {
	return false;
    }

    /**
     * Implements {@link Query#removable} by calling {@link #addable}
     */
    public boolean removable()
    {
	return addable();
    }

    /**
     * Implements {@link Query#remove} by returning false;
     */
    public boolean remove() {
	return false;
    }

    /**
     * The actual condition returning <em>true</em> or
     * <em>false</em>. If it is appropriate to re-test the condition
     * several times before resetting, the actual condition may also
     * set the {@link #tested} flag.
     */
    abstract public boolean condition() throws Exception;

    /**
     * Returns a {@link java.lang.String} representation of the
     * condition object.
     */
    public String toString() {
	return "Condition#" + System.identityHashCode( this );
    }
}
