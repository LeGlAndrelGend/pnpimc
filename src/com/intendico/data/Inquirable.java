/*********************************************************************
Copyright 2012, Ralph Ronnquist.

This file is part of GORITE.

GORITE is free software: you can redistribute it and/or modify it
under the terms of the Lesser GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GORITE is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the Lesser GNU General Public
License along with GORITE.  If not, see <http://www.gnu.org/licenses/>.
**********************************************************************/

package com.intendico.data;

/**
 * This interface defines the requirements of an inquirable data
 * structure, which basically is a named {@link Query} factory.
 */
public interface Inquirable {

    /**
     * Interface method to obtain the name of this Inquirable.
     */
    public String getName();

    /**
     * Interface method for providing an access {@link Query} given a
     * collection of values and {@link Ref} objects.
     */
    public Query get(Object/*...*/ [] arguments) throws Exception ;

}
