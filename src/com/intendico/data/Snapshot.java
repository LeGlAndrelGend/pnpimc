/*********************************************************************
Copyright 2012, Ralph Ronnquist.

This file is part of GORITE.

GORITE is free software: you can redistribute it and/or modify it
under the terms of the Lesser GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GORITE is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the Lesser GNU General Public
License along with GORITE.  If not, see <http://www.gnu.org/licenses/>.
**********************************************************************/

package com.intendico.data;

import java.util.Vector;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Observer;

/**
 * The Snapshot class implements a query overlay for capturing changes
 * to a source {@link Query} in respect of the added and lost valid
 * binding combinations for the {@link Ref} objects
 * involved. Temporally a snapshot is taken by the {@link #reset}
 * method, as well as at construction. Thereafter successive {@link
 * #next} invocations provide the added valid binding combinations
 * since the prior snapshot.
 *
 * <p> Following a reset, the Snapshot also holds the lost bindings,
 * which can be inspected separately.
 *
 * <p> A Snapshot may hold a non-null {@link #sync} object, which is
 * used for synchronizing on during {@link #reset}. Any concurrent
 * thread that updates queried components should synchronize on the
 * same object, and in that way avoid multi-threading conflict.
 */
public class Snapshot implements Query {

    /**
     * Holds all the bindings found at the most recent snapshot.
     */
    public HashSet/*<Vector>*/ snapshot;

    /**
     * Holds the bindings that were valid before the most recent
     * snapshot, but were no longer valid at that snapshot.
     */
    public HashSet/*<Vector>*/ lost = new HashSet/*<Vector>*/();

    /**
     * Holds the Ref objects in a Vector.
     */
    public Vector/*<Ref>*/ refs;

    /**
     * The iterator for recalling successive new bindings by the
     * {@link #next} method.
     */
    public Iterator/*<Vector>*/ bindings;

    /**
     * The query being monitored.
     */
    public Query query;

    /**
     * The object, if any, to synchronize on during {@link
     * #reset}. This is to support multi-thread access to queried
     * components; any concurrent thread needs to synchronize on the
     * same object.
     */
    public Object sync = null;

    /**
     * Constructor.
     */
    public Snapshot(Query q) {
	query = q;
	refs = query.getRefs( new Vector/*<Ref>*/() );
	try {
	    reset();
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }

    /**
     * Alternative constructor with a synchronization object.
     */
    public Snapshot(Object lock,Query q) {
	this( q );
	sync = lock;
    }

    /**
     * The {@link Query#copy} method implemented by creating a new
     * Snapshot with a copy of the monitored {@link #query}, and
     * sharing any {@link #sync} object.
     */
    public Query copy(Vector/*<Ref>*/ newrefs) throws Exception {
	return new Snapshot( sync, query.copy( newrefs ) );
    }

    /**
     * The {@link Query#reset} method implemented by capturing a new
     * snapshot of the given query, and initialising the Iterator for
     * the collection of bindings that are new since the last
     * snapshot.
     *
     * <p> This method updates {@link #snapshot}, {@link #lost} and
     * {@link #bindings}.
     */
    public void reset()	throws Exception {
	if ( sync == null ) {
	    doReset();
	} else {
	    synchronized( sync ) {
		doReset();
	    }
	}
    }

    /**
     * Actual reset method, which may and might not be synchronized.
     */
    private void doReset() throws Exception {
	lost = snapshot != null? snapshot : new HashSet/*<Vector>*/();
	HashSet/*<Vector>*/ is = new HashSet/*<Vector>*/();
	snapshot = new HashSet/*<Vector>*/();
	Ref.clear( refs );
	query.reset();
	boolean none = true;
	while ( query.next() ) {
	    none = false;
	    Vector v = Ref.getBinding( refs );
	    if ( snapshot.contains( v ) )
		continue;
	    snapshot.add( v );
	    if ( lost != null && lost.contains( v ) ) {
		lost.remove( v );
	    } else {
		is.add( v );
	    } 
	}
	bindings = none? null : is.iterator();
    }

    /**
     * Produces the succession of bindings that the qurey was valid
     * for at the last snapshot, but not at the one prior.
     *
     * <p> This method uses {@link #bindings}.
     */
    public boolean next() {
	if ( bindings == null )
	    return false;
	if ( bindings.hasNext() ) {
	    Ref.bind( refs, (Vector) bindings.next() );
	} else {
	    bindings = null;
	    return false;
	}
	if ( ! bindings.hasNext() )
	    bindings = null;
	return true;
    }

    /**
     * The {@link Query#getRefs} method implemented by returning the
     * query's Ref object.
     */
    public Vector/*<Ref>*/ getRefs(Vector/*<Ref>*/ v) {
	return query.getRefs( v );
    }

    /**
     * The {@link Query#addObserver} method implemented by adding
     * the observer to the query.
     */
    public void addObserver(Observer x) {
	query.addObserver( x );
    }

    /**
     * The {@link Query#deleteObserver} method implemented by
     * removing the observer from the query.
     */
    public void deleteObserver(Observer x) {
	query.deleteObserver( x );
    }

    /**
     * The {@link Query#addable} method implemented by forwarding to
     * the contained query.
     */
    public boolean addable() {
	return query.addable();
    }

    /**
     * The {@link Query#add} method implemented by forwarding to the
     * contained query.
     */
    public boolean add() {
	return query.add();
    }

    /**
     * The {@link Query#removable} method implemented by forwarding to
     * the wrapped query.
     */
    public boolean removable() {
	return query.removable();
    }

    /**
     * The {@link Query#remove} method implemented by forwarding to
     * the wrapped query.
     */
    public boolean remove() {
	return query.remove();
    }

    /**
     * Utility method to remove all remaining bindings for the
     * query. This method applies to the current snapshot, and might
     * make most sense for a newly created Snapshot object, where it
     * will remove all matching bindings.
     */
    public void removeAll() {
	while ( next() ) {
	    remove();
	}
    }

    /**
     * Returns a String representation of the Snapshot object.
     */
    public String toString() {
	return "Snapshot( " + query.toString() + " )";
    }
}
