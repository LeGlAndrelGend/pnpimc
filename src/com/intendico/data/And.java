/*********************************************************************
Copyright 2012, Ralph Ronnquist.

This file is part of GORITE.

GORITE is free software: you can redistribute it and/or modify it
under the terms of the Lesser GNU General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GORITE is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the Lesser GNU General Public
License along with GORITE.  If not, see <http://www.gnu.org/licenses/>.
**********************************************************************/

package com.intendico.data;

import java.util.Observer;
import java.util.Vector;

/**
 * The And class implements a conjunction of multiple queries. It
 * provides a left-to-right short-cut evaluation, which returns
 * immediately if a conjunct fails.
 */
public class And implements Query {

    /**
     * Holds the conjuncts being combined.
     */
    public Query [] conjuncts;

    /**
     * Keeps track of which query is current.
     */
    public int current;

    /**
     * Constructor.
     */
    public And(Query/*...*/ [] q) throws Exception {
	conjuncts = q;
	reset();
    }

    /**
     * The {@link Query#copy} method implemented by creating a new
     * And object with copies of the conjuncts.
     */
    public Query copy(Vector/*<Ref>*/ newrefs) throws Exception {
	Query [] q = new Query [ conjuncts.length ];
	for ( int i = 0; i < q.length; i++ ) {
	    q[i] = conjuncts[i].copy( newrefs );
	}
	return new And( q );
    }

    /**
     * The {@link Query#reset} method implemented by reverting to the
     * first conjunct and resetting it.
     */
    public void reset()
	throws Exception
    {
	current = 0;
	if ( conjuncts != null && conjuncts.length > 0 )
	    conjuncts[ 0 ].reset();
    }

    /**
     * The {@link Query#next} method implemented by advancing all
     * conjucts to the next match in a left-to-right fashion. Thus,
     * the right-most conjunct is advanced to its next match. If the
     * conjunct is exhausted, it gets reset, but after that the
     * preceding conjunct is advanced to its next match. If that is
     * exhausted, then that gets reset, and so on, up to advancing the
     * first conjunct until it gets exhausted.
     */
    public boolean next()
	throws Exception
    {
	if ( conjuncts == null || conjuncts.length == 0 )
	    return true;
	while ( current >= 0 ) {
	    if ( conjuncts[ current ].next() ) {
		current += 1;
		if ( current < conjuncts.length ) {
		    conjuncts[ current ].reset();
		    continue;
		}
		current -= 1;
		return true;
	    }
	    current -= 1;
	}
	return false;
    }

    /**
     * The {@link Query#getRefs} method implemented by combining the
     * Ref objects of all conjuncts.
     */
    public Vector/*<Ref>*/ getRefs(Vector/*<Ref>*/ v) {
	if ( conjuncts != null ) {
	    for ( int i=0; i < conjuncts.length; i++ ) {
		conjuncts[ i ].getRefs( v );
	    }
	}
	return v;
    }

    /**
     * The {@link Query#addObserver} method implemented by adding
     * the observer to all the conjuncts.
     */
    public void addObserver(Observer x) {
	if ( conjuncts != null )
	    for ( int i=0; i < conjuncts.length; i++ )
		conjuncts[ i ].addObserver( x );
    }

    /**
     * The {@link Query#deleteObserver} method implemented by
     * removing the observer from all the conjuncts.
     */
    public void deleteObserver(Observer x) {
	if ( conjuncts != null )
	    for ( int i=0; i < conjuncts.length; i++ )
		conjuncts[ i ].deleteObserver( x );
    }

    /**
     * The {@link Query#addable} method implemented by forwarding to all
     * conjuncts.
     */
    public boolean addable()
    {
	for ( int i=0; i < conjuncts.length; i++ )
	    if ( ! conjuncts[ i ].addable() )
		return false;
	return true;
    }

    /**
     * The {@link Query#add} method implemented by forwarding to all
     * conjuncts.
     */
    public boolean add()
    {
	boolean added = false;
	if ( conjuncts != null )
	    for ( int i=0; i < conjuncts.length; i++ )
		added |= conjuncts[ i ].add();
	return added;
    }

    /**
     * Implements {@link Query#removable} by finding a conjunct that
     * is removable.
     */
    public boolean removable()
    {
	for ( int i=0; i < conjuncts.length; i++ )
	    if ( conjuncts[ i ].removable() )
		return true;
	return false;
    }

    /**
     * Implements {@link Query#remove} by forawrding to the first
     * conjunct that is removable.
     */
    public boolean remove() {
	for ( int i=0; i < conjuncts.length; i++ )
	    if ( conjuncts[ i ].removable() )
		return conjuncts[ i ].remove();
	return false;
    }

    /**
     * Returns a String representation of the And object.
     */
    public String toString() {
	StringBuffer s = new StringBuffer();
	s.append( "And(" );
	if ( conjuncts != null )
	    for ( int i=0; i < conjuncts.length; i++ ) {
		if ( i > 0 )
		    s.append( "," );
		s.append( " " );
		s.append( conjuncts[ i ].toString() );
	    }
	s.append( " )" );
	return s.toString();
    }
}
