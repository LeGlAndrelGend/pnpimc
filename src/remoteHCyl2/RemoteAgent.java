package remoteHCyl2;

import org.json.JSONObject;
import pipe.EndPoint;
import pipe.Status;
import pipe.Request;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import com.intendico.gorite.*;
import com.intendico.data.Ref;
import com.intendico.gorite.addon.TimeTrigger;
import pipe.UDPClient;

import static pipe.Status.*;

public class RemoteAgent extends Performer {
    
    public Status status;
    
    Goal getLengthHCyl2(String ipDevice, int portDevice) {
        return new Goal( "getLengthHCyl2" ) {
            @Override
            public Goal.States execute(Data d) {
                String rp = (String) d.getValue( "remoteHCyl2 process");

                // step 2 takes 1 seconds.
                if ( TimeTrigger.isPending( d, "deadline", 1*1000 ) )
                    return Goal.States.BLOCKED;

                /**GET length HCyl2*/
                UDPClient client = new UDPClient(ipDevice, portDevice, "getLengthHCyl2()");
                String responce = null;
                try{
                    responce = client.sendMessage();
                } catch (IOException e){System.err.println(e);}
                JSONObject jsonObject = new JSONObject(responce);
                int length = jsonObject.getInt("length");
                Map<String, Map<String, Object>> HCYLPARAMS = (Map<String, Map<String,Object>>) d.getValue("local HCYLPARAMS");
                HCYLPARAMS.put("HCyl2", new HashMap<>());
                HCYLPARAMS.get("HCyl2").put("length", length);

                System.err.println( "getLengthHCyl2 completed" );

                d.setValue( "remoteHCyl2 machine", "device2" );
                d.setValue( "remoteHCyl2 time", new Integer(2) );
                d.setValue("local HCYLPARAMS", HCYLPARAMS);

                status.outs = new Vector<Ref>();
                status.outs.add( new Ref( "remoteHCyl2 machine", "device2" ) );
                status.outs.add ( new Ref( "remoteHCyl2 time", new Integer( 2 ) ) );
                status.outs.add( new Ref("local HCYLPARAMS", HCYLPARAMS));
                status.state = PASSED;

                return Goal.States.PASSED;
            }
        };
    }

    Goal sendStateHCyl2(){
        return new Goal("sendStateHCyl2") {
            @Override
            public Goal.States execute(Data d) {
                if ( TimeTrigger.isPending( d, "deadline", 1*1000 ) )
                    return Goal.States.BLOCKED;

                Map<String, Map<String, Object>> HCYLPARAMS = (Map<String, Map<String, Object>>) d.getValue("local HCYLPARAMS");
                Map<String, Object> test = new HashMap<>();
                HCYLPARAMS.put("test", test);
                d.setValue("local HCYLPARAMS", HCYLPARAMS);
                System.out.println(d.getValue("local HCYLPARAMS"));
                System.out.println(HCYLPARAMS);
                System.out.println("XTARGET" + d.getValue("XTARGET"));
                System.out.println("YTARGET" + d.getValue("YTARGET"));
                System.out.println("HCyl1" + d.getValue("HCyl1 state"));
                System.out.println("HCyl2" + d.getValue("HCyl2 state"));
                status.outs = new Vector<Ref>();
                status.outs.add( new Ref("local HCYLPARAMS", HCYLPARAMS));
                status.state = PASSED;
//                String agentStatus = HCYLPARAMS.get("HCyl2").get("state").toString();
//                UDPClient client = new UDPClient(EndPoint.hostAgent2, EndPoint.portDevice2, agentStatus);
//                System.out.println("Has been sent control signal('" + agentStatus + "') to device:" + EndPoint.hostAgent2 + EndPoint.portDevice2);

                return States.PASSED;
            }
        };
    }
    public void make( Request r ) {
        Data d = new Data();
        d.set( r.ins );
        d.set( r.outs );
        performGoal( new BDIGoal( "getLengthHCyl2" ), "make", d );
        performGoal( new BDIGoal( "sendStateHCyl2" ), "make", d );
    }
    
    public void setStatus( String g, Vector<Ref> o, String s ) {
        status = new Status( g, o, s );
    }
    
    public Status getStatus() {
        return status;
    }
    
    public RemoteAgent( String n, String ipDevice, int portDevice) {
        super( n );      
        addGoal( getLengthHCyl2(ipDevice, portDevice) );
        addGoal( sendStateHCyl2() );
    }
    
}
